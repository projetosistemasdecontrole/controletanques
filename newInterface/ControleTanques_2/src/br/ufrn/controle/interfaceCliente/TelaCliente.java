package br.ufrn.controle.interfaceCliente;

import br.ufrn.controle.core.ComponentMover;
import br.ufrn.controle.modelo.Controles;
import br.ufrn.controle.modelo.Funcao;
import br.ufrn.controle.modelo.Malha;
import br.ufrn.controle.modelo.ParametrosEntrada;
import br.ufrn.controle.negocio.ClienteControle;
import br.ufrn.controle.negocio.ObservadorEstados;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
public class TelaCliente extends javax.swing.JFrame {

    /**
     * Creates new form telaCliente
     */
    
    private JToggleButton malhaFechadaBtn;
    private JToggleButton malhaAbertaBtn;
    private JToggleButton sinalDegrauBtn;
    private JToggleButton sinalSenoidalBtn;
    private JToggleButton sinalQuadraticoBtn;
    private JToggleButton sinalDenteDeSerraBtn;
    private JToggleButton sinalAleatorioBtn;
    private JToggleButton simuladorBtn;
    private JToggleButton plantaBtn;
    private JToggleButton configuracao1Btn;
    private JToggleButton configuracao2Btn;
    private JToggleButton configuracao3Btn;
    private JToggleButton obsEstadoBtn;
    private JToggleButton segReferenciaBtn;
    
    private JButton iniciarBtn;
    private JButton atualizarBtn;
    private JButton conectarBtn;
    
    private JPanel titleBarMalha;
    private JPanel panelMalha;
    private JPanel titleBarSinal;
    private JPanel panelSinal;
    private JPanel titleBarLeitura;
    private JPanel panelLeitura;
    private JPanel titleBarEscrita;
    private JPanel panelEscrita;
    private JPanel panelConfiguracoes;
    private JPanel titleBarConfiguracoes;
    private JPanel titleBarMestre;
    private JPanel titleBarEscravo;
    private JPanel panelControlador;
    private JPanel titleBarControlador;
    private JPanel panelObservador;
    private JPanel titleBarObservador;
    private JPanel panelSeguidor;
    private JPanel titleBarSeguidor;
    private JPanel titleBarMatrizSeg;
    private JPanel titleBarMatrizObs;
    
    private JLabel titleMalha;
    private JLabel titleSinal;
    private JLabel titleEscrita;
    private JLabel titleLeitura;
    private JLabel titleConfiguracoes;
    private JLabel lblAmplitude;
    private JLabel notificacao1;
    private JLabel notificacao2;
    private JLabel notificacao3;
    private JLabel lblPeriodo;
    private JLabel lblPeriodoMin;
    private JLabel lblPeriodoMax;
    private JLabel lblAmplitudeMin;
    private JLabel lblAmplitudeMax;
    private JLabel lblOffset;
    private JLabel lblIP;
    private JLabel lblPorta;
    private JLabel lblSetPoint;
    private JLabel titleMestre;
    private JLabel titleEscravo;
    private JLabel lblControladoresE;
    private JLabel lblKpE;
    private JLabel lblKdE;
    private JLabel lblTdE;
    private JLabel lblKiE;
    private JLabel lblTiE;
    private JLabel lblControladores;
    private JLabel lblKp;
    private JLabel lblKd;
    private JLabel lblTd;
    private JLabel lblKi;
    private JLabel lblTi;
    private JLabel lblAntiWindUp;
    private JLabel lblFiltroDerivativo;
    private JLabel lblAntiWindUpE;
    private JLabel lblFiltroDerivativoE;
    private JLabel titleControlador;
    private JLabel titleObservador;
    private JLabel lblEstados;
    private JLabel lblPolos;
    private JLabel lblP1;
    private JLabel lblP2;
    private JLabel lblP3;
    private JLabel lblGanhos;
    private JLabel lblL1;
    private JLabel lblL2;
    private JLabel lblL3;
    private JLabel lblL;
    private JLabel lblL1Matriz;
    private JLabel lblL2Matriz;
    private JLabel lblL3Matriz;
    private JLabel titleSeguidor;
    private JLabel titleMatrizObs;
    private JLabel titleMatrizSeg;
    
    private JTextField txtIP;
    private JTextField txtPorta;
    private JTextField txtAmplitude;
    private JTextField txtPeriodo;
    private JTextField txtPeriodoMin;
    private JTextField txtPeriodoMax;
    private JTextField txtAmplitudeMin;
    private JTextField txtAmplitudeMax;
    private JTextField txtOffset;
    private JTextField txtSetPoint;
    private JTextField txtKp;
    private JTextField txtKd;
    private JTextField txtTd;
    private JTextField txtKi;
    private JTextField txtTi;
    private JTextField txtKpE;
    private JTextField txtKdE;
    private JTextField txtTdE;
    private JTextField txtKiE;
    private JTextField txtTiE;
    private JTextField txtAntiWindUp;
    private JTextField txtFiltroDerivativo;
    private JTextField txtAntiWindUpE;
    private JTextField txtFiltroDerivativoE;
    private JTextField txtP1;
    private JTextField txtP2;
    private JTextField txtP3;
    private JTextField txtL1;
    private JTextField txtL2;
    private JTextField txtL3;
    
    private JComboBox comboBoxControladoresE;
    
    private JCheckBox leitura00;
    private JCheckBox leitura01;
    private JCheckBox leitura02;
    private JCheckBox leitura03;
    private JCheckBox leitura04;
    private JCheckBox leitura05;
    private JCheckBox leitura06;
    private JCheckBox leitura07;
    private JCheckBox chkFiltroDerivativo;
    private JCheckBox chkSemFiltroIntegrativo;
    private JCheckBox chkFiltroDerivativoE;
    private JCheckBox chkSemFiltroIntegrativoE;

    private JRadioButton escrita00;
    private JRadioButton escrita01;
    private JRadioButton escrita02;
    private JRadioButton escrita03;
    private JRadioButton escrita04;
    private JRadioButton escrita05;
    private JRadioButton escrita06;
    private JRadioButton escrita07;
    private JRadioButton radioAntiWindup;
    private JRadioButton radioFiltroCondicional;
    private JRadioButton radioAntiWindupE;
    private JRadioButton radioFiltroCondicionalE;
    
    private JFrame frameGrafico;
    JComboBox comboBoxControladores;
    
    private ClienteControle clienteControle;
    private Grafico telaGrafico;
    private ParametrosEntrada parametros;
    
    private boolean parado = true;
    private double kd = 0.0;
    private double ki = 0.0;
    private double kdE = 0.0;
    private double kiE = 0.0;
    
    public boolean parado() {
        return parado;
    }
    
    public String getIp() {
        return txtIP.getText();
    }
    
    public String getPorta() {
        return txtPorta.getText();
    }
    
    public ParametrosEntrada getParametrosEntrada() {
        return parametros;
    }
    
    public void clique_botao_iniciar() {
        if (parado) {
            parado = false;
            iniciarBtn.setText("Parar");
            atualizarBtn.setVisible(true);
            
            clienteControle = new ClienteControle(this, parametros);
            clienteControle.iniciar(this.getIp(), this.getPorta());

            if (telaGrafico != null) {
                telaGrafico.dispose();
                frameGrafico.dispose();
            }
            
            abrirTelaGrafico();
        }
        else
        {
            parado = true;
            iniciarBtn.setText("Iniciar");
            atualizarBtn.setVisible(false);
        }
    }
   
    public boolean aplicarParametros() {
        parametros = new ParametrosEntrada();
        
        try {       
            //Malha
            if (malhaAbertaBtn.isSelected())
                parametros.malha = Malha.Aberta;
            else if (malhaFechadaBtn.isSelected())
                parametros.malha = Malha.Fechada;
            
            //Sinal
            if (sinalDegrauBtn.isSelected())
                parametros.funcao = Funcao.Degrau;
            else if (sinalSenoidalBtn.isSelected())
                parametros.funcao = Funcao.Senoide;
            else if (sinalQuadraticoBtn.isSelected())
                parametros.funcao = Funcao.Quadrada;
            else if (sinalDenteDeSerraBtn.isSelected())
                parametros.funcao = Funcao.DenteDeSerra;
            else if (sinalAleatorioBtn.isSelected())
                parametros.funcao = Funcao.Random;

            //Parâmetros do sinal
            if (!txtAmplitude.getText().isEmpty() && malhaAbertaBtn.isSelected())
                parametros.amplitude = Double.parseDouble(txtAmplitude.getText());
            else if (!txtSetPoint.getText().isEmpty() && (malhaFechadaBtn.isSelected() || obsEstadoBtn.isSelected()))
                parametros.amplitude = Double.parseDouble(txtSetPoint.getText());
            else
                parametros.amplitude = 0;

            if (!txtPeriodo.getText().isEmpty())
                parametros.periodo = Double.parseDouble(txtPeriodo.getText());
            else
                parametros.periodo = 0;

            if (!txtOffset.getText().isEmpty())
                parametros.offset = Double.parseDouble(txtOffset.getText());
            else
                parametros.offset = 0;

            if (!txtPeriodoMin.getText().isEmpty())
                parametros.periodoMin = Double.parseDouble(txtPeriodoMin.getText());
            else
                parametros.periodoMin = 0;

            if (!txtPeriodoMax.getText().isEmpty())
                parametros.periodoMax = Double.parseDouble(txtPeriodoMax.getText());
            else
                parametros.periodoMax = 0;

            if (!txtAmplitudeMin.getText().isEmpty())
                parametros.amplitudeMin = Double.parseDouble(txtAmplitudeMin.getText());
            else
                parametros.amplitudeMin = 0;

            if (!txtAmplitudeMax.getText().isEmpty())
                parametros.amplitudeMax = Double.parseDouble(txtAmplitudeMax.getText());
            else
                parametros.amplitudeMax = 0;

            //Canais de leitura
            parametros.canal0 = leitura00.isSelected();
            parametros.canal1 = leitura01.isSelected();

            //Canal de escrita
            if (escrita07.isSelected())
                parametros.canalEscrita = 7;
            else if (escrita01.isSelected())
                parametros.canalEscrita = 1;
            else if (escrita02.isSelected())
                parametros.canalEscrita = 2;
            else if (escrita03.isSelected())
                parametros.canalEscrita = 3;
            else if (escrita04.isSelected())
                parametros.canalEscrita = 4;
            else if (escrita05.isSelected())
                parametros.canalEscrita = 5;
            else if (escrita06.isSelected())
                parametros.canalEscrita = 6;
            else
                parametros.canalEscrita = 0;

            //PV
            if (configuracao1Btn.isSelected())
                parametros.canalPv = 0;
            else if (configuracao2Btn.isSelected())
                parametros.canalPv = 1;
            else if (configuracao3Btn.isSelected()) {
                parametros.canalPv = 1;
                parametros.controleCascata = true;
            }
            else
                parametros.canalPv = 0;

            //Parâmetros do controlador
            if (!txtKp.getText().isEmpty())
                parametros.kp = Double.parseDouble(txtKp.getText());
            else
                parametros.kp = 0.0;
            
            parametros.kd = kd;
            parametros.ki = ki;
            
            if (!txtKpE.getText().isEmpty())
                parametros.kpE = Double.parseDouble(txtKpE.getText());
            else
                parametros.kpE = 0.0;

            parametros.kdE = kdE;
            parametros.kiE = kiE;

            //Tipo de controlador
            if (comboBoxControladores.getSelectedItem().toString().equals(Controles.P.getName()))
                parametros.controle = Controles.P;
            else if (comboBoxControladores.getSelectedItem().toString().equals(Controles.PD.getName()))
                parametros.controle = Controles.PD;
            else if (comboBoxControladores.getSelectedItem().toString().equals(Controles.PI.getName()))
                parametros.controle = Controles.PI;
            else if (comboBoxControladores.getSelectedItem().toString().equals(Controles.PID.getName()))
                parametros.controle = Controles.PID;
            else if (comboBoxControladores.getSelectedItem().toString().equals(Controles.PI_D.getName()))
                parametros.controle = Controles.PI_D;
            
            if (comboBoxControladoresE.getSelectedItem().toString().equals(Controles.P.getName()))
                parametros.controleE = Controles.P;
            else if (comboBoxControladoresE.getSelectedItem().toString().equals(Controles.PD.getName()))
                parametros.controleE = Controles.PD;
            else if (comboBoxControladoresE.getSelectedItem().toString().equals(Controles.PI.getName()))
                parametros.controleE = Controles.PI;
            else if (comboBoxControladoresE.getSelectedItem().toString().equals(Controles.PID.getName()))
                parametros.controleE = Controles.PID;
            else if (comboBoxControladoresE.getSelectedItem().toString().equals(Controles.PI_D.getName()))
                parametros.controleE = Controles.PI_D;
            
            //Observação de estados
            if (obsEstadoBtn.isSelected()) {
                parametros.obsEstados = true;
                parametros.canalPv = 1;
                
                if (!txtP1.getText().isEmpty() && !txtP2.getText().isEmpty()) {
                    String polo1 = txtP1.getText();
                    String polo2 = txtP2.getText();
                    polo1 = polo1.replace(" ", "");
                    polo2 = polo2.replace(" ", "");

                    if (polo1.indexOf("j") != -1) {
                        String[] p1_ab = polo1.split("j");
                        String p1_a = p1_ab[0].substring(0,p1_ab[0].length()-1);
                        String p1_b = p1_ab[0].charAt(p1_ab[0].length()-1) + p1_ab[1];    
                        parametros.polo1_a = Double.parseDouble(p1_a);
                        parametros.polo1_b = Double.parseDouble(p1_b);
                    }
                    else
                        parametros.polo1_a = Double.parseDouble(polo1);

                    if (polo2.indexOf("j") != -1) {
                        String[] p2_ab = polo2.split("j");
                        String p2_a = p2_ab[0].substring(0,p2_ab[0].length()-1);
                        String p2_b = p2_ab[0].charAt(p2_ab[0].length()-1) + p2_ab[1];    
                        parametros.polo2_a = Double.parseDouble(p2_a);
                        parametros.polo2_b = Double.parseDouble(p2_b);
                    }
                    else
                        parametros.polo2_a = Double.parseDouble(polo2);
                }
                else {
                    try {
                        showPolos();
                    }
                    catch(Exception e) { }
                }    
            }
            
            return true;
        }
        catch(Exception ex) {
            return false;
        }
    }
    
    private void habilitarEntradasControlador() {
        final DecimalFormat decimal = new DecimalFormat("0.000");
        
        if (comboBoxControladores.getSelectedItem().toString().equals("P")) {
            txtKp.setEnabled(true);
            txtKd.setEnabled(false);
            txtKi.setEnabled(false);
            txtTd.setEnabled(false);
            txtTi.setEnabled(false);
            txtTd.setText("");
            txtKi.setText("");
            txtTd.setText("");
            txtTi.setText("");
        }
        else if (comboBoxControladores.getSelectedItem().toString().equals("PD")){
            txtKp.setEnabled(true);
            txtKd.setEnabled(true);
            txtKi.setEnabled(false);
            txtTd.setEnabled(true);
            txtTi.setEnabled(false);
            txtKi.setText("");
            txtTi.setText("");
        }
        else if (comboBoxControladores.getSelectedItem().toString().equals("PI")){
            txtKp.setEnabled(true);
            txtKd.setEnabled(false);
            txtKi.setEnabled(true);
            txtTd.setEnabled(false);
            txtTi.setEnabled(true);
            txtKd.setText("");
            txtTd.setText("");
        }
        else if (comboBoxControladores.getSelectedItem().toString().equals("PID")){
            txtKp.setEnabled(true);
            txtKd.setEnabled(true);
            txtKi.setEnabled(true);
            txtTd.setEnabled(true);
            txtTi.setEnabled(true);                
        }
        else if (comboBoxControladores.getSelectedItem().toString().equals("PI-D")){
            txtKp.setEnabled(true);
            txtKd.setEnabled(true);
            txtKi.setEnabled(true);
            txtTd.setEnabled(true);
            txtTi.setEnabled(true);
        }
        
        
        if (comboBoxControladoresE.getSelectedItem().toString().equals("P")) {
            txtKpE.setEnabled(true);
            txtKdE.setEnabled(false);
            txtKiE.setEnabled(false);
            txtTdE.setEnabled(false);
            txtTiE.setEnabled(false);
            txtTdE.setText("");
            txtKiE.setText("");
            txtTdE.setText("");
            txtTiE.setText("");
        }
        else if (comboBoxControladoresE.getSelectedItem().toString().equals("PD")){
            txtKpE.setEnabled(true);
            txtKdE.setEnabled(true);
            txtKiE.setEnabled(false);
            txtTdE.setEnabled(true);
            txtTiE.setEnabled(false);
            txtKiE.setText("");
            txtTiE.setText("");
        }
        else if (comboBoxControladoresE.getSelectedItem().toString().equals("PI")){
            txtKpE.setEnabled(true);
            txtKdE.setEnabled(false);
            txtKiE.setEnabled(true);
            txtTdE.setEnabled(false);
            txtTiE.setEnabled(true);
            txtKdE.setText("");
            txtTdE.setText("");
        }
        else if (comboBoxControladoresE.getSelectedItem().toString().equals("PID")){
            txtKpE.setEnabled(true);
            txtKdE.setEnabled(true);
            txtKiE.setEnabled(true);
            txtTdE.setEnabled(true);
            txtTiE.setEnabled(true);                
        }
        else if (comboBoxControladoresE.getSelectedItem().toString().equals("PI-D")){
            txtKpE.setEnabled(true);
            txtKdE.setEnabled(true);
            txtKiE.setEnabled(true);
            txtTdE.setEnabled(true);
            txtTiE.setEnabled(true);
        }
        
        
        txtTd.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                if (txtKd.getText().isEmpty() && !txtTd.getText().isEmpty()) {
                    double kp = Double.parseDouble(txtKp.getText());
                    double td = Double.parseDouble(txtTd.getText());
                    kd = kp * td;
                    txtKd.setText(String.valueOf(decimal.format(kd)).replace(",", "."));
                }
            }
        });

        txtKd.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }

            @Override
            public void focusLost(FocusEvent fe) {
                if (txtTd.getText().isEmpty() && !txtKd.getText().isEmpty()) {
                    double kp = Double.parseDouble(txtKp.getText());
                    kd = Double.parseDouble(txtKd.getText());
                    double td = kd / kp;
                    txtTd.setText(String.valueOf(decimal.format(td)).replace(",", "."));
                }
            }
        });

        txtTi.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                if (txtKi.getText().isEmpty() && !txtTi.getText().isEmpty()) {
                    double kp = Double.parseDouble(txtKp.getText());
                    double ti = Double.parseDouble(txtTi.getText());
                    ki = kp / ti;
                    txtKi.setText(String.valueOf(decimal.format(ki)).replace(",", "."));
                }
            }
        });

        txtKi.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }

            @Override
            public void focusLost(FocusEvent fe) {
                if (txtTi.getText().isEmpty() && !txtKi.getText().isEmpty()) {
                    double kp = Double.parseDouble(txtKp.getText());
                    ki = Double.parseDouble(txtKi.getText());
                    double ti = kp / ki;
                    txtTi.setText(String.valueOf(decimal.format(ti)).replace(",", "."));
                }
            }
        });
        
        txtTdE.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                if (txtKdE.getText().isEmpty() && !txtTdE.getText().isEmpty()) {
                    double kpE = Double.parseDouble(txtKpE.getText());
                    double tdE = Double.parseDouble(txtTdE.getText());
                    kdE = kpE * tdE;
                    txtKdE.setText(String.valueOf(decimal.format(kdE)).replace(",", "."));
                }
            }
        });

        txtKdE.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }

            @Override
            public void focusLost(FocusEvent fe) {
                if (txtTdE.getText().isEmpty() && !txtKdE.getText().isEmpty()) {
                    double kpE = Double.parseDouble(txtKpE.getText());
                    kdE = Double.parseDouble(txtKdE.getText());
                    double tdE = kdE / kpE;
                    txtTdE.setText(String.valueOf(decimal.format(tdE)).replace(",", "."));
                }
            }
        });

        txtTiE.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                if (txtKiE.getText().isEmpty() && !txtTiE.getText().isEmpty()) {
                    double kpE = Double.parseDouble(txtKpE.getText());
                    double tiE = Double.parseDouble(txtTiE.getText());
                    kiE = kpE / tiE;
                    txtKiE.setText(String.valueOf(decimal.format(kiE)).replace(",", "."));
                }
            }
        });

        txtKiE.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }

            @Override
            public void focusLost(FocusEvent fe) {
                if (txtTiE.getText().isEmpty() && !txtKiE.getText().isEmpty()) {
                    double kpE = Double.parseDouble(txtKpE.getText());
                    kiE = Double.parseDouble(txtKiE.getText());
                    double tiE = kpE / kiE;
                    txtTiE.setText(String.valueOf(decimal.format(tiE)).replace(",", "."));
                }
            }
        });
    }
    
    private void showGanhos() {
        if (!txtP1.getText().isEmpty() && !txtP2.getText().isEmpty() && txtL1.getText().isEmpty() && txtL2.getText().isEmpty()) {
            final DecimalFormat decimal = new DecimalFormat("0.00");
            double polo1_a = 0.0;
            double polo1_b = 0.0;
            double polo2_a = 0.0;
            double polo2_b = 0.0;

            String polo1 = txtP1.getText();
            String polo2 = txtP2.getText();
            polo1 = polo1.replace(" ", "");
            polo2 = polo2.replace(" ", "");

            if (polo1.indexOf("j") != -1) {
                String[] p1_ab = polo1.split("j");
                String p1_a = p1_ab[0].substring(0,p1_ab[0].length()-1);
                String p1_b = p1_ab[0].charAt(p1_ab[0].length()-1) + p1_ab[1];    
                polo1_a = Double.parseDouble(p1_a);
                polo1_b = Double.parseDouble(p1_b);
            }
            else
                polo1_a = Double.parseDouble(polo1);

            if (polo2.indexOf("j") != -1) {
                String[] p2_ab = polo2.split("j");
                String p2_a = p2_ab[0].substring(0,p2_ab[0].length()-1);
                String p2_b = p2_ab[0].charAt(p2_ab[0].length()-1) + p2_ab[1];    
                polo2_a = Double.parseDouble(p2_a);
                polo2_b = Double.parseDouble(p2_b);
            }
            else
                polo2_a = Double.parseDouble(polo2);

            double[] ganhos = new ObservadorEstados(polo1_a, polo1_b, polo2_a, polo2_b).getL();
            txtL1.setText(String.valueOf(decimal.format(ganhos[0])).replace(",", "."));
            txtL2.setText(String.valueOf(decimal.format(ganhos[1])).replace(",", "."));
        }
    }
    
    private void showPolos() {
        if (!txtL1.getText().isEmpty() && !txtL2.getText().isEmpty() && txtP1.getText().isEmpty() && txtP2.getText().isEmpty()) {
            final DecimalFormat decimal = new DecimalFormat("0.00");
            double l1 = Double.valueOf(txtL1.getText());
            double l2 = Double.valueOf(txtL2.getText());        
            ObservadorEstados objPolos = new ObservadorEstados(0, 0, 0, 0);
            double[] polos = objPolos.getPolos(l1,l2);

            if (polos[1] >= 0)
                txtP1.setText(String.valueOf(decimal.format(polos[0]).replace(",", ".") + "+j" + String.valueOf(decimal.format(polos[1])).replace(",", ".")));
            else
                txtP1.setText(String.valueOf(decimal.format(polos[0]).replace(",", ".") + "-j" + String.valueOf(decimal.format(-1*polos[1])).replace(",", ".")));

            if (polos[3] >= 0)
                txtP2.setText(String.valueOf(decimal.format(polos[2]).replace(",", ".") + "+j" + String.valueOf(decimal.format(polos[3])).replace(",", ".")));
            else
                txtP2.setText(String.valueOf(decimal.format(polos[2]).replace(",", ".") + "-j" + String.valueOf(decimal.format(-1*polos[3])).replace(",", ".")));
        }
    }
    
    
    public TelaCliente() {
        initComponents();
        
        addWindowListener(  
            new WindowAdapter()   
            {
                public void windowClosing(WindowEvent we) {
                    if (!parado)
                        clique_botao_iniciar();
                }
            }
        );
    }

    public JPanel createContentPane(){
        
        // We create a bottom JPanel to place everything on.
        GradientPanel totalGUI = new GradientPanel();
        totalGUI.setLayout(null);
        
        // Panel para a TitleBar
        JPanel titleBarPanel = new JPanel();
        titleBarPanel.setLayout(null);
        titleBarPanel.setLocation(0, 0);
        titleBarPanel.setSize(900, 32);
        titleBarPanel.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarPanel);
        
        //Imagem thumb
        JButton thumbnail = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/thumbnail.png"));
            thumbnail.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        thumbnail.setLocation(6,2);
        thumbnail.setSize(24, 27);
        thumbnail.setBorderPainted(false); 
        thumbnail.setContentAreaFilled(false); 
        thumbnail.setFocusPainted(false); 
        thumbnail.setOpaque(false);
        
        titleBarPanel.add(thumbnail);
        
        // Título
        JLabel titleLabel = new JLabel("Sistema de Controle de Tanques");
        titleLabel.setLocation(32, 0);
        titleLabel.setSize(200, 32);
        titleBarPanel.add(titleLabel);    
        
        //Adicionando Botão de Fechar
        JButton closeButton = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/closeButton.png"));
            closeButton.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        closeButton.setLocation(866,4);
        closeButton.setSize(24, 24);
        closeButton.setBorderPainted(false); 
        closeButton.setContentAreaFilled(false); 
        closeButton.setFocusPainted(false); 
        closeButton.setOpaque(false);
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        titleBarPanel.add(closeButton);

        // Painel de portas de Leitura.
        
        titleBarLeitura = new JPanel();
        titleBarLeitura.setLayout(null);
        titleBarLeitura.setLocation(644, 40);
        titleBarLeitura.setSize(120, 32);
        titleBarLeitura.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarLeitura);
        
        titleLeitura = new JLabel("Portas de Leitura");
        titleLeitura.setLocation(10, 0);
        titleLeitura.setSize(200, 32);
        titleBarLeitura.add(titleLeitura);  
        
        panelLeitura = new JPanel();
        panelLeitura.setOpaque(true);
        panelLeitura.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelLeitura.setLayout(new BoxLayout(panelLeitura, BoxLayout.PAGE_AXIS));
        panelLeitura.setLocation(644, 72);
        panelLeitura.setSize(120, 207);
        panelLeitura.add(Box.createRigidArea(new Dimension(10,0)));
        
        leitura00 = new JCheckBox("Canal 00");
        leitura00.setSelected(true);
        panelLeitura.add(leitura00);
        panelLeitura.add(Box.createHorizontalGlue());
        
        leitura01 = new JCheckBox("Canal 01");
        panelLeitura.add(leitura01);
        panelLeitura.add(Box.createHorizontalGlue());
        
        leitura02 = new JCheckBox("Canal 02");
        panelLeitura.add(leitura02);
        panelLeitura.add(Box.createHorizontalGlue());
        
        leitura03 = new JCheckBox("Canal 03");
        panelLeitura.add(leitura03);
        panelLeitura.add(Box.createRigidArea(new Dimension(10, 0)));
        
        leitura04 = new JCheckBox("Canal 04");
        panelLeitura.add(leitura04);
        panelLeitura.add(Box.createRigidArea(new Dimension(10, 0)));
        
        leitura05 = new JCheckBox("Canal 05");
        panelLeitura.add(leitura05);
        panelLeitura.add(Box.createRigidArea(new Dimension(10, 0)));
        
        leitura06 = new JCheckBox("Canal 06");
        panelLeitura.add(leitura06);
        panelLeitura.add(Box.createRigidArea(new Dimension(10, 0)));
        
        leitura07 = new JCheckBox("Canal 07");
        panelLeitura.add(leitura07);
        panelLeitura.add(Box.createRigidArea(new Dimension(10, 0))); 
        
        totalGUI.add(panelLeitura);
        
        // Painel de portas de Escrita.       
        
        titleBarEscrita = new JPanel();
        titleBarEscrita.setLayout(null);
        titleBarEscrita.setLocation(770, 40);
        titleBarEscrita.setSize(120, 32);
        titleBarEscrita.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarEscrita);
        
        titleEscrita = new JLabel("Portas de Escrita");
        titleEscrita.setLocation(10, 0);
        titleEscrita.setSize(200, 32);
        titleBarEscrita.add(titleEscrita);
        
        String escrita[] = {"Canal 00", "Canal 01", "Canal 02", "Canal 03", "Canal 04", "Canal 05", "Canal 06", "Canal 07"};
        
        panelEscrita = new JPanel();  
        panelEscrita.setOpaque(true);
        panelEscrita.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelEscrita.setLayout(new BoxLayout(panelEscrita, BoxLayout.PAGE_AXIS));
        panelEscrita.setLocation(770, 72);
        panelEscrita.setSize(120, 207);
        panelEscrita.add(Box.createRigidArea(new Dimension(10,0)));
        
        escrita00 = new JRadioButton(escrita[0]);
        escrita00.setSelected(true);
        escrita01 = new JRadioButton(escrita[1]);
        escrita01.setSelected(true);
        escrita02 = new JRadioButton(escrita[2]);
        escrita02.setSelected(true);
        escrita03 = new JRadioButton(escrita[3]);
        escrita03.setSelected(true);
        escrita04 = new JRadioButton(escrita[4]);
        escrita04.setSelected(true);
        escrita05 = new JRadioButton(escrita[5]);
        escrita05.setSelected(true);
        escrita06 = new JRadioButton(escrita[6]);
        escrita06.setSelected(true);
        escrita07 = new JRadioButton(escrita[7]);
        escrita07.setSelected(true);
        
        ButtonGroup grupoEscrita = new ButtonGroup();
        grupoEscrita.add(escrita00);
        grupoEscrita.add(escrita01);
        grupoEscrita.add(escrita02);
        grupoEscrita.add(escrita03);
        grupoEscrita.add(escrita04);
        grupoEscrita.add(escrita05);
        grupoEscrita.add(escrita06);
        grupoEscrita.add(escrita07);
        
        panelEscrita.add(Box.createRigidArea(new Dimension(0,0)));
        panelEscrita.add(escrita00);
        panelEscrita.add(Box.createHorizontalGlue());
        panelEscrita.add(escrita01);
        panelEscrita.add(Box.createHorizontalGlue());
        panelEscrita.add(escrita02);
        panelEscrita.add(Box.createHorizontalGlue());
        panelEscrita.add(escrita03);
        panelEscrita.add(Box.createHorizontalGlue());
        panelEscrita.add(escrita04);
        panelEscrita.add(Box.createHorizontalGlue());
        panelEscrita.add(escrita05);
        panelEscrita.add(Box.createHorizontalGlue());
        panelEscrita.add(escrita06);
        panelEscrita.add(Box.createHorizontalGlue());
        panelEscrita.add(escrita07);
        panelEscrita.add(Box.createRigidArea(new Dimension(10,10)));
        
        totalGUI.add(panelEscrita);
        
        //Malhas
        
        titleBarMalha = new JPanel();
        titleBarMalha.setLayout(null);
        titleBarMalha.setLocation(10, 40);
        titleBarMalha.setSize(630, 32);
        titleBarMalha.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarMalha);
        
        titleMalha = new JLabel("Malhas");
        titleMalha.setLocation(10, 0);
        titleMalha.setSize(200, 32);
        titleBarMalha.add(titleMalha);
        
        panelMalha = new JPanel();  
        panelMalha.setOpaque(true);
        panelMalha.setLayout(null);
        panelMalha.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelMalha.setLocation(10, 72);
        panelMalha.setSize(630, 85);
        
        malhaAbertaBtn = new JToggleButton("Malha Aberta");
        malhaAbertaBtn.setLocation(10, 25);
        malhaAbertaBtn.setSize(110, 30);
        malhaAbertaBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                malhaAbertaBtnActionPerformed(evt);
            }
        });
        panelMalha.add(malhaAbertaBtn);
        
        malhaFechadaBtn = new JToggleButton("Malha Fechada");
        malhaFechadaBtn.setLocation(130, 25);
        malhaFechadaBtn.setSize(120, 30);
        malhaFechadaBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                malhaFechadaBtnActionPerformed(evt);
            }
        });
        panelMalha.add(malhaFechadaBtn);
        
        obsEstadoBtn = new JToggleButton("Observação de Estados");
        obsEstadoBtn.setLocation(260, 25);
        obsEstadoBtn.setSize(170, 30);
        obsEstadoBtn.setVisible(true);
        //obsEstadoBtn.setEnabled(true);
        obsEstadoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                obsEstadoBtnActionPerformed(evt);
            }
        });
        panelMalha.add(obsEstadoBtn);
        
        segReferenciaBtn = new JToggleButton("Seguidor de Referência");
        segReferenciaBtn.setLocation(440, 25);
        segReferenciaBtn.setSize(180, 30);
        segReferenciaBtn.setVisible(true);
        //segReferenciaBtn.setEnabled(true);
        segReferenciaBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                segReferenciaBtnActionPerformed(evt);
            }
        });
        panelMalha.add(segReferenciaBtn);
        
        ButtonGroup grupoMalha = new ButtonGroup();
        grupoMalha.add(malhaAbertaBtn);
        grupoMalha.add(malhaFechadaBtn);
        grupoMalha.add(obsEstadoBtn);
        grupoMalha.add(segReferenciaBtn);
        
        totalGUI.add(panelMalha);
        
                //Configurações
        
        titleBarConfiguracoes = new JPanel();
        titleBarConfiguracoes.setLayout(null);
        titleBarConfiguracoes.setLocation(644, 40);
        titleBarConfiguracoes.setSize(246, 32);
        titleBarConfiguracoes.setVisible(false);
        titleBarConfiguracoes.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarConfiguracoes);
        
        titleConfiguracoes = new JLabel("Configurações");
        titleConfiguracoes.setLocation(10, 0);
        titleConfiguracoes.setSize(200, 32);
        titleConfiguracoes.setVisible(false);
        titleBarConfiguracoes.add(titleConfiguracoes);
        
        panelConfiguracoes = new JPanel();  
        panelConfiguracoes.setOpaque(true);
        panelConfiguracoes.setLayout(null);
        panelConfiguracoes.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelConfiguracoes.setLocation(644, 72);
        panelConfiguracoes.setSize(246, 207);
        panelConfiguracoes.setVisible(false);
        
        configuracao1Btn = new JToggleButton("Controle Tanque Superior");
        configuracao1Btn.setLocation(35, 25);
        configuracao1Btn.setSize(180, 30);
        configuracao1Btn.setVisible(false);
        configuracao1Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configuracao1BtnActionPerformed(evt);
            }
        });
        panelConfiguracoes.add(configuracao1Btn);
        
        configuracao2Btn = new JToggleButton("Controle Tanque Inferior");
        configuracao2Btn.setLocation(35, 85);
        configuracao2Btn.setSize(180, 30);
        configuracao2Btn.setVisible(false);
        configuracao2Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configuracao2BtnActionPerformed(evt);
            }
        });
        panelConfiguracoes.add(configuracao2Btn);
        
        configuracao3Btn = new JToggleButton("Controle em Cascata");
        configuracao3Btn.setLocation(35, 145);
        configuracao3Btn.setSize(180, 30);
        configuracao3Btn.setVisible(false);
        //configuracao3Btn.setEnabled(true);
        configuracao3Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configuracao3BtnActionPerformed(evt);
            }
        });
        panelConfiguracoes.add(configuracao3Btn);
        
        ButtonGroup grupoConfiguracoes = new ButtonGroup();
        grupoConfiguracoes.add(configuracao1Btn);
        grupoConfiguracoes.add(configuracao2Btn);
        grupoConfiguracoes.add(configuracao3Btn);
        
        totalGUI.add(panelConfiguracoes);
        
        //Sinais
        
        titleBarSinal = new JPanel();
        titleBarSinal.setLayout(null);
        titleBarSinal.setLocation(10, 162);
        titleBarSinal.setSize(630, 32);
        titleBarSinal.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarSinal);
        
        titleSinal = new JLabel("Sinais");
        titleSinal.setLocation(10, 0);
        titleSinal.setSize(200, 32);
        titleBarSinal.add(titleSinal);
        
        panelSinal = new JPanel();  
        panelSinal.setOpaque(true);
        panelSinal.setLayout(null);
        panelSinal.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelSinal.setLocation(10, 194);
        panelSinal.setSize(630, 85);
        
        sinalDegrauBtn = new JToggleButton("Degrau");
        sinalDegrauBtn.setLocation(120, 10);
        sinalDegrauBtn.setSize(130, 30);
        sinalDegrauBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sinalDegrauBtnActionPerformed(evt);
            }
        });
        
        panelSinal.add(sinalDegrauBtn);
        
        sinalSenoidalBtn = new JToggleButton("Senoidal");
        sinalSenoidalBtn.setLocation(260, 10);
        sinalSenoidalBtn.setSize(130, 30);
        sinalSenoidalBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sinalSenoidalBtnActionPerformed(evt);
            }
        });
        
        panelSinal.add(sinalSenoidalBtn);
        
        sinalQuadraticoBtn = new JToggleButton("Quadrático");
        sinalQuadraticoBtn.setLocation(400, 10);
        sinalQuadraticoBtn.setSize(130, 30);
        sinalQuadraticoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sinalQuadraticoBtnActionPerformed(evt);
            }
        });
        
        panelSinal.add(sinalQuadraticoBtn);
        
        sinalDenteDeSerraBtn = new JToggleButton("Dente de Serra");
        sinalDenteDeSerraBtn.setLocation(190, 50);
        sinalDenteDeSerraBtn.setSize(130, 30);
        sinalDenteDeSerraBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sinalDenteDeSerraBtnActionPerformed(evt);
            }
        });
        
        panelSinal.add(sinalDenteDeSerraBtn);
        
        sinalAleatorioBtn = new JToggleButton("Aleatório");
        sinalAleatorioBtn.setLocation(330, 50);
        sinalAleatorioBtn.setSize(130, 30);
        sinalAleatorioBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sinalAleatorioBtnActionPerformed(evt);
            }
        });
        
        panelSinal.add(sinalAleatorioBtn);
        
        ButtonGroup grupoSinal = new ButtonGroup();
        grupoSinal.add(sinalDegrauBtn);
        grupoSinal.add(sinalSenoidalBtn);
        grupoSinal.add(sinalQuadraticoBtn);
        grupoSinal.add(sinalDenteDeSerraBtn);
        grupoSinal.add(sinalAleatorioBtn);
        
        totalGUI.add(panelSinal);
        
        //Dados
        
        JPanel titleBarDados = new JPanel();
        titleBarDados.setLayout(null);
        titleBarDados.setLocation(10, 555);
        titleBarDados.setSize(565, 32);
        titleBarDados.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarDados);
        
        JLabel titleDados = new JLabel("Dados");
        titleDados.setLocation(10, 0);
        titleDados.setSize(200, 32);
        titleBarDados.add(titleDados);
        
        JPanel panelDados = new JPanel();  
        panelDados.setOpaque(true);
        panelDados.setLayout(null);
        panelDados.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelDados.setLocation(10, 587);
        panelDados.setSize(565, 100);
        
        lblAmplitude = new JLabel("Amplitude");
        lblAmplitude.setLocation(0, 10);
        lblAmplitude.setSize(80, 40);
        lblAmplitude.setHorizontalAlignment(0);
        panelDados.add(lblAmplitude);
        
        txtAmplitude = new JTextField(8);
        txtAmplitude.setLocation(80, 20);
        txtAmplitude.setSize(40, 22);
        panelDados.add(txtAmplitude);
        
        lblSetPoint = new JLabel("SetPoint");
        lblSetPoint.setLocation(0, 10);
        lblSetPoint.setSize(80, 40);
        lblSetPoint.setHorizontalAlignment(0);
        lblSetPoint.setVisible(false);
        panelDados.add(lblSetPoint);
        
        txtSetPoint = new JTextField(8);
        txtSetPoint.setLocation(80, 20);
        txtSetPoint.setSize(40, 22);
        txtSetPoint.setVisible(false);
        panelDados.add(txtSetPoint);
        
        notificacao1 = new JLabel("Por favor, utilizar valores entre -3 e 3V.");
        notificacao1.setLocation(0, 30);
        notificacao1.setSize(285, 40);
        notificacao1.setHorizontalAlignment(0);
        notificacao1.setEnabled(false);
        notificacao1.setVisible(false);
        panelDados.add(notificacao1);
        
        lblOffset = new JLabel("Offset");
        lblOffset.setLocation(120, 10);
        lblOffset.setSize(60, 40);
        lblOffset.setHorizontalAlignment(0);
        panelDados.add(lblOffset);
        
        txtOffset = new JTextField(8);
        txtOffset.setLocation(190, 20);
        txtOffset.setSize(40, 22);
        panelDados.add(txtOffset);
        
        lblAmplitudeMin = new JLabel("Amplitude Mín.");
        lblAmplitudeMin.setLocation(0, 10);
        lblAmplitudeMin.setSize(100, 40);
        lblAmplitudeMin.setHorizontalAlignment(0);
        lblAmplitudeMin.setVisible(false);
        panelDados.add(lblAmplitudeMin);
        
        txtAmplitudeMin = new JTextField(8);
        txtAmplitudeMin.setLocation(100, 20);
        txtAmplitudeMin.setSize(40, 22);
        txtAmplitudeMin.setVisible(false);
        panelDados.add(txtAmplitudeMin);
        
        lblAmplitudeMax = new JLabel("Amplitude Máx.");
        lblAmplitudeMax.setLocation(140, 10);
        lblAmplitudeMax.setSize(100, 40);
        lblAmplitudeMax.setHorizontalAlignment(0);
        lblAmplitudeMax.setVisible(false);
        panelDados.add(lblAmplitudeMax);
        
        txtAmplitudeMax = new JTextField(8);
        txtAmplitudeMax.setLocation(240, 20);
        txtAmplitudeMax.setSize(40, 22);
        txtAmplitudeMax.setVisible(false);
        panelDados.add(txtAmplitudeMax);
        
        notificacao2 = new JLabel("Por favor, utilizar valores entre 0 e 3V.");
        notificacao2.setLocation(0, 30);
        notificacao2.setSize(285, 40);
        notificacao2.setHorizontalAlignment(0);
        notificacao2.setEnabled(false);
        notificacao2.setVisible(false);
        panelDados.add(notificacao2);
        
        lblPeriodo = new JLabel("Período");
        lblPeriodo.setLocation(280, 10);
        lblPeriodo.setSize(70, 40);
        lblPeriodo.setHorizontalAlignment(0);
        panelDados.add(lblPeriodo);
        
        txtPeriodo = new JTextField(8);
        txtPeriodo.setLocation(360, 20);
        txtPeriodo.setSize(40, 22);
        panelDados.add(txtPeriodo);
        
        lblPeriodoMin = new JLabel("Período Mín.");
        lblPeriodoMin.setLocation(280, 10);
        lblPeriodoMin.setSize(100, 40);
        lblPeriodoMin.setHorizontalAlignment(0);
        lblPeriodoMin.setVisible(false);
        panelDados.add(lblPeriodoMin);
        
        txtPeriodoMin = new JTextField(8);
        txtPeriodoMin.setLocation(370, 20);
        txtPeriodoMin.setSize(40, 22);
        txtPeriodoMin.setVisible(false);
        panelDados.add(txtPeriodoMin);
        
        lblPeriodoMax = new JLabel("Período Máx.");
        lblPeriodoMax.setLocation(400, 10);
        lblPeriodoMax.setSize(100, 40);
        lblPeriodoMax.setHorizontalAlignment(0);
        lblPeriodoMax.setVisible(false);
        panelDados.add(lblPeriodoMax);
        
        txtPeriodoMax = new JTextField(8);
        txtPeriodoMax.setLocation(490, 20);
        txtPeriodoMax.setSize(40, 22);
        txtPeriodoMax.setVisible(false);
        panelDados.add(txtPeriodoMax);
        
        notificacao3 = new JLabel("Por favor, utilizar valores em segundos.");
        notificacao3.setLocation(280, 30);
        notificacao3.setSize(285, 40);
        notificacao3.setHorizontalAlignment(0);
        notificacao3.setEnabled(false);
        notificacao3.setVisible(false);
        panelDados.add(notificacao3);
        
        /*String canais[] = {"Canal 00", "Canal 01", "Canal 02", "Canal 03", "Canal 04", "Canal 05", "Canal 06", "Canal 07"};
        JComboBox comboBoxPV = new JComboBox(canais);
        comboBoxPV.setSelectedIndex(0);
        comboBoxPV.setLocation(200, 15);
        comboBoxPV.setSize(80, 30);
        panelDados.add(comboBoxPV);
        
        JLabel lblPV = new JLabel("PV");
        lblPV.setLocation(170, 15);
        lblPV.setSize(30, 30);
        panelDados.add(lblPV);*/
        
        totalGUI.add(panelDados);

        //Controlador
        
        titleBarControlador = new JPanel();
        titleBarControlador.setLayout(null);
        titleBarControlador.setLocation(10, 285);
        titleBarControlador.setSize(880, 32);
        titleBarControlador.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarControlador);
        
        titleControlador = new JLabel("Controlador");
        titleControlador.setLocation(10, 0);
        titleControlador.setSize(200, 32);
        titleBarControlador.add(titleControlador);
        
        titleBarMestre = new JPanel();
        titleBarMestre.setLayout(null);
        titleBarMestre.setLocation(20, 327);
        titleBarMestre.setSize(425, 32);
        titleBarMestre.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarMestre);
        
        titleMestre = new JLabel("Mestre");
        titleMestre.setLocation(10, 0);
        titleMestre.setSize(200, 32);
        titleBarMestre.add(titleMestre);
        
        titleBarEscravo = new JPanel();
        titleBarEscravo.setLayout(null);
        titleBarEscravo.setLocation(450, 327);
        titleBarEscravo.setSize(435, 32);
        titleBarEscravo.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarEscravo);
        
        titleEscravo = new JLabel("Escravo");
        titleEscravo.setLocation(10, 0);
        titleEscravo.setSize(200, 32);
        titleBarEscravo.add(titleEscravo);
        
        panelControlador = new JPanel();  
        panelControlador.setOpaque(true);
        panelControlador.setLayout(null);
        panelControlador.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelControlador.setLocation(10, 317);
        panelControlador.setSize(880, 230);
        
        String controladores[] = {"P", "PD", "PI", "PID", "PI-D"};
        comboBoxControladores = new JComboBox(controladores);
        comboBoxControladores.setSelectedIndex(0);
        comboBoxControladores.setLocation(132, 52);
        comboBoxControladores.setSize(50, 30);
        comboBoxControladores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxControladoresActionPerformed(evt);
            }
        });
        panelControlador.add(comboBoxControladores);
        
        lblControladores = new JLabel("Tipo de Controlador");
        lblControladores.setLocation(12, 52);
        lblControladores.setSize(130, 30);
        panelControlador.add(lblControladores);
        
        lblKp = new JLabel("Kp");
        lblKp.setLocation(0, 82);
        lblKp.setSize(40, 40);
        lblKp.setHorizontalAlignment(0);
        panelControlador.add(lblKp);
        
        txtKp = new JTextField(8);
        txtKp.setLocation(50, 92);
        txtKp.setSize(40, 22);
        panelControlador.add(txtKp);
        
        lblKi = new JLabel("Ki");
        lblKi.setLocation(0, 112);
        lblKi.setSize(40, 40);
        lblKi.setHorizontalAlignment(0);
        panelControlador.add(lblKi);
        
        txtKi = new JTextField(8);
        txtKi.setLocation(50, 122);
        txtKi.setSize(40, 22);
        panelControlador.add(txtKi);
        
        lblTi = new JLabel("Ti");
        lblTi.setLocation(90, 112);
        lblTi.setSize(40, 40);
        lblTi.setHorizontalAlignment(0);
        panelControlador.add(lblTi);
        
        txtTi = new JTextField(8);
        txtTi.setLocation(130, 122);
        txtTi.setSize(40, 22);
        panelControlador.add(txtTi);
        
        lblKd = new JLabel("Kd");
        lblKd.setLocation(0, 142);
        lblKd.setSize(40, 40);
        lblKd.setHorizontalAlignment(0);
        panelControlador.add(lblKd);
        
        txtKd = new JTextField(8);
        txtKd.setLocation(50, 152);
        txtKd.setSize(40, 22);
        panelControlador.add(txtKd);
        
        lblTd = new JLabel("Td");
        lblTd.setLocation(90, 142);
        lblTd.setSize(40, 40);
        lblTd.setHorizontalAlignment(0);
        panelControlador.add(lblTd);
        
        txtTd = new JTextField(8);
        txtTd.setLocation(130, 152);
        txtTd.setSize(40, 22);
        panelControlador.add(txtTd);
        
        radioAntiWindup = new JRadioButton();
        radioAntiWindup.setLocation(190, 45);
        radioAntiWindup.setSize(100, 22);
        radioAntiWindup.setVisible(true);
        radioAntiWindup.setText("Anti-Windup");
        panelControlador.add(radioAntiWindup);
        
        radioFiltroCondicional = new JRadioButton();
        radioFiltroCondicional.setLocation(190, 65);
        radioFiltroCondicional.setSize(130, 22);
        radioFiltroCondicional.setVisible(true);
        radioFiltroCondicional.setText("Filtro Condicional");
        panelControlador.add(radioFiltroCondicional);
        
        chkFiltroDerivativo = new JCheckBox();
        chkFiltroDerivativo.setLocation(190, 85);
        chkFiltroDerivativo.setSize(130, 22);
        chkFiltroDerivativo.setVisible(true);
        chkFiltroDerivativo.setText("Filtro Derivativo");
        panelControlador.add(chkFiltroDerivativo);
        
        chkSemFiltroIntegrativo = new JCheckBox();
        chkSemFiltroIntegrativo.setLocation(190, 105);
        chkSemFiltroIntegrativo.setSize(150, 22);
        chkSemFiltroIntegrativo.setVisible(true);
        chkSemFiltroIntegrativo.setText("Sem Filtro Derivativo");
        panelControlador.add(chkSemFiltroIntegrativo);
        
        lblFiltroDerivativo = new JLabel("Coeficiente de Tempo do Filtro Derivativo");
        lblFiltroDerivativo.setLocation(192, 162);
        lblFiltroDerivativo.setSize(250, 40);
        lblFiltroDerivativo.setVisible(true);
        panelControlador.add(lblFiltroDerivativo);
        
        txtFiltroDerivativo = new JTextField(8);
        txtFiltroDerivativo.setLocation(195, 196);
        txtFiltroDerivativo.setSize(40, 22);
        txtFiltroDerivativo.setVisible(true);
        panelControlador.add(txtFiltroDerivativo);
        
        lblAntiWindUp = new JLabel("Coeficiente de Tempo do Filtro Anti-Windup");
        lblAntiWindUp.setLocation(192, 118);
        lblAntiWindUp.setSize(250, 40);
        lblAntiWindUp.setVisible(true);
        panelControlador.add(lblAntiWindUp);
        
        txtAntiWindUp = new JTextField(8);
        txtAntiWindUp.setLocation(195, 152);
        txtAntiWindUp.setSize(40, 22);
        txtAntiWindUp.setVisible(true);
        panelControlador.add(txtAntiWindUp);
        
        comboBoxControladoresE = new JComboBox(controladores);
        comboBoxControladoresE.setSelectedIndex(0);
        comboBoxControladoresE.setLocation(570, 52);
        comboBoxControladoresE.setSize(50, 30);
        comboBoxControladoresE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxControladoresEActionPerformed(evt);
            }
        });
        panelControlador.add(comboBoxControladoresE);
        
        lblControladoresE = new JLabel("Tipo de Controlador");
        lblControladoresE.setLocation(445, 52);
        lblControladoresE.setSize(130, 30);
        panelControlador.add(lblControladoresE);

        lblKpE = new JLabel("Kp");
        lblKpE.setLocation(435, 82);
        lblKpE.setSize(40, 40);
        lblKpE.setHorizontalAlignment(0);
        panelControlador.add(lblKpE);
        
        txtKpE = new JTextField(8);
        txtKpE.setLocation(485, 92);
        txtKpE.setSize(40, 22);
        panelControlador.add(txtKpE);
        
        lblKiE = new JLabel("Ki");
        lblKiE.setLocation(435, 112);
        lblKiE.setSize(40, 40);
        lblKiE.setHorizontalAlignment(0);
        panelControlador.add(lblKiE);
        
        txtKiE = new JTextField(8);
        txtKiE.setLocation(485, 122);
        txtKiE.setSize(40, 22);
        panelControlador.add(txtKiE);
        
        lblTiE = new JLabel("Ti");
        lblTiE.setLocation(525, 112);
        lblTiE.setSize(40, 40);
        lblTiE.setHorizontalAlignment(0);
        panelControlador.add(lblTiE);
        
        txtTiE = new JTextField(8);
        txtTiE.setLocation(565, 122);
        txtTiE.setSize(40, 22);
        panelControlador.add(txtTiE);
        
        lblKdE = new JLabel("Kd");
        lblKdE.setLocation(435, 142);
        lblKdE.setSize(40, 40);
        lblKdE.setHorizontalAlignment(0);
        panelControlador.add(lblKdE);
        
        txtKdE = new JTextField(8);
        txtKdE.setLocation(485, 152);
        txtKdE.setSize(40, 22);
        panelControlador.add(txtKdE);
        
        lblTdE = new JLabel("Td");
        lblTdE.setLocation(525, 142);
        lblTdE.setSize(40, 40);
        lblTdE.setHorizontalAlignment(0);
        panelControlador.add(lblTdE);
        
        txtTdE = new JTextField(8);
        txtTdE.setLocation(565, 152);
        txtTdE.setSize(40, 22);
        panelControlador.add(txtTdE);

        radioAntiWindupE = new JRadioButton();
        radioAntiWindupE.setLocation(630, 45);
        radioAntiWindupE.setSize(100, 22);
        radioAntiWindupE.setVisible(true);
        radioAntiWindupE.setText("Anti-Windup");
        panelControlador.add(radioAntiWindupE);
        
        radioFiltroCondicionalE = new JRadioButton();
        radioFiltroCondicionalE.setLocation(630, 65);
        radioFiltroCondicionalE.setSize(130, 22);
        radioFiltroCondicionalE.setVisible(true);
        radioFiltroCondicionalE.setText("Filtro Condicional");
        panelControlador.add(radioFiltroCondicionalE);
        
        chkFiltroDerivativoE = new JCheckBox();
        chkFiltroDerivativoE.setLocation(630, 85);
        chkFiltroDerivativoE.setSize(130, 22);
        chkFiltroDerivativoE.setVisible(true);
        chkFiltroDerivativoE.setText("Filtro Derivativo");
        panelControlador.add(chkFiltroDerivativoE);
        
        chkSemFiltroIntegrativoE = new JCheckBox();
        chkSemFiltroIntegrativoE.setLocation(630, 105);
        chkSemFiltroIntegrativoE.setSize(150, 22);
        chkSemFiltroIntegrativoE.setVisible(true);
        chkSemFiltroIntegrativoE.setText("Sem Filtro Derivativo");
        panelControlador.add(chkSemFiltroIntegrativoE);
        
        lblAntiWindUpE = new JLabel("Coeficiente de Tempo do Filtro Anti-Windup");
        lblAntiWindUpE.setLocation(630, 118);
        lblAntiWindUpE.setSize(250, 40);
        lblAntiWindUpE.setVisible(true);
        panelControlador.add(lblAntiWindUpE);
        
        txtAntiWindUpE = new JTextField(8);
        txtAntiWindUpE.setLocation(633, 152);
        txtAntiWindUpE.setSize(40, 22);
        txtAntiWindUpE.setVisible(true);
        panelControlador.add(txtAntiWindUpE);
        
        lblFiltroDerivativoE = new JLabel("Coeficiente de Tempo do Filtro Derivativo");
        lblFiltroDerivativoE.setLocation(630, 162);
        lblFiltroDerivativoE.setSize(250, 40);
        lblFiltroDerivativoE.setVisible(true);
        panelControlador.add(lblFiltroDerivativoE);
        
        txtFiltroDerivativoE = new JTextField(8);
        txtFiltroDerivativoE.setLocation(633, 196);
        txtFiltroDerivativoE.setSize(40, 22);
        txtFiltroDerivativoE.setVisible(true);
        panelControlador.add(txtFiltroDerivativoE);
        
        totalGUI.add(panelControlador);

        //Observação de Estados
        
        titleBarObservador = new JPanel();
        titleBarObservador.setLayout(null);
        titleBarObservador.setLocation(10, 285);
        titleBarObservador.setSize(880, 32);
        titleBarObservador.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarObservador);
        
        titleObservador = new JLabel("Observação de Estados");
        titleObservador.setLocation(10, 0);
        titleObservador.setSize(200, 32);
        titleBarObservador.add(titleObservador);
        
        panelObservador = new JPanel();  
        panelObservador.setOpaque(true);
        panelObservador.setLayout(null);
        panelObservador.setVisible(false);
        panelObservador.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelObservador.setLocation(10, 317);
        panelObservador.setSize(880, 230);
        
        titleBarMatrizObs = new JPanel();
        titleBarMatrizObs.setLayout(null);
        titleBarMatrizObs.setLocation(20, 330);
        titleBarMatrizObs.setSize(380, 32);
        titleBarMatrizObs.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarMatrizObs);
        
        titleMatrizObs = new JLabel("Matriz de Ganhos do Observador");
        titleMatrizObs.setLocation(20, 0);
        titleMatrizObs.setSize(200, 32);
        titleBarMatrizObs.add(titleMatrizObs);
        
        lblPolos = new JLabel("Pólos");
        lblPolos.setLocation(42, 50);
        lblPolos.setSize(250, 40);
        lblPolos.setVisible(true);
        panelObservador.add(lblPolos);
        
        lblP1 = new JLabel("P1");
        lblP1.setLocation(10, 80);
        lblP1.setSize(250, 40);
        lblP1.setVisible(true);
        panelObservador.add(lblP1);
        
        txtP1 = new JTextField(8);
        txtP1.setLocation(40, 90);
        txtP1.setSize(50, 22);
        txtP1.setVisible(true);
        panelObservador.add(txtP1);
        
        lblP2 = new JLabel("P2");
        lblP2.setLocation(10, 110);
        lblP2.setSize(250, 40);
        lblP2.setVisible(true);
        panelObservador.add(lblP2);
        
        txtP2 = new JTextField(8);
        txtP2.setLocation(40, 120);
        txtP2.setSize(50, 22);
        txtP2.setVisible(true);
        panelObservador.add(txtP2);
        
        JButton setas1 = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/setas1.png"));
            setas1.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        setas1.setLocation(100, 110);
        setas1.setSize(32, 12);
        setas1.setBorderPainted(false); 
        setas1.setContentAreaFilled(false); 
        setas1.setFocusPainted(false); 
        setas1.setOpaque(false);
        
        panelObservador.add(setas1);
        
        lblGanhos = new JLabel("Ganhos");
        lblGanhos.setLocation(168, 50);
        lblGanhos.setSize(250, 40);
        lblGanhos.setVisible(true);
        panelObservador.add(lblGanhos);
        
        lblL1 = new JLabel("L1");
        lblL1.setLocation(140, 80);
        lblL1.setSize(250, 40);
        lblL1.setVisible(true);
        panelObservador.add(lblL1);
        
        txtL1 = new JTextField(8);
        txtL1.setLocation(170, 90);
        txtL1.setSize(40, 22);
        txtL1.setVisible(true);
        panelObservador.add(txtL1);
        
        lblL2 = new JLabel("L2");
        lblL2.setLocation(140, 110);
        lblL2.setSize(250, 40);
        lblL2.setVisible(true);
        panelObservador.add(lblL2);
        
        txtL2 = new JTextField(8);
        txtL2.setLocation(170, 120);
        txtL2.setSize(40, 22);
        txtL2.setVisible(true);
        panelObservador.add(txtL2);
        
        JButton setas2 = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/setas2.png"));
            setas2.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        setas2.setLocation(220, 110);
        setas2.setSize(16, 12);
        setas2.setBorderPainted(false); 
        setas2.setContentAreaFilled(false); 
        setas2.setFocusPainted(false); 
        setas2.setOpaque(false);
        
        panelObservador.add(setas2);
        
        lblL = new JLabel("L = ");
        lblL.setLocation(250, 95);
        lblL.setSize(250, 40);
        lblL.setVisible(true);
        panelObservador.add(lblL);
        
        JButton colchetes = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/colchetes.png"));
            colchetes.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        colchetes.setLocation(270, 50);
        colchetes.setSize(128, 120);
        colchetes.setBorderPainted(false); 
        colchetes.setContentAreaFilled(false); 
        colchetes.setFocusPainted(false); 
        colchetes.setOpaque(false);
        
        panelObservador.add(colchetes);
        
        lblL1Matriz = new JLabel("L1");
        lblL1Matriz.setLocation(325, 70);
        lblL1Matriz.setSize(250, 40);
        lblL1Matriz.setVisible(true);
        panelObservador.add(lblL1Matriz);
        
        lblL2Matriz = new JLabel("L2");
        lblL2Matriz.setLocation(325, 110);
        lblL2Matriz.setSize(250, 40);
        lblL2Matriz.setVisible(true);
        panelObservador.add(lblL2Matriz);
        
        totalGUI.add(panelObservador);
        
        //Seguidor de Referência
        
        titleBarSeguidor = new JPanel();
        titleBarSeguidor.setLayout(null);
        titleBarSeguidor.setLocation(10, 285);
        titleBarSeguidor.setSize(880, 32);
        titleBarSeguidor.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarSeguidor);
        
        titleSeguidor = new JLabel("Seguidor de Referência");
        titleSeguidor.setLocation(10, 0);
        titleSeguidor.setSize(200, 32);
        titleBarSeguidor.add(titleSeguidor);
        
        panelSeguidor = new JPanel();  
        panelSeguidor.setOpaque(true);
        panelSeguidor.setLayout(null);
        panelSeguidor.setVisible(false);
        panelSeguidor.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelSeguidor.setLocation(10, 317);
        panelSeguidor.setSize(880, 230);
        
        titleBarMatrizSeg = new JPanel();
        titleBarMatrizSeg.setLayout(null);
        titleBarMatrizSeg.setLocation(20, 330);
        titleBarMatrizSeg.setSize(470, 32);
        titleBarMatrizSeg.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarMatrizSeg);
        
        titleMatrizSeg = new JLabel("Matriz de Ganhos do Seguidor");
        titleMatrizSeg.setLocation(20, 0);
        titleMatrizSeg.setSize(200, 32);
        titleBarMatrizSeg.add(titleMatrizSeg);
        
        lblPolos = new JLabel("Pólos");
        lblPolos.setLocation(42, 50);
        lblPolos.setSize(250, 40);
        lblPolos.setVisible(true);
        panelSeguidor.add(lblPolos);
        
        lblP1 = new JLabel("P1");
        lblP1.setLocation(10, 80);
        lblP1.setSize(250, 40);
        lblP1.setVisible(true);
        panelSeguidor.add(lblP1);
        
        txtP1 = new JTextField(8);
        txtP1.setLocation(40, 90);
        txtP1.setSize(50, 22);
        txtP1.setVisible(true);
        panelSeguidor.add(txtP1);
        
        lblP2 = new JLabel("P2");
        lblP2.setLocation(10, 110);
        lblP2.setSize(250, 40);
        lblP2.setVisible(true);
        panelSeguidor.add(lblP2);
        
        txtP2 = new JTextField(8);
        txtP2.setLocation(40, 120);
        txtP2.setSize(50, 22);
        txtP2.setVisible(true);
        panelSeguidor.add(txtP2);
        
        lblP3 = new JLabel("P3");
        lblP3.setLocation(10, 140);
        lblP3.setSize(250, 40);
        lblP3.setVisible(true);
        panelSeguidor.add(lblP3);
        
        txtP3 = new JTextField(8);
        txtP3.setLocation(40, 150);
        txtP3.setSize(50, 22);
        txtP3.setVisible(true);
        panelSeguidor.add(txtP3);
        
        setas1 = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/setas1.png"));
            setas1.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        setas1.setLocation(100, 125);
        setas1.setSize(32, 12);
        setas1.setBorderPainted(false); 
        setas1.setContentAreaFilled(false); 
        setas1.setFocusPainted(false); 
        setas1.setOpaque(false);
        
        panelSeguidor.add(setas1);
        
        lblGanhos = new JLabel("Ganhos");
        lblGanhos.setLocation(168, 50);
        lblGanhos.setSize(250, 40);
        lblGanhos.setVisible(true);
        panelSeguidor.add(lblGanhos);
        
        lblL1 = new JLabel("L1");
        lblL1.setLocation(140, 80);
        lblL1.setSize(250, 40);
        lblL1.setVisible(true);
        panelSeguidor.add(lblL1);
        
        txtL1 = new JTextField(8);
        txtL1.setLocation(170, 90);
        txtL1.setSize(40, 22);
        txtL1.setVisible(true);
        panelSeguidor.add(txtL1);
        
        lblL2 = new JLabel("L2");
        lblL2.setLocation(140, 110);
        lblL2.setSize(250, 40);
        lblL2.setVisible(true);
        panelSeguidor.add(lblL2);
        
        txtL2 = new JTextField(8);
        txtL2.setLocation(170, 120);
        txtL2.setSize(40, 22);
        txtL2.setVisible(true);
        panelSeguidor.add(txtL2);
        
        lblL3 = new JLabel("L3");
        lblL3.setLocation(140, 140);
        lblL3.setSize(250, 40);
        lblL3.setVisible(true);
        panelSeguidor.add(lblL3);
        
        txtL3 = new JTextField(8);
        txtL3.setLocation(170, 150);
        txtL3.setSize(40, 22);
        txtL3.setVisible(true);
        panelSeguidor.add(txtL3);
        
        setas2 = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/setas2.png"));
            setas2.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        setas2.setLocation(220, 125);
        setas2.setSize(16, 12);
        setas2.setBorderPainted(false); 
        setas2.setContentAreaFilled(false); 
        setas2.setFocusPainted(false); 
        setas2.setOpaque(false);
        
        panelSeguidor.add(setas2);
        
        lblL = new JLabel("L = ");
        lblL.setLocation(250, 110);
        lblL.setSize(250, 40);
        lblL.setVisible(true);
        panelSeguidor.add(lblL);
        
        colchetes = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/colchetes2.png"));
            colchetes.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        colchetes.setLocation(270, 108);
        colchetes.setSize(199, 45);
        colchetes.setBorderPainted(false); 
        colchetes.setContentAreaFilled(false); 
        colchetes.setFocusPainted(false); 
        colchetes.setOpaque(false);
        
        panelSeguidor.add(colchetes);
        
        lblL1Matriz = new JLabel("L1");
        lblL1Matriz.setLocation(295, 110);
        lblL1Matriz.setSize(250, 40);
        lblL1Matriz.setVisible(true);
        panelSeguidor.add(lblL1Matriz);
        
        lblL2Matriz = new JLabel("L2");
        lblL2Matriz.setLocation(355, 110);
        lblL2Matriz.setSize(250, 40);
        lblL2Matriz.setVisible(true);
        panelSeguidor.add(lblL2Matriz);
        
        lblL3Matriz = new JLabel("L3");
        lblL3Matriz.setLocation(415, 110);
        lblL3Matriz.setSize(250, 40);
        lblL3Matriz.setVisible(true);
        panelSeguidor.add(lblL3Matriz);
        
        totalGUI.add(panelSeguidor);
        
        //Conexão
        
        JPanel titleBarConexao = new JPanel();
        titleBarConexao.setLayout(null);
        titleBarConexao.setLocation(580, 555);
        titleBarConexao.setSize(170, 32);
        titleBarConexao.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarConexao);
        
        JLabel titleConexao = new JLabel("Conexão");
        titleConexao.setLocation(20, 0);
        titleConexao.setSize(200, 32);
        titleBarConexao.add(titleConexao);
        
        JPanel panelConexao = new JPanel();  
        panelConexao.setOpaque(true);
        panelConexao.setLayout(null);
        panelConexao.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelConexao.setLocation(580, 587);
        panelConexao.setSize(170, 100);
        
        simuladorBtn = new JToggleButton("Simulador");
        simuladorBtn.setLocation(40, 10);
        simuladorBtn.setSize(95, 30);
        simuladorBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simuladorBtnActionPerformed(evt);
            }
        });
        
        panelConexao.add(simuladorBtn);
        
        plantaBtn = new JToggleButton("Planta");
        plantaBtn.setLocation(40, 50);
        plantaBtn.setSize(95, 30);
        plantaBtn.setEnabled(true);
        plantaBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                plantaBtnActionPerformed(evt);
            }
        });
        panelConexao.add(plantaBtn);
        
        ButtonGroup grupoConexao = new ButtonGroup();
        grupoConexao.add(simuladorBtn);
        grupoConexao.add(plantaBtn);
        
        lblIP = new JLabel("IP");
        lblIP.setLocation(15, 115);
        lblIP.setSize(40, 30);
        panelConexao.add(lblIP);
        
        txtIP = new JTextField(8);
        txtIP.setText("10.13.99.69");
        txtIP.setLocation(35, 120);
        txtIP.setSize(80, 22);
        txtIP.setEnabled(false);
        panelConexao.add(txtIP);
        
        lblPorta = new JLabel("Porta");
        lblPorta.setLocation(15, 145);
        lblPorta.setSize(60, 30);
        panelConexao.add(lblPorta);
        
        txtPorta = new JTextField(8);
        txtPorta.setText("20081");
        txtPorta.setLocation(55, 150);
        txtPorta.setSize(60, 22);
        txtPorta.setEnabled(false);
        panelConexao.add(txtPorta);
        
        totalGUI.add(panelConexao);
        
        //Rodapé
        
        iniciarBtn = new JButton ("Iniciar");
        iniciarBtn.setLocation(760, 555);
        iniciarBtn.setSize(130, 60);
        iniciarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                iniciarBtnActionPerformed(evt);
            }
        });
        totalGUI.add(iniciarBtn);
        
        atualizarBtn = new JButton ("Atualizar");
        atualizarBtn.setLocation(760, 625);
        atualizarBtn.setSize(130, 60);
        atualizarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atualizarBtnActionPerformed(evt);
            }
        });
        atualizarBtn.setVisible(false);
        totalGUI.add(atualizarBtn);
        
        //JButton logo = new JButton();
       // try {
      //      Image img = ImageIO.read(getClass().getResource("icones/logo.png"));
       //     logo.setIcon(new ImageIcon(img));
      //  } catch (IOException ex) {
       // }
      //  logo.setLocation(20,538);
       // logo.setSize(110, 49);
       // logo.setBorderPainted(false); 
       // logo.setContentAreaFilled(false); 
      //  logo.setFocusPainted(false); 
       // logo.setOpaque(false);
        
       // totalGUI.add(logo);
        
        totalGUI.setOpaque(true);
        return totalGUI;
    }
    
    // This is the new ActionPerformed Method.
    // It catches any events with an ActionListener attached.
    // Using an if statement, we can determine which button was pressed
    // and change the appropriate values in our GUI.

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) { 
        System.exit(0);
    }
    
    private void malhaFechadaBtnActionPerformed(ActionEvent evt) {
            panelControlador.setVisible(true);
            titleBarControlador.setVisible(true);
            titleMestre.setVisible(true);
            titleBarMestre.setVisible(true);
            titleBarEscravo.setVisible(true);
            titleEscravo.setVisible(true);
            
            panelObservador.setVisible(false);
            titleBarObservador.setVisible(false);
        
            panelLeitura.setVisible(false);
            panelEscrita.setVisible(false);
            titleBarLeitura.setVisible(false);
            titleBarEscrita.setVisible(false);
            panelConfiguracoes.setVisible(true);
            titleBarConfiguracoes.setVisible(true);
            titleConfiguracoes.setVisible(true);
            configuracao1Btn.setVisible(true);
            configuracao2Btn.setVisible(true);
            configuracao3Btn.setVisible(true);
            
            txtAntiWindUp.setEnabled(true);
            lblAntiWindUp.setEnabled(true);
            txtAntiWindUpE.setEnabled(true);
            lblAntiWindUpE.setEnabled(true);
            txtFiltroDerivativo.setEnabled(true);
            lblFiltroDerivativo.setEnabled(true);
            txtFiltroDerivativoE.setEnabled(true);
            lblFiltroDerivativoE.setEnabled(true);
            chkFiltroDerivativo.setEnabled(true);
            chkFiltroDerivativoE.setEnabled(true);
            chkSemFiltroIntegrativo.setEnabled(true);
            chkSemFiltroIntegrativoE.setEnabled(true);
            radioAntiWindup.setEnabled(true);
            radioAntiWindupE.setEnabled(true);
            radioFiltroCondicional.setEnabled(true);
            radioFiltroCondicionalE.setEnabled(true);
            comboBoxControladoresE.setEnabled(true);
            lblControladoresE.setEnabled(true);
            comboBoxControladores.setEnabled(true);
            lblControladores.setEnabled(true);
            lblKp.setEnabled(true);
            lblKpE.setEnabled(true);
            lblKi.setEnabled(true);
            lblKiE.setEnabled(true);
            lblKd.setEnabled(true);
            lblKdE.setEnabled(true);
            lblTi.setEnabled(true);
            lblTiE.setEnabled(true);
            lblTd.setEnabled(true);
            lblTdE.setEnabled(true);
            
            if(sinalDegrauBtn.isSelected() || sinalSenoidalBtn.isSelected() || sinalQuadraticoBtn.isSelected() || sinalDenteDeSerraBtn.isSelected()){
                txtSetPoint.setVisible(true);
                lblSetPoint.setVisible(true);
                txtAmplitude.setVisible(false);
                lblAmplitude.setVisible(false);
                
                notificacao1.setText("Por favor, utilizar valores entre 0 e 30cm.");
                notificacao2.setText("Por favor, utilizar valores entre 0 e 30cm.");
            }
            
            habilitarEntradasControlador();
    }
    
    private void comboBoxControladoresActionPerformed(ActionEvent evt) {
        habilitarEntradasControlador();
    }
    
    private void comboBoxControladoresEActionPerformed(ActionEvent evt) {
        habilitarEntradasControlador();
    }
    
    private void malhaAbertaBtnActionPerformed(ActionEvent evt) {
            panelControlador.setVisible(true);
            titleBarControlador.setVisible(true);
            titleMestre.setVisible(true);
            titleBarMestre.setVisible(true);
            titleBarEscravo.setVisible(true);
            titleEscravo.setVisible(true);
            
            panelObservador.setVisible(false);
            titleBarObservador.setVisible(false);
        
            panelLeitura.setVisible(true);
            panelEscrita.setVisible(true);
            titleBarLeitura.setVisible(true);
            titleBarEscrita.setVisible(true);
            panelConfiguracoes.setVisible(false);
            titleBarConfiguracoes.setVisible(false);
            titleConfiguracoes.setVisible(false);
            configuracao1Btn.setVisible(false);
            configuracao2Btn.setVisible(false);
            configuracao3Btn.setVisible(false);
            
            txtAntiWindUp.setEnabled(false);
            lblAntiWindUp.setEnabled(false);
            txtAntiWindUpE.setEnabled(false);
            lblAntiWindUpE.setEnabled(false);
            txtFiltroDerivativo.setEnabled(false);
            lblFiltroDerivativo.setEnabled(false);
            txtFiltroDerivativoE.setEnabled(false);
            lblFiltroDerivativoE.setEnabled(false);
            chkFiltroDerivativo.setEnabled(false);
            chkFiltroDerivativoE.setEnabled(false);
            chkSemFiltroIntegrativo.setEnabled(false);
            chkSemFiltroIntegrativoE.setEnabled(false);
            radioAntiWindup.setEnabled(false);
            radioAntiWindupE.setEnabled(false);
            radioFiltroCondicional.setEnabled(false);
            radioFiltroCondicionalE.setEnabled(false);
            comboBoxControladoresE.setEnabled(false);
            lblControladoresE.setEnabled(false);
            comboBoxControladores.setEnabled(false);
            lblControladores.setEnabled(false);
            txtKp.setEnabled(false);
            lblKp.setEnabled(false);
            txtKpE.setEnabled(false);
            lblKpE.setEnabled(false);
            txtKi.setEnabled(false);
            lblKi.setEnabled(false);
            txtKiE.setEnabled(false);
            lblKiE.setEnabled(false);
            txtKd.setEnabled(false);
            lblKd.setEnabled(false);
            txtKdE.setEnabled(false);
            lblKdE.setEnabled(false);
            txtTi.setEnabled(false);
            lblTi.setEnabled(false);
            txtTiE.setEnabled(false);
            lblTiE.setEnabled(false);
            txtTd.setEnabled(false);
            lblTd.setEnabled(false);
            txtTdE.setEnabled(false);
            lblTdE.setEnabled(false);            
            
            if(sinalDegrauBtn.isSelected() || sinalQuadraticoBtn.isSelected() || sinalDenteDeSerraBtn.isSelected()){
                txtSetPoint.setVisible(false);
                lblSetPoint.setVisible(false);
                txtAmplitude.setVisible(true);
                lblAmplitude.setVisible(true);
                
                notificacao1.setText("Por favor, utilizar valores entre -3 e 3V.");
                notificacao2.setText("Por favor, utilizar valores entre -3 e 3V.");
            } else if(sinalSenoidalBtn.isSelected()){
                txtSetPoint.setVisible(false);
                lblSetPoint.setVisible(false);
                txtAmplitude.setVisible(true);
                lblAmplitude.setVisible(true);
                
                notificacao1.setText("Por favor, utilizar valores entre 0 e 3V.");
                notificacao2.setText("Por favor, utilizar valores entre 0 e 3V.");
            } else if(sinalAleatorioBtn.isSelected()){
                txtSetPoint.setVisible(false);
                lblSetPoint.setVisible(false);
                txtAmplitude.setVisible(false);
                lblAmplitude.setVisible(false);
                
                notificacao1.setText("Por favor, utilizar valores entre 0 e 3V.");
                notificacao2.setText("Por favor, utilizar valores entre -3 e 3V.");
            }
    }
    
    private void obsEstadoBtnActionPerformed(ActionEvent evt) {
        panelControlador.setVisible(false);
        titleBarControlador.setVisible(false);
        titleMestre.setVisible(false);
        titleBarMestre.setVisible(false);
        titleBarEscravo.setVisible(false);
        titleEscravo.setVisible(false);

        titleBarObservador.setVisible(true);
        titleBarSeguidor.setVisible(false);
        panelSeguidor.setVisible(false);
        panelObservador.setVisible(true);
        titleBarObservador.setVisible(true);
        titleBarMatrizObs.setVisible(true);
        titleBarSeguidor.setVisible(false);
        titleBarMatrizSeg.setVisible(false);

        panelLeitura.setVisible(false);
        panelEscrita.setVisible(false);
        titleBarLeitura.setVisible(false);
        titleBarEscrita.setVisible(false);
        panelConfiguracoes.setVisible(true);
        titleBarConfiguracoes.setVisible(true);
        titleConfiguracoes.setVisible(true);
        configuracao1Btn.setVisible(true);
        configuracao2Btn.setVisible(true);
        configuracao3Btn.setVisible(true);
        obsEstadoBtn.setVisible(true);
          
        txtP1.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showGanhos();
            }
        });
        
        txtP2.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showGanhos();
            }
        });
        
        txtL1.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showPolos();
            }
        });
        
        txtL2.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showPolos();
            }
        });
    }
    
    private void segReferenciaBtnActionPerformed(ActionEvent evt) {
        panelControlador.setVisible(false);
        titleBarControlador.setVisible(false);
        titleMestre.setVisible(false);
        titleBarMestre.setVisible(false);
        titleBarEscravo.setVisible(false);
        titleEscravo.setVisible(false);

        titleBarObservador.setVisible(false);
        titleBarSeguidor.setVisible(true);
        panelSeguidor.setVisible(true);
        panelObservador.setVisible(false);
        titleBarObservador.setVisible(false);
        titleBarMatrizObs.setVisible(false);
        titleBarSeguidor.setVisible(true);
        titleBarMatrizSeg.setVisible(true);

        panelLeitura.setVisible(false);
        panelEscrita.setVisible(false);
        titleBarLeitura.setVisible(false);
        titleBarEscrita.setVisible(false);
        panelConfiguracoes.setVisible(true);
        titleBarConfiguracoes.setVisible(true);
        titleConfiguracoes.setVisible(true);
        configuracao1Btn.setVisible(true);
        configuracao2Btn.setVisible(true);
        configuracao3Btn.setVisible(true);
        obsEstadoBtn.setVisible(true);
          
        txtP1.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showGanhos();
            }
        });
        
        txtP2.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showGanhos();
            }
        });
        
        txtL1.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showPolos();
            }
        });
        
        txtL2.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) { }
            
            @Override
            public void focusLost(FocusEvent fe) {
                showPolos();
            }
        });
    }

    private void simuladorBtnActionPerformed(ActionEvent evt) {
        if(simuladorBtn.isSelected()){
            txtIP.setText("127.0.0.1");
        }
    }
    
    private void plantaBtnActionPerformed(ActionEvent evt) {
        if(plantaBtn.isSelected()){
           txtIP.setText("10.13.99.69");
        }
    }
    
    private void configuracao1BtnActionPerformed(ActionEvent evt) {
            panelControlador.setVisible(true);
            titleBarControlador.setVisible(true);
            panelObservador.setVisible(false);
            titleBarObservador.setVisible(false);
        
            titleMestre.setVisible(false);
            titleBarMestre.setVisible(false);
            titleBarEscravo.setVisible(false);
            titleEscravo.setVisible(false);
            
            comboBoxControladoresE.setVisible(false);
            lblControladoresE.setVisible(false);
            lblKpE.setVisible(false);
            lblKdE.setVisible(false);
            lblTdE.setVisible(false);
            lblKiE.setVisible(false);
            lblTiE.setVisible(false);
            txtKpE.setVisible(false);
            txtKdE.setVisible(false);
            txtTdE.setVisible(false);
            txtKiE.setVisible(false);
            txtTiE.setVisible(false);
            
            lblAntiWindUpE.setVisible(false);
            lblFiltroDerivativoE.setVisible(false);
            txtAntiWindUpE.setVisible(false);
            txtFiltroDerivativoE.setVisible(false);
            chkFiltroDerivativoE.setVisible(false);
            chkSemFiltroIntegrativoE.setVisible(false);
            radioAntiWindupE.setVisible(false);
            radioFiltroCondicionalE.setVisible(false);
            
            if(obsEstadoBtn.isSelected()){
                panelControlador.setVisible(false);
                titleBarControlador.setVisible(false);
                titleMestre.setVisible(false);
                titleBarMestre.setVisible(false);
                titleBarEscravo.setVisible(false);
                titleEscravo.setVisible(false);

                panelObservador.setVisible(true);
                titleBarObservador.setVisible(true);
            }
    }
    
        private void configuracao2BtnActionPerformed(ActionEvent evt) {
            panelControlador.setVisible(true);
            titleBarControlador.setVisible(true);
            panelObservador.setVisible(false);
            titleBarObservador.setVisible(false);
            
            titleMestre.setVisible(false);
            titleBarMestre.setVisible(false);
            titleBarEscravo.setVisible(false);
            titleEscravo.setVisible(false);
            comboBoxControladoresE.setVisible(false);
            lblControladoresE.setVisible(false);
            lblKpE.setVisible(false);
            lblKdE.setVisible(false);
            lblTdE.setVisible(false);
            lblKiE.setVisible(false);
            lblTiE.setVisible(false);
            txtKpE.setVisible(false);
            txtKdE.setVisible(false);
            txtTdE.setVisible(false);
            txtKiE.setVisible(false);
            txtTiE.setVisible(false);
            
            lblAntiWindUpE.setVisible(false);
            lblFiltroDerivativoE.setVisible(false);
            txtAntiWindUpE.setVisible(false);
            txtFiltroDerivativoE.setVisible(false);
            chkFiltroDerivativoE.setVisible(false);
            chkSemFiltroIntegrativoE.setVisible(false);
            radioAntiWindupE.setVisible(false);
            radioFiltroCondicionalE.setVisible(false);
            
            if(obsEstadoBtn.isSelected()){
                panelControlador.setVisible(false);
                titleBarControlador.setVisible(false);
                titleMestre.setVisible(false);
                titleBarMestre.setVisible(false);
                titleBarEscravo.setVisible(false);
                titleEscravo.setVisible(false);

                panelObservador.setVisible(true);
                titleBarObservador.setVisible(true);
            }
    }
        
    private void configuracao3BtnActionPerformed(ActionEvent evt) {
            panelControlador.setVisible(true);
            titleBarControlador.setVisible(true);
            panelObservador.setVisible(false);
            titleBarObservador.setVisible(false);
        
            titleMestre.setVisible(true);
            titleBarMestre.setVisible(true);
            titleBarEscravo.setVisible(true);
            titleEscravo.setVisible(true);
            comboBoxControladoresE.setVisible(true);
            lblControladoresE.setVisible(true);
            lblKpE.setVisible(true);
            lblKdE.setVisible(true);
            lblTdE.setVisible(true);
            lblKiE.setVisible(true);
            lblTiE.setVisible(true);
            txtKpE.setVisible(true);
            txtKdE.setVisible(true);
            txtTdE.setVisible(true);
            txtKiE.setVisible(true);
            txtTiE.setVisible(true);
            
            lblAntiWindUpE.setVisible(true);
            lblFiltroDerivativoE.setVisible(true);
            txtAntiWindUpE.setVisible(true);
            txtFiltroDerivativoE.setVisible(true);
            chkFiltroDerivativoE.setVisible(true);
            chkSemFiltroIntegrativoE.setVisible(true);
            radioAntiWindupE.setVisible(true);
            radioFiltroCondicionalE.setVisible(true);
            
            if(obsEstadoBtn.isSelected()){
                panelControlador.setVisible(false);
                titleBarControlador.setVisible(false);
                titleMestre.setVisible(false);
                titleBarMestre.setVisible(false);
                titleBarEscravo.setVisible(false);
                titleEscravo.setVisible(false);

                panelObservador.setVisible(true);
                titleBarObservador.setVisible(true);
            }
    }
    
    private void sinalDegrauBtnActionPerformed(ActionEvent evt) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtPeriodo.setVisible(false);
            lblPeriodo.setVisible(false);
            txtOffset.setVisible(false);
            lblOffset.setVisible(false);
            notificacao1.setVisible(true);
            notificacao2.setVisible(false);
            notificacao3.setVisible(false);
            
            txtPeriodoMin.setVisible(false);
            txtPeriodoMax.setVisible(false);
            lblPeriodoMax.setVisible(false);
            lblPeriodoMin.setVisible(false);
            txtAmplitudeMin.setVisible(false);
            txtAmplitudeMax.setVisible(false);
            lblAmplitudeMax.setVisible(false);
            lblAmplitudeMin.setVisible(false);
            
            notificacao2.setText("Por favor, utilizar valores entre -3 e 3V.");
            
            if(malhaAbertaBtn.isSelected()){
                txtSetPoint.setVisible(false);
                lblSetPoint.setVisible(false);
                txtAmplitude.setVisible(true);
                lblAmplitude.setVisible(true);
            } else if(malhaFechadaBtn.isSelected() || obsEstadoBtn.isSelected()){
                txtSetPoint.setVisible(true);
                lblSetPoint.setVisible(true);
                txtAmplitude.setVisible(false);
                lblAmplitude.setVisible(false);
                
                notificacao1.setText("Por favor, utilizar valores entre 0 e 30cm.");
                notificacao2.setVisible(false);
                notificacao2.setText("Por favor, utilizar valores entre 0 e 30cm.");
            }
    }
    
    private void sinalSenoidalBtnActionPerformed(ActionEvent evt) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtPeriodo.setVisible(true);
            lblPeriodo.setVisible(true);
            txtOffset.setVisible(true);
            lblOffset.setVisible(true);
            notificacao2.setVisible(true);
            notificacao3.setVisible(true);
            txtPeriodoMin.setVisible(false);
            txtPeriodoMax.setVisible(false);
            lblPeriodoMax.setVisible(false);
            lblPeriodoMin.setVisible(false);
            txtAmplitudeMin.setVisible(false);
            txtAmplitudeMax.setVisible(false);
            lblAmplitudeMax.setVisible(false);
            lblAmplitudeMin.setVisible(false);
            
            notificacao1.setVisible(false);
            
            notificacao1.setText("Por favor, utilizar valores entre 0 e 3V.");
            notificacao2.setText("Por favor, utilizar valores entre 0 e 3V.");
            notificacao3.setText("Por favor, utilizar valores em segundos.");
            
            if(malhaAbertaBtn.isSelected()){
                txtSetPoint.setVisible(false);
                lblSetPoint.setVisible(false);
                txtAmplitude.setVisible(true);
                lblAmplitude.setVisible(true);
            } else if(malhaFechadaBtn.isSelected()){
                txtSetPoint.setVisible(true);
                lblSetPoint.setVisible(true);
                txtAmplitude.setVisible(false);
                lblAmplitude.setVisible(false);
                
                notificacao1.setText("Por favor, utilizar valores entre 0 e 30cm.");
                notificacao2.setText("Por favor, utilizar valores entre 0 e 30cm.");
            }
    }
    
    private void sinalQuadraticoBtnActionPerformed(ActionEvent evt) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtPeriodo.setVisible(true);
            lblPeriodo.setVisible(true);
            txtOffset.setVisible(true);
            lblOffset.setVisible(true);
            notificacao2.setVisible(true);
            notificacao3.setVisible(true);
            txtPeriodoMin.setVisible(false);
            txtPeriodoMax.setVisible(false);
            lblPeriodoMax.setVisible(false);
            lblPeriodoMin.setVisible(false);
            txtAmplitudeMin.setVisible(false);
            txtAmplitudeMax.setVisible(false);
            lblAmplitudeMax.setVisible(false);
            lblAmplitudeMin.setVisible(false);
            
            notificacao1.setVisible(false);
            
            notificacao1.setText("Por favor, utilizar valores entre -3 e 3V.");
            notificacao2.setText("Por favor, utilizar valores entre -3 e 3V.");
            notificacao3.setText("Por favor, utilizar valores em segundos.");
            
            if(malhaAbertaBtn.isSelected()){
                txtSetPoint.setVisible(false);
                lblSetPoint.setVisible(false);
                txtAmplitude.setVisible(true);
                lblAmplitude.setVisible(true);
            } else if(malhaFechadaBtn.isSelected()){
                txtSetPoint.setVisible(true);
                lblSetPoint.setVisible(true);
                txtAmplitude.setVisible(false);
                lblAmplitude.setVisible(false);
                
                notificacao1.setText("Por favor, utilizar valores entre 0 e 30cm.");
                notificacao2.setText("Por favor, utilizar valores entre 0 e 30cm.");
            }
    }
    
    private void sinalDenteDeSerraBtnActionPerformed(ActionEvent evt) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtPeriodo.setVisible(true);
            lblPeriodo.setVisible(true);
            txtOffset.setVisible(true);
            lblOffset.setVisible(true);
            notificacao2.setVisible(true);
            notificacao3.setVisible(true);
            
            txtPeriodoMin.setVisible(false);
            txtPeriodoMax.setVisible(false);
            lblPeriodoMax.setVisible(false);
            lblPeriodoMin.setVisible(false);
            txtAmplitudeMin.setVisible(false);
            txtAmplitudeMax.setVisible(false);
            lblAmplitudeMax.setVisible(false);
            lblAmplitudeMin.setVisible(false);
            
            notificacao1.setVisible(false);
            
            notificacao1.setText("Por favor, utilizar valores entre -3 e 3V.");
            notificacao2.setText("Por favor, utilizar valores entre -3 e 3V.");
            notificacao3.setText("Por favor, utilizar valores em segundos.");
            
            if(malhaAbertaBtn.isSelected()){
                txtSetPoint.setVisible(false);
                lblSetPoint.setVisible(false);
                txtAmplitude.setVisible(true);
                lblAmplitude.setVisible(true);
            } else if(malhaFechadaBtn.isSelected()){
                txtSetPoint.setVisible(true);
                lblSetPoint.setVisible(true);
                txtAmplitude.setVisible(false);
                lblAmplitude.setVisible(false);
                
                notificacao1.setText("Por favor, utilizar valores entre 0 e 30cm.");
                notificacao2.setText("Por favor, utilizar valores entre 0 e 30cm.");
            }
    }
    
    private void sinalAleatorioBtnActionPerformed(ActionEvent evt) {
            txtSetPoint.setVisible(false);
            lblSetPoint.setVisible(false);
            txtAmplitude.setVisible(false);
            lblAmplitude.setVisible(false);
            txtPeriodo.setVisible(false);
            lblPeriodo.setVisible(false);
            txtOffset.setVisible(false);
            lblOffset.setVisible(false);
            notificacao1.setVisible(false);
            notificacao2.setVisible(true);
            notificacao3.setVisible(true);
            
            txtPeriodoMin.setVisible(true);
            txtPeriodoMax.setVisible(true);
            lblPeriodoMax.setVisible(true);
            lblPeriodoMin.setVisible(true);
            txtAmplitudeMin.setVisible(true);
            txtAmplitudeMax.setVisible(true);
            lblAmplitudeMax.setVisible(true);
            lblAmplitudeMin.setVisible(true);
            
            notificacao2.setText("O valor da amplitude será gerado aleatoriamente.");
            notificacao3.setText("O valor do período será gerado aleatoriamente.");          
    }
    
    void abrirTelaGrafico()
    {
        frameGrafico = new JFrame();
        frameGrafico.setUndecorated(true);
        
        telaGrafico = new Grafico(clienteControle);
        frameGrafico.setContentPane(telaGrafico.createContentPane());
        telaGrafico.criar();

        // The other bits and pieces that make our program a bit more stable.
        frameGrafico.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frameGrafico.setSize(840, 640);
        frameGrafico.setVisible(true);
        
        //Poder mover o JFrame
        ComponentMover cm = new ComponentMover();
        cm.registerComponent(frameGrafico);
    }
    
    private void iniciarBtnActionPerformed(ActionEvent evt) {       
        if(aplicarParametros())
            clique_botao_iniciar();
        else
            JOptionPane.showMessageDialog(this, "Parâmetros incorretos");
     }
    
    private void atualizarBtnActionPerformed(ActionEvent evt) {       
        if(!aplicarParametros())
            JOptionPane.showMessageDialog(this, "Erro ao atualizar parâmetros");
    }
    
    /*
    private void chkFiltroDerivativoActionPerformed(java.awt.event.ActionEvent evt) {                                                    
        // TODO add your handling code here:
        if(chkFiltroDerivativo.isSelected()){
                lblFiltroDerivativo.setVisible(true);
                txtFiltroDerivativo.setVisible(true);
                filtroDerivativo = true;
        } else if(!chkFiltroDerivativo.isSelected()){
                lblFiltroDerivativo.setVisible(false);
                txtFiltroDerivativo.setVisible(false);
                filtroDerivativo = false;
        }
    }                                                   

    private void radioAntiWindupActionPerformed(java.awt.event.ActionEvent evt) {                                                
        // TODO add your handling code here:
        if(radioAntiWindup.isSelected()){
                lblAntiWindUp.setVisible(true);
                txtAntiWindUp.setVisible(true);
                filtroAntiWindup = true;
        } else if(!radioAntiWindup.isSelected()){
                lblAntiWindUp.setVisible(false);
                txtAntiWindUp.setVisible(false);
                filtroAntiWindup = false;
        }
    }                                               

    private void radioFiltroCondicionalActionPerformed(java.awt.event.ActionEvent evt) {                                                       
        // TODO add your handling code here:
        if (radioFiltroCondicional.isSelected()){
                lblAntiWindUp.setVisible(false);
                txtAntiWindUp.setVisible(false);
                filtroCondicional = true;
        } else filtroCondicional = false;
        
    }                                                      

    private void chkSemFiltroIntegrativoActionPerformed(java.awt.event.ActionEvent evt) {                                                        
        // TODO add your handling code here:
        if(chkSemFiltroIntegrativo.isSelected()){
            radioAntiWindup.setEnabled(false);
            radioFiltroCondicional.setEnabled(false);
            semFiltro = true;
        } else if(!chkSemFiltroIntegrativo.isSelected()){
            radioAntiWindup.setEnabled(true);
            radioFiltroCondicional.setEnabled(true);
            semFiltro = false;
        }
        
        if(!chkSemFiltroIntegrativo.isSelected() && radioAntiWindup.isSelected()){
            lblAntiWindUp.setVisible(true);
            txtAntiWindUp.setVisible(true);
            semFiltro = false;
        } else if(chkSemFiltroIntegrativo.isSelected() && radioAntiWindup.isSelected()){
            lblAntiWindUp.setVisible(false);
            txtAntiWindUp.setVisible(false);
            semFiltro = true;
        }
    }*/
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleBarPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de Controle de Tanques");
        setUndecorated(true);

        javax.swing.GroupLayout titleBarPanelLayout = new javax.swing.GroupLayout(titleBarPanel);
        titleBarPanel.setLayout(titleBarPanelLayout);
        titleBarPanelLayout.setHorizontalGroup(
            titleBarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 840, Short.MAX_VALUE)
        );
        titleBarPanelLayout.setVerticalGroup(
            titleBarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 530, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titleBarPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(titleBarPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        final TelaCliente tela = new TelaCliente();
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                tela.setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel titleBarPanel;
    // End of variables declaration//GEN-END:variables
}
