/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.interfaceCliente;

import br.ufrn.controle.modelo.Malha;
import br.ufrn.controle.negocio.ClienteControle;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Daniel
 */
public class Grafico extends javax.swing.JFrame {

    /**
     * Creates new form Grafico
     */
    
    public JLabel lblTr0100;
    public JLabel lblTr595;
    public JLabel lblTr1090;
    public JLabel lblMpAbsoluto;
    public JLabel lblMp;
    public JLabel lblTs2;
    public JLabel lblTs5;
    public JLabel lblTs7;
    public JLabel lblTs10;
    public JLabel lblEssAbsoluto;
    public JLabel lblEss;
    public JLabel lblTp;
    
    public JTextField txtTr0100;
    public JTextField txtTr595;
    public JTextField txtTr1090;
    public JTextField txtMpAbsoluto;
    public JTextField txtMp;
    public JTextField txtTs2;
    public JTextField txtTs5;
    public JTextField txtTs7;
    public JTextField txtTs10;
    public JTextField txtEssAbsoluto;
    public JTextField txtEss;
    public JTextField txtTp;
    
    private JPanel panelGrafico1;
    private JPanel panelGrafico2;

    private JCheckBox chkPMestre;
    private JCheckBox chkIMestre;
    private JCheckBox chkDMestre;
    private JCheckBox chkPEscravo;
    private JCheckBox chkIEscravo;
    private JCheckBox chkDEscravo;
    private JCheckBox chkSaidaControlador;
    private JCheckBox chkSinalDesejado;
    private JCheckBox chkSinalErro;
    private JCheckBox chkSinalEscrito;
    private JCheckBox chkCanal0; //Nivel Tanque 1 
    private JCheckBox chkCanal1; //Nivel Tanque 2
    private JCheckBox chkSetPointMestre;
    private JCheckBox chkSetPointEscravo;
    private JCheckBox chkErroMestre;
    private JCheckBox chkErroEscravo;
    
    private JCheckBox chkErroEstimacao1;
    private JCheckBox chkSinalDesejadoNivel;
    private JCheckBox nivelEstimado1; //Nivel Estimado Tanque 1
    private JCheckBox nivelEstimado2; //Nivel Estimado Tanque 2

    private ClienteControle clienteControle;
    private Timer timer;
    private boolean cascata;
    private boolean obsEstados;
    
    private XYSeriesCollection datasetTensao;
    private XYSeriesCollection datasetNivel;
    private XYSeriesCollection datasetControlador;
    
    
    public Grafico(ClienteControle cliente) {
        initComponents();
        clienteControle = cliente;
        
        //System.out.print("Grafico iniciado");
        
        /*addWindowListener(  
            new WindowAdapter()   
            {
                public void windowClosing(WindowEvent we) {
                    clienteControle.stop();
                }
            }
        );*/
    }
    
    public void config() {
        cascata = clienteControle.getParametrosEntrada().controleCascata;
        obsEstados = clienteControle.getParametrosEntrada().obsEstados;
        
        if (!obsEstados) {
            chkSinalDesejado.setVisible(true);
            chkErroEstimacao1.setVisible(false);
            nivelEstimado1.setVisible(false);
            nivelEstimado2.setVisible(false);
            
            if(cascata){
                chkSinalErro.setVisible(false);
                chkErroMestre.setVisible(true);
                chkErroEscravo.setVisible(true);
            }
            else{
                chkSinalErro.setVisible(true);
                chkErroMestre.setVisible(false);
                chkErroEscravo.setVisible(false);
            }
        }
        else {
            chkErroEstimacao1.setVisible(true);
            chkSinalDesejadoNivel.setVisible(true);
            nivelEstimado1.setVisible(true);
            nivelEstimado2.setVisible(true);
        }
        
        chkSinalEscrito.setVisible(true);
    }
    
    public void criar(){
        this.config();
        
        datasetTensao = new XYSeriesCollection();
        datasetNivel = new XYSeriesCollection();
        datasetControlador = new XYSeriesCollection();
            
        try {
            
            if (!clienteControle.getParametrosEntrada().obsEstados && clienteControle.getParametrosEntrada().malha == Malha.Fechada) { // Malha fechada
                datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculadoP(cascata ? "P - Mestre" : ""));
                datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculadoI(cascata ? "I - Mestre" : ""));
                datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculadoD(cascata ? "D - Mestre" : ""));
                datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculado(cascata ? "PID - Mestre" : ""));

                datasetNivel.addSeries(clienteControle.getComparador().getSinalDesejado(cascata ? "SetPoint - Mestre" : ""));
                datasetNivel.addSeries(clienteControle.getComparador().getSinalErro(cascata ? "Erro - Mestre" : ""));
                datasetNivel.addSeries(clienteControle.getBaseControle().getSinalEscrito(""));

                if (cascata) {
                    datasetControlador.addSeries(clienteControle.getBaseControle().getSinalMA("Sinal Desejado"));
                    datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculadoP("P - Escravo"));
                    datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculadoI("I - Escravo"));
                    datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculadoD("D - Escravo"));
                    datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculado("PID - Escravo"));

                    datasetNivel.addSeries(clienteControle.getComparadorE().getSinalDesejado("SetPoint - Escravo"));
                    datasetNivel.addSeries(clienteControle.getComparadorE().getSinalErro("Erro - Escravo"));
                }
            }
            else { // Malha aberta ou observação de estados
                if (clienteControle.getParametrosEntrada().malha == Malha.Aberta)
                    datasetTensao.addSeries(clienteControle.getBaseControle().getSinalMA(""));
                datasetTensao.addSeries(clienteControle.getBaseControle().getSinalEscrito(""));
            }
            
            datasetNivel.addSeries(clienteControle.getBaseControle().getSinalCanal0(""));
            if (clienteControle.getParametrosEntrada().canal1 || clienteControle.getParametrosEntrada().canalPv == 1)
                datasetNivel.addSeries(clienteControle.getBaseControle().getSinalCanal1(""));
            
            if (clienteControle.getParametrosEntrada().obsEstados) {
                datasetNivel.addSeries(clienteControle.getBaseControle().getSinalMA(""));
                datasetNivel.addSeries(clienteControle.getEstimador().getSinalX1Est());
                //datasetNivel.addSeries(clienteControle.getEstimador().getSinalX2Est());
                datasetNivel.addSeries(clienteControle.getEstimador().getSinalYEst());
                datasetNivel.addSeries(clienteControle.getEstimador().getSinalU());
            }
            
            
            //Gráfico 1: Nível
            JFreeChart graf1 = ChartFactory.createXYLineChart(
                     "Gráfico de Nível",
                     "Tempo (s)",
                     "Nível (cm)",
                     datasetNivel,
                     PlotOrientation.VERTICAL,
                     true,
                     true,
                     false
             );

            //Gráfico 2: Tensão (malha aberta ou observação de estados) ou Controladores (malha fechada)
            JFreeChart graf2;
            if (clienteControle.getParametrosEntrada().obsEstados || clienteControle.getParametrosEntrada().malha == Malha.Aberta) {
                graf2 = ChartFactory.createXYLineChart(
                            "Gráfico de Tensão",
                            "Tempo (s)",
                            "Tensão (V)",
                            datasetTensao,
                            PlotOrientation.VERTICAL,
                            true,
                            true,
                            false
                        );
            }
            else {
                graf2 = ChartFactory.createXYLineChart(
                            "Gráfico dos Controladores",
                            "Tempo (s)",
                            "Nível (cm)",
                            datasetControlador,
                            PlotOrientation.VERTICAL,
                            true,
                            true,
                            false
                        );
            }
            
            ChartPanel chartPanel1 = new ChartPanel(graf2, true);
            chartPanel1.setSize(panelGrafico1.getWidth(),panelGrafico1.getHeight());
            chartPanel1.setVisible(true); 
            panelGrafico1.removeAll();
            panelGrafico1.add(chartPanel1); 
            panelGrafico1.revalidate();
            panelGrafico1.repaint();
            
            ChartPanel chartPanel2 = new ChartPanel(graf1, true);
            chartPanel2.setSize(panelGrafico2.getWidth(),panelGrafico2.getHeight());
            chartPanel2.setVisible(true);
            panelGrafico2.removeAll();
            panelGrafico2.add(chartPanel2); 
            panelGrafico2.revalidate();
            panelGrafico2.repaint();
            
            showVariaveisRegime();
        }  
        catch (Exception e) {  
            e.printStackTrace();  
        }
    }
    
    public void showVariaveisRegime() {
        final DecimalFormat decimal = new DecimalFormat("0.00");
        
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtTr0100.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTr_0_100())));
                txtTr595.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTr_5_95())));
                txtTr1090.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTr_10_90())));
                
                txtTs2.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTs_2())));
                txtTs5.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTs_5())));
                txtTs7.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTs_7())));
                txtTs10.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTs_10())));
                
                txtMpAbsoluto.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getMpAbs())));
                txtMp.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getMpPerc())));
                txtTp.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getTp())));
                
                txtEssAbsoluto.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getEssAbs())));
                txtEss.setText(String.valueOf(decimal.format(clienteControle.getVariaveisControle().getEssPerc())));
            }
        };
        
        this.timer = new Timer(100, action);
        this.timer.start();
    }
    
    public JPanel createContentPane(){

        // We create a bottom JPanel to place everything on.
        GradientPanel totalGUI = new GradientPanel();
        totalGUI.setLayout(null);
     
        // Panel para a TitleBar
        JPanel titleBarPanel = new JPanel();
        titleBarPanel.setLayout(null);
        titleBarPanel.setLocation(0, 0);
        titleBarPanel.setSize(840, 32);
        titleBarPanel.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarPanel);
        
        //Imagem thumb
        JButton thumbnail = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/thumbnail.png"));
            thumbnail.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        thumbnail.setLocation(6,2);
        thumbnail.setSize(24, 27);
        thumbnail.setBorderPainted(false); 
        thumbnail.setContentAreaFilled(false); 
        thumbnail.setFocusPainted(false); 
        thumbnail.setOpaque(false);
        
        titleBarPanel.add(thumbnail);
        
        // Título
        JLabel titleLabel = new JLabel("Sistema de Controle de Tanques");
        titleLabel.setLocation(32, 0);
        titleLabel.setSize(200, 32);
        titleBarPanel.add(titleLabel);    
        
        //Adicionando Botão de Fechar
        JButton closeButton = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("icones/closeButton.png"));
            closeButton.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        closeButton.setLocation(806,4);
        closeButton.setSize(24, 24);
        closeButton.setBorderPainted(false); 
        closeButton.setContentAreaFilled(false); 
        closeButton.setFocusPainted(false); 
        closeButton.setOpaque(false);
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        titleBarPanel.add(closeButton);
        
        //Panel Gráfico 1
        
        panelGrafico1 = new JPanel();  
        panelGrafico1.setOpaque(true);
        panelGrafico1.setLayout(null);
        panelGrafico1.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelGrafico1.setLocation(10, 42);
        panelGrafico1.setSize(408, 300);
        
        totalGUI.add(panelGrafico1);
        
        //Panel Gráfico 2
        
        panelGrafico2 = new JPanel();  
        panelGrafico2.setOpaque(true);
        panelGrafico2.setLayout(null);
        panelGrafico2.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelGrafico2.setLocation(424, 42);
        panelGrafico2.setSize(408, 300);
        
        totalGUI.add(panelGrafico2);
        
        //Panel Checkbox Grafico 1 & 2
        
        JPanel panelChkGrafico2 = new JPanel();  
        panelChkGrafico2.setOpaque(true);
        //panelChkGrafico1.setLayout(new BoxLayout(panelChkGrafico1, BoxLayout.LINE_AXIS));
        panelChkGrafico2.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelChkGrafico2.setLocation(9, 346);
        panelChkGrafico2.setSize(410, 110);
        //panelChkGrafico.add(Box.createRigidArea(new Dimension(0,0)));        
        
        JPanel panelChkGrafico1 = new JPanel();  
        panelChkGrafico1.setOpaque(true);
        //panelChkGrafico2.setLayout(new BoxLayout(panelChkGrafico2, BoxLayout.LINE_AXIS));
        panelChkGrafico1.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelChkGrafico1.setLocation(423, 346);
        panelChkGrafico1.setSize(410, 110);
        //panelChkGrafico.add(Box.createRigidArea(new Dimension(0,0))); 
        
        chkSetPointMestre = new JCheckBox("SetPoint - Mestre");
        chkSetPointMestre.setVisible(false);
        chkSetPointMestre.setSelected(true);
        chkSetPointMestre.setLocation(10,10);
        panelChkGrafico1.add(chkSetPointMestre);
        chkSetPointMestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSetPointMestreActionPerformed(evt);
            }
        });
        
        chkSetPointEscravo = new JCheckBox("SetPoint - Escravo");
        chkSetPointEscravo.setVisible(false);
        chkSetPointEscravo.setSelected(true);
        panelChkGrafico1.add(chkSetPointEscravo);
        chkSetPointEscravo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSetPointEscravoActionPerformed(evt);
            }
        });
        
        chkErroMestre = new JCheckBox("Erro - Mestre");
        chkErroMestre.setVisible(false);
        chkErroMestre.setSelected(true);
        panelChkGrafico1.add(chkErroMestre);
        chkErroMestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkErroMestreActionPerformed(evt);
            }
        });
        
        chkErroEscravo = new JCheckBox("Erro - Escravo");
        chkErroEscravo.setVisible(false);
        chkErroEscravo.setSelected(true);
        panelChkGrafico1.add(chkErroEscravo);
        chkErroEscravo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkErroEscravoActionPerformed(evt);
            }
        });
        
        chkCanal0 = new JCheckBox("Canal 0");
        chkCanal0.setSelected(true);
        panelChkGrafico1.add(chkCanal0);
        chkCanal0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkCanal0ActionPerformed(evt);
            }
        });
        
        chkCanal1 = new JCheckBox("Canal 1");
        chkCanal1.setSelected(true);
        panelChkGrafico1.add(chkCanal1);
        chkCanal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkCanal1ActionPerformed(evt);
            }
        });
        
        chkPMestre = new JCheckBox("P - Mestre");
        chkPMestre.setVisible(false);
        chkPMestre.setSelected(true);
        panelChkGrafico2.add(chkPMestre);
        chkPMestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPMestreActionPerformed(evt);
            }
        });
        
        chkIMestre = new JCheckBox("I - Mestre");
        chkIMestre.setVisible(false);
        chkIMestre.setSelected(true);
        panelChkGrafico2.add(chkIMestre);
        chkIMestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkIMestreActionPerformed(evt);
            }
        });
        
        chkDMestre = new JCheckBox("D - Mestre");
        chkDMestre.setVisible(false);
        chkDMestre.setSelected(true);
        panelChkGrafico2.add(chkDMestre);
        chkDMestre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkDMestreActionPerformed(evt);
            }
        });
        
        chkPEscravo = new JCheckBox("P - Escravo");
        chkPEscravo.setVisible(false);
        chkPEscravo.setSelected(true);
        panelChkGrafico2.add(chkPEscravo);
        chkPEscravo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPEscravoActionPerformed(evt);
            }
        });
        
        chkIEscravo = new JCheckBox("I - Escravo");
        chkIEscravo.setVisible(false);
        chkIEscravo.setSelected(true);
        panelChkGrafico2.add(chkIEscravo);
        chkIEscravo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkIEscravoActionPerformed(evt);
            }
        });
        
        chkDEscravo = new JCheckBox("D - Escravo");
        chkDEscravo.setVisible(false);
        chkDEscravo.setSelected(true);
        panelChkGrafico2.add(chkDEscravo);
        chkDEscravo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkDEscravoActionPerformed(evt);
            }
        });
        
        chkSaidaControlador = new JCheckBox("Saída Controlador");
        chkSaidaControlador.setVisible(false);
        chkSaidaControlador.setSelected(true);
        panelChkGrafico2.add(chkSaidaControlador);
        chkSaidaControlador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSaidaControladorActionPerformed(evt);
            }
        });
        
        chkSinalDesejado = new JCheckBox("Sinal Desejado");
        chkSinalDesejado.setVisible(false);
        chkSinalDesejado.setSelected(true);
        panelChkGrafico2.add(chkSinalDesejado);
        chkSinalDesejado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSinalDesejadoActionPerformed(evt);
            }
        });
        
        chkSinalErro = new JCheckBox("Sinal de Erro");
        chkSinalErro.setVisible(false);
        chkSinalErro.setSelected(true);
        panelChkGrafico1.add(chkSinalErro);
        chkSinalErro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSinalErroActionPerformed(evt);
            }
        });
        
        chkSinalEscrito = new JCheckBox("Sinal Escrito");
        chkSinalEscrito.setVisible(true);
        chkSinalEscrito.setSelected(true);
        panelChkGrafico2.add(chkSinalEscrito);
        chkSinalEscrito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSinalEscritoActionPerformed(evt);
            }
        });
        
        chkErroEstimacao1 = new JCheckBox("Erro - Tanque Inferior");
        chkErroEstimacao1.setVisible(false);
        chkErroEstimacao1.setSelected(true);
        panelChkGrafico1.add(chkErroEstimacao1);
        chkErroEstimacao1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkErroEstimacao1ActionPerformed(evt);
            }
        });

        chkSinalDesejadoNivel = new JCheckBox("Sinal Desejado");
        chkSinalDesejadoNivel.setVisible(false);
        chkSinalDesejadoNivel.setSelected(true);
        panelChkGrafico1.add(chkSinalDesejadoNivel);
        chkSinalDesejadoNivel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSinalDesejadoNivelActionPerformed(evt);
            }
        });

        nivelEstimado1 = new JCheckBox("Nível Estimado - Tanque Superior");
        nivelEstimado1.setVisible(false);
        nivelEstimado1.setSelected(true);
        panelChkGrafico1.add(nivelEstimado1);
        nivelEstimado1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nivelEstimado1ActionPerformed(evt);
            }
        });

        nivelEstimado2 = new JCheckBox("Nível Estimado - Tanque Inferior");
        nivelEstimado2.setVisible(false);
        nivelEstimado2.setSelected(true);
        panelChkGrafico1.add(nivelEstimado2);
        nivelEstimado2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nivelEstimado2ActionPerformed(evt);
            }
        });
        
        totalGUI.add(panelChkGrafico1);
        totalGUI.add(panelChkGrafico2);
        
        //Panel Tempo de Subida
        
        JPanel titleBarTr = new JPanel();
        titleBarTr.setLayout(null);
        titleBarTr.setLocation(10, 460);
        titleBarTr.setSize(160, 32);
        titleBarTr.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarTr);
            
        JLabel titleTr = new JLabel("Tempo de Subida");
        titleTr.setLocation(10, 0);
        titleTr.setSize(200, 32);
        titleBarTr.add(titleTr);
        
        JPanel panelTr = new JPanel();  
        panelTr.setOpaque(true);
        panelTr.setLayout(null);
        panelTr.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelTr.setLocation(10, 492);
        panelTr.setSize(160, 140);
        
        lblTr0100 = new JLabel("tr (0 - 100)");
        lblTr0100.setLocation(10, 10);
        lblTr0100.setSize(90, 30);
        panelTr.add(lblTr0100);
        
        txtTr0100 = new JTextField(8);
        txtTr0100.setLocation(80, 15);
        txtTr0100.setSize(70, 22);
        txtTr0100.setEnabled(false);
        panelTr.add(txtTr0100);
        
        lblTr595 = new JLabel("tr (5 - 95)");
        lblTr595.setLocation(10, 38);
        lblTr595.setSize(90, 30);
        panelTr.add(lblTr595);
        
        txtTr595 = new JTextField(8);
        txtTr595.setLocation(80, 42);
        txtTr595.setSize(70, 22);
        txtTr595.setEnabled(false);
        panelTr.add(txtTr595);
        
        lblTr1090 = new JLabel("tr (10 - 90)");
        lblTr1090.setLocation(10, 66);
        lblTr1090.setSize(90, 30);
        panelTr.add(lblTr1090);
        
        txtTr1090 = new JTextField(8);
        txtTr1090.setLocation(80, 69);
        txtTr1090.setSize(70, 22);
        txtTr1090.setEnabled(false);
        panelTr.add(txtTr1090);
        
        totalGUI.add(panelTr);
        
        //Panel Mp
        
        JPanel titleBarMp = new JPanel();
        titleBarMp.setLayout(null);
        titleBarMp.setLocation(175, 460);
        titleBarMp.setSize(160, 32);
        titleBarMp.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarMp);
            
        JLabel titleMp = new JLabel("Máximo Pico");
        titleMp.setLocation(10, 0);
        titleMp.setSize(200, 32);
        titleBarMp.add(titleMp);
        
        JPanel panelMp = new JPanel();  
        panelMp.setOpaque(true);
        panelMp.setLayout(null);
        panelMp.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelMp.setLocation(175, 492);
        panelMp.setSize(160, 140);
        
        lblMpAbsoluto = new JLabel("Mp");
        lblMpAbsoluto.setLocation(10, 10);
        lblMpAbsoluto.setSize(90, 30);
        panelMp.add(lblMpAbsoluto);
        
        txtMpAbsoluto = new JTextField(8);
        txtMpAbsoluto.setLocation(80, 15);
        txtMpAbsoluto.setSize(70, 22);
        txtMpAbsoluto.setEnabled(false);
        panelMp.add(txtMpAbsoluto);
        
        lblMp = new JLabel("Mp (%)");
        lblMp.setLocation(10, 38);
        lblMp.setSize(90, 30);
        panelMp.add(lblMp);
        
        txtMp = new JTextField(8);
        txtMp.setLocation(80, 42);
        txtMp.setSize(70, 22);
        txtMp.setEnabled(false);
        panelMp.add(txtMp);
        
        totalGUI.add(panelMp);
        
        //Panel Tp
        
        JPanel titleBarTp = new JPanel();
        titleBarTp.setLayout(null);
        titleBarTp.setLocation(340, 460);
        titleBarTp.setSize(160, 32);
        titleBarTp.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarTp);
            
        JLabel titleTp = new JLabel("Tempo de Pico");
        titleTp.setLocation(10, 0);
        titleTp.setSize(200, 32);
        titleBarTp.add(titleTp);
        
        JPanel panelTp = new JPanel();  
        panelTp.setOpaque(true);
        panelTp.setLayout(null);
        panelTp.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelTp.setLocation(340, 492);
        panelTp.setSize(160, 140);
        
        lblTp = new JLabel("tp");
        lblTp.setLocation(10, 10);
        lblTp.setSize(90, 30);
        panelTp.add(lblTp);
        
        txtTp = new JTextField(8);
        txtTp.setLocation(80, 15);
        txtTp.setSize(70, 22);
        txtTp.setEnabled(false);
        panelTp.add(txtTp);
        
        totalGUI.add(panelTp);
        
        //Panel Ts
        
        JPanel titleBarTs = new JPanel();
        titleBarTs.setLayout(null);
        titleBarTs.setLocation(505, 460);
        titleBarTs.setSize(160, 32);
        titleBarTs.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarTs);
            
        JLabel titleTs = new JLabel("Tempo de Acomodação");
        titleTs.setLocation(10, 0);
        titleTs.setSize(200, 32);
        titleBarTs.add(titleTs);
        
        JPanel panelTs = new JPanel();  
        panelTs.setOpaque(true);
        panelTs.setLayout(null);
        panelTs.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelTs.setLocation(505, 492);
        panelTs.setSize(160, 140);
        
        lblTs2 = new JLabel("ts (2%)");
        lblTs2.setLocation(10, 10);
        lblTs2.setSize(90, 30);
        panelTs.add(lblTs2);
        
        txtTs2 = new JTextField(8);
        txtTs2.setLocation(80, 15);
        txtTs2.setSize(70, 22);
        txtTs2.setEnabled(false);
        panelTs.add(txtTs2);
        
        lblTs5 = new JLabel("ts (5%)");
        lblTs5.setLocation(10, 38);
        lblTs5.setSize(90, 30);
        panelTs.add(lblTs5);
        
        txtTs5 = new JTextField(8);
        txtTs5.setLocation(80, 42);
        txtTs5.setSize(70, 22);
        txtTs5.setEnabled(false);
        panelTs.add(txtTs5);
        
        lblTs7 = new JLabel("ts (7%)");
        lblTs7.setLocation(10, 66);
        lblTs7.setSize(90, 30);
        panelTs.add(lblTs7);
        
        txtTs7 = new JTextField(8);
        txtTs7.setLocation(80, 69);
        txtTs7.setSize(70, 22);
        txtTs7.setEnabled(false);
        panelTs.add(txtTs7);
        
        lblTs10 = new JLabel("ts (10%)");
        lblTs10.setLocation(10, 94);
        lblTs10.setSize(90, 30);
        panelTs.add(lblTs10);
        
        txtTs10 = new JTextField(8);
        txtTs10.setLocation(80, 96);
        txtTs10.setSize(70, 22);
        txtTs10.setEnabled(false);
        panelTs.add(txtTs10);
        
        totalGUI.add(panelTs);
        
        //Panel Ess
        
        JPanel titleBarEss = new JPanel();
        titleBarEss.setLayout(null);
        titleBarEss.setLocation(670, 460);
        titleBarEss.setSize(160, 32);
        titleBarEss.setBackground(Color.LIGHT_GRAY);
        totalGUI.add(titleBarEss);
            
        JLabel titleEss = new JLabel("Erro de Regime");
        titleEss.setLocation(10, 0);
        titleEss.setSize(200, 32);
        titleBarEss.add(titleEss);
        
        JPanel panelEss = new JPanel();  
        panelEss.setOpaque(true);
        panelEss.setLayout(null);
        panelEss.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        panelEss.setLocation(670, 492);
        panelEss.setSize(160, 140);
        
        lblEssAbsoluto = new JLabel("Ess");
        lblEssAbsoluto.setLocation(10, 10);
        lblEssAbsoluto.setSize(90, 30);
        panelEss.add(lblEssAbsoluto);
        
        txtEssAbsoluto = new JTextField(8);
        txtEssAbsoluto.setLocation(80, 15);
        txtEssAbsoluto.setSize(70, 22);
        txtEssAbsoluto.setEnabled(false);
        panelEss.add(txtEssAbsoluto);
        
        lblEss = new JLabel("Ess (%)");
        lblEss.setLocation(10, 38);
        lblEss.setSize(90, 30);
        panelEss.add(lblEss);
        
        txtEss = new JTextField(8);
        txtEss.setLocation(80, 42);
        txtEss.setSize(70, 22);
        txtEss.setEnabled(false);
        panelEss.add(txtEss);
        
        totalGUI.add(panelEss);
        
        totalGUI.setOpaque(true);
        return totalGUI;
    }
    
    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) { 
        //System.exit(0);
        clienteControle.stop();
    }
    
    private void chkPMestreActionPerformed(java.awt.event.ActionEvent evt) {
        //if (datasetControlador.getSeriesIndex(clienteControle.getSinalCalculadoP().getKey()) != -1;
        
        if (!chkPMestre.isSelected())
            datasetControlador.removeSeries(clienteControle.getControlador().getSinalCalculadoP(""));
        else
            datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculadoP(""));
    }
    
    private void chkIMestreActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkIMestre.isSelected())
            datasetControlador.removeSeries(clienteControle.getControlador().getSinalCalculadoI(""));
        else
            datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculadoI(""));
    }
    
    private void chkDMestreActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkDMestre.isSelected())
            datasetControlador.removeSeries(clienteControle.getControlador().getSinalCalculadoD(""));
        else
            datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculadoD(""));
    }
    
    private void chkPEscravoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkPEscravo.isSelected())
            datasetControlador.removeSeries(clienteControle.getControladorE().getSinalCalculadoP(""));
        else
            datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculadoP(""));
    }
    
    private void chkIEscravoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkIEscravo.isSelected())
            datasetControlador.removeSeries(clienteControle.getControladorE().getSinalCalculadoI(""));
        else
            datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculadoI(""));
    }
        
    private void chkDEscravoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkDEscravo.isSelected())
            datasetControlador.removeSeries(clienteControle.getControladorE().getSinalCalculadoD(""));
        else
            datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculadoD(""));
    }
    
    private void chkSaidaControladorActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkSaidaControlador.isSelected()) {
            datasetControlador.removeSeries(clienteControle.getControlador().getSinalCalculado(cascata ? "PID - Mestre" : ""));
            datasetControlador.removeSeries(clienteControle.getControladorE().getSinalCalculado("PID - Escravo"));
        }
        else {
            datasetControlador.addSeries(clienteControle.getControlador().getSinalCalculado(cascata ? "PID - Mestre" : ""));
            datasetControlador.addSeries(clienteControle.getControladorE().getSinalCalculado("PID - Escravo"));
        }
    }
    
    private void chkSinalDesejadoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkSinalDesejado.isSelected()) {
            if (cascata) {
                datasetControlador.removeSeries(clienteControle.getBaseControle().getSinalMA("Sinal Desejado"));
                datasetNivel.removeSeries(clienteControle.getComparador().getSinalDesejado("SetPoint - Mestre"));
                datasetNivel.removeSeries(clienteControle.getComparadorE().getSinalDesejado("SetPoint - Escravo"));
            }
        }
        else {
            if (cascata) {
                datasetControlador.addSeries(clienteControle.getBaseControle().getSinalMA("Sinal Desejado"));
                datasetNivel.addSeries(clienteControle.getComparador().getSinalDesejado("SetPoint - Mestre"));
                datasetNivel.addSeries(clienteControle.getComparadorE().getSinalDesejado("SetPoint - Escravo"));
            }
        }
    }
    
    private void  chkSinalDesejadoNivelActionPerformed (java.awt.event.ActionEvent evt) {
        if (!chkSinalDesejadoNivel.isSelected())
            datasetNivel.removeSeries(clienteControle.getBaseControle().getSinalMA(""));
        else
            datasetNivel.addSeries(clienteControle.getBaseControle().getSinalMA(""));
    }
    
    private void chkSinalErroActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkSinalErro.isSelected()) {
            datasetNivel.removeSeries(clienteControle.getComparador().getSinalErro(cascata ? "Erro - Mestre" : ""));
            datasetNivel.removeSeries(clienteControle.getComparadorE().getSinalErro("Erro - Escravo"));
        }
        else {
            datasetNivel.addSeries(clienteControle.getComparador().getSinalErro(cascata ? "Erro - Mestre" : ""));
            datasetNivel.addSeries(clienteControle.getComparadorE().getSinalErro("Erro - Escravo"));
        }
    }
    
    private void chkSinalEscritoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkSinalEscrito.isSelected()) {
            if (!obsEstados)
                datasetNivel.removeSeries(clienteControle.getBaseControle().getSinalEscrito(""));
            else
                datasetTensao.removeSeries(clienteControle.getBaseControle().getSinalEscrito(""));
        }
        else {
            if (!obsEstados)
                datasetNivel.addSeries(clienteControle.getBaseControle().getSinalEscrito(""));
            else
                datasetTensao.addSeries(clienteControle.getBaseControle().getSinalEscrito(""));
        }
    }
    
    private void chkCanal0ActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkCanal0.isSelected())
            datasetNivel.removeSeries(clienteControle.getBaseControle().getSinalCanal0(""));
        else
            datasetNivel.addSeries(clienteControle.getBaseControle().getSinalCanal0(""));
    }
    
    private void chkCanal1ActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkCanal1.isSelected())
            datasetNivel.removeSeries(clienteControle.getBaseControle().getSinalCanal1(""));
        else
            datasetNivel.addSeries(clienteControle.getBaseControle().getSinalCanal1(""));
    }
    
    private void chkSetPointMestreActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkSetPointMestre.isSelected())
            datasetNivel.removeSeries(clienteControle.getComparador().getSinalDesejado("SetPoint - Mestre"));
        else
            datasetNivel.addSeries(clienteControle.getComparador().getSinalDesejado("SetPoint - Mestre"));
    }
    
    private void chkSetPointEscravoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkSetPointEscravo.isSelected())
            datasetNivel.removeSeries(clienteControle.getComparadorE().getSinalDesejado("SetPoint - Escravo"));
        else
            datasetNivel.addSeries(clienteControle.getComparadorE().getSinalDesejado("SetPoint - Escravo"));
    }
    
    private void chkErroMestreActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkErroMestre.isSelected())
            datasetNivel.removeSeries(clienteControle.getComparador().getSinalErro("Erro - Mestre"));
        else
            datasetNivel.addSeries(clienteControle.getComparador().getSinalErro("Erro - Mestre"));
    }
    
    private void chkErroEscravoActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkErroEscravo.isSelected())
            datasetNivel.removeSeries(clienteControle.getComparadorE().getSinalErro("Erro - Escravo"));
        else
            datasetNivel.addSeries(clienteControle.getComparadorE().getSinalErro("Erro - Escravo"));
    }
    
    private void chkErroEstimacao1ActionPerformed(java.awt.event.ActionEvent evt) {
        if (!chkErroEstimacao1.isSelected())
            datasetNivel.removeSeries(clienteControle.getEstimador().getSinalU());
        else
            datasetNivel.addSeries(clienteControle.getEstimador().getSinalU());
    }
    
    private void nivelEstimado1ActionPerformed(java.awt.event.ActionEvent evt) {
        if (!nivelEstimado1.isSelected())
            datasetNivel.removeSeries(clienteControle.getEstimador().getSinalX1Est());
        else
            datasetNivel.addSeries(clienteControle.getEstimador().getSinalX1Est());
    }
    
    private void nivelEstimado2ActionPerformed(java.awt.event.ActionEvent evt) {
        if (!nivelEstimado2.isSelected())
            datasetNivel.removeSeries(clienteControle.getEstimador().getSinalYEst());
        else
            datasetNivel.addSeries(clienteControle.getEstimador().getSinalYEst());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        /*java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Grafico().setVisible(true);
            }
        });*/
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
