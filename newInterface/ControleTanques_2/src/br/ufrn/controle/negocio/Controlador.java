/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.negocio;

import br.ufrn.controle.modelo.Controles;
import org.jfree.data.xy.XYSeries;

/**
 *
 * @author Adriana
 */
public class Controlador {
    
    private double _erro;
    private double _pv;
    private double _time;
    
    private double _acaoP;
    private double _acaoI;
    private double _acaoD;
    
    private double h = 0.1;
    private double erro_integral_anterior;
    private double erro_anterior;
    private double nivel_anterior;
    
    private XYSeries _sinalControladorP = new XYSeries("Saída P");
    private XYSeries _sinalControladorI = new XYSeries("Saída I");
    private XYSeries _sinalControladorD = new XYSeries("Saída D");
    private XYSeries _sinalControlador = new XYSeries("Saída Controlador");
    

    public Controlador() {
        erro_integral_anterior = 0.0;
        erro_anterior = 0.0;
        nivel_anterior = 0.0;
    }
    
    public double getSaida(Controles tipoControlador, double kp, double ki, double kd, double erro, double pv) {
        _erro = erro;
        _pv = pv;
        double controlador = 0.0;
        
        if (tipoControlador == Controles.P)
            controlador = P(kp);
        else if (tipoControlador == Controles.PI)
            controlador = PI(kp,ki);
        else if (tipoControlador == Controles.PD)
            controlador = PD(kp,kd);
        else if (tipoControlador == Controles.PID)
            controlador = PID(kp,ki,kd);
        else if (tipoControlador == Controles.PI_D)
            controlador = PI_D(kp,ki,kd);
        
        return controlador;
    }
    
    public double P(double kp){
        double P = _erro * kp;
        _acaoP = P;
        return P;
    }
    
    public double I(double ki) {
        double I = erro_integral_anterior + ki*h*_erro;
        _acaoI = I;
        return I;
    }
    
    public double D(double kd) {
        double D = kd * (_erro-erro_anterior)/h;
        _acaoD = D;
        return D;
    }
    
    public double _D(double kd) {
        double _D = kd * (_pv-nivel_anterior)/h;
        _acaoD = _D;
        return _D;
    }
    
    public double PD(double kp, double kd){
        double aux = (P(kp) + D(kd));
        erro_anterior = _erro;
        return aux;
    }
    
    public double PI(double kp, double ki) {
        double I = I(ki);
        double aux = (P(kp) + I);
        erro_integral_anterior = I;
        return aux;
    }
    
    public double PID(double kp, double ki, double kd) {
        double I = I(ki);
        double aux = (P(kp) + I + D(kd));
        erro_anterior = _erro;
        erro_integral_anterior = I;
        return aux;
    }
    
    public double PI_D(double kp, double ki, double kd) {
        double I = I(ki);
        double aux = (P(kp) + I + _D(kd));
        erro_integral_anterior = I;
        nivel_anterior = _pv;
        return aux;
    }
    
    
    public void addSinais(double time, double acaoP, double acaoI, double acaoD, double acaoControlador)
    {
        _sinalControlador.add(time,acaoControlador);
        _sinalControladorP.add(time,acaoP);
        _sinalControladorI.add(time,acaoI);
        _sinalControladorD.add(time,acaoD);
    }
    
    
    public XYSeries getSinalCalculado(String descricao) {
        if (!descricao.equals(""))
            _sinalControlador.setKey(descricao);
        
        return _sinalControlador;
    }
    
    public XYSeries getSinalCalculadoP(String descricao) {
        if (!descricao.equals(""))
            _sinalControladorP.setKey(descricao);
        
        return _sinalControladorP;
    }
    
    public XYSeries getSinalCalculadoI(String descricao) {
        if (!descricao.equals(""))
            _sinalControladorI.setKey(descricao);
        
        return _sinalControladorI;
    }
    
    public XYSeries getSinalCalculadoD(String descricao) {
        if (!descricao.equals(""))
            _sinalControladorD.setKey(descricao);
        
        return _sinalControladorD;
    }
    
    
    public double getAcaoP() {
        return _acaoP;
    }
    
    public double getAcaoI() {
        return _acaoI;
    }
    
    public double getAcaoD() {
        return _acaoD;
    }        
}


