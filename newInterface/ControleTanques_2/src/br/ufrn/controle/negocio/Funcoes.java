/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.negocio;

import br.ufrn.controle.modelo.Funcao;
import java.util.Random;

/**
 *
 * @author Adriana
 */
public class Funcoes {
    
    // Valores auxiliares para configuração randômica
    private double periodoRandom = -1; //segundos
    private double amplitudeRandom;
    
    
    public double getDegrau(double amplitude, double offset){
         return (amplitude + offset);
     }

    public double getSenoide(double amplitude, double periodo, double offset, double tempo){
         return ((amplitude * Math.sin((2.0*Math.PI*tempo)/periodo)) + offset);
     }

    public double getQuadrada(double amplitude, double periodo, double offset, double tempo){ 
         if (tempo % periodo <= (periodo/2.0)) {
             return (offset - amplitude);
         }
         else{
             return (offset + amplitude);
         }
     }
     
    public double getDenteSerra(double amplitude, double periodo, double offset, double tempo){
         return (((amplitude / (periodo)) * ((tempo/4) % ((periodo+0.125)*2))) - (amplitude)+ offset);
     }
    
    
    public double getRetornoFuncao(Funcao funcao, double amplitude, double offset, double periodo, double tempo) {
        double y = 0.0;
        
        if (funcao == Funcao.Degrau)
            y = getDegrau(amplitude, offset);
        else if (funcao == Funcao.Senoide)
            y = getSenoide(amplitude, periodo, offset, tempo);
        else if (funcao == Funcao.DenteDeSerra)
            y = getDenteSerra(amplitude, periodo, offset, tempo);
        else if (funcao == Funcao.Quadrada)
            y = getQuadrada(amplitude, periodo, offset, tempo);
        
        return y;
    }

    public double getRetornoFuncaoRandom(double amp_min, double amp_max, double per_min, double per_max, double tempo) {   
        if (tempo >= periodoRandom) {
           periodoRandom = (per_min + (int) (Math.random() * ((per_max - per_min) + 1)));
           amplitudeRandom = amp_min + (amp_max - amp_min) * new Random().nextDouble();
        }
        
        return amplitudeRandom;
    }
}
