/**
 * \file ClienteControleTanque.java
 * 
 * \brief Este arquivo contém a chamada da aplicação referente ao controle de uma planta de tanques.
 * 
 * \author 
 * Adriana Benício Galvão \n
 * Bruno de Almeida Silva \n
 * Daniel Silva de Oliveira \n
 * Petrucio Ricardo Tavares de Medeiros \n
 * Talison Augusto Correia de Melo \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * adriana_benicio06 at yahoo (dot) com (dot) br \n
 * brunoalmeidarn at outlook (dot) com \n
 * tres.daniel at hotmail (dot) com \n
 * petrucior at gmail (dot) com \n
 * talisonaugusto at hotmail (dot) com
 * \version 0.0
 * \date February 2014
 */

package br.ufrn.controle.negocio;

import br.ufrn.controle.interfaceCliente.TelaCliente;
import br.ufrn.controle.modelo.Funcao;
import br.ufrn.controle.modelo.Malha;
import br.ufrn.controle.modelo.ParametrosEntrada;
import br.ufrn.dca.controle.QuanserClient;
import br.ufrn.dca.controle.QuanserClientException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.SwingUtilities;


public class ClienteControle {

    final private TelaCliente telaCliente;
    final private Funcoes funcoes;
    final private Filters filtros;
    final private VariaveisControle variaveisControle;
    final private ParametrosEntrada parametrosEntrada;
    final private BaseControle base;
    final private Comparador comparador;
    final private Comparador comparadorE;
    final private Controlador controlador;
    final private Controlador controladorE;
    final private Estimador estimador;
    
    private QuanserClient quanserClient;
    private double time = 0;
    private boolean conectado = false;
    
    //Constante de multiplicação para cm
    private double v_to_cm = 6.25;
    
    //Variáveis de controle
    private double sensor0 = 0;
    private double sensor1 = 0;
    private double setPoint = 0;
    private double sinalMA = 0;
    private double pv = 0;
    private double erro = 0;
    private double erroE = 0;
    private double acaoP = 0;
    private double acaoI = 0;
    private double acaoD = 0;
    private double acaoPE = 0;
    private double acaoIE = 0;
    private double acaoDE = 0;
    private double saidaControlador = 0;
    private double saidaControladorE = 0;
    private double sinalEscrito = 0;
    
    //Observação de estados
    private double x1Est;
    private double x2Est;
    private double yEst;
    private double y;
    private double u;
    
    
    public ClienteControle(final TelaCliente telaCliente, final ParametrosEntrada parametros) {
        this.telaCliente = telaCliente;
        this.parametrosEntrada = parametros;
        
        this.funcoes = new Funcoes();
        this.filtros = new Filters();
        this.variaveisControle = new VariaveisControle();
        this.comparador = new Comparador();
        this.comparadorE = new Comparador();
        this.controlador = new Controlador();
        this.controladorE = new Controlador();
        this.base = new BaseControle();
        this.estimador = new Estimador();
    }
    
    final public void conectar() {
        try {
            quanserClient = new QuanserClient(telaCliente.getIp(), Integer.parseInt(telaCliente.getPorta()));
            conectado = true;
        }
        catch (QuanserClientException ex) {
            conectado = false;
            System.out.print("Erro ao tentar conexão");
        }
    }
    
    final public void desconectar() {
        if (conectado) {
            System.out.print("Desconectando...");
            try {
                quanserClient.write(0, 0);
                System.out.print("Desconectado com sucesso.");
            }
            catch(Exception ex) {
                System.out.print("Erro ao enviar tensão 0.");
                System.out.print(ex.getMessage());
            }
        }
        else
            System.out.print("Cliente desconectado");
        
        conectado = false;
    }
    
    private ClienteControle me() {
        return this;
    }
    
    public boolean conectado() {
        return this.conectado;
    }
    
    
    public void iniciar(String ip, String porta) 
    {
        if (!conectado)
            conectar();
        
        if (conectado) {           
            final ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
            exec.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    long startTempo, endTempo;
                    try {
                        startTempo = System.currentTimeMillis(); 
                        if (!telaCliente.parado()) {
                            
                            sensor0 = v_to_cm * quanserClient.read(0);
                            if (parametrosEntrada.canal1 || parametrosEntrada.canalPv == 1)
                                sensor1 = v_to_cm * quanserClient.read(1);

                            if (parametrosEntrada.funcao == Funcao.Random)
                                sinalMA = funcoes.getRetornoFuncaoRandom(parametrosEntrada.amplitudeMin, parametrosEntrada.amplitudeMax, parametrosEntrada.periodoMin, parametrosEntrada.periodoMax, time);
                            else
                                sinalMA = funcoes.getRetornoFuncao(parametrosEntrada.funcao, parametrosEntrada.amplitude, parametrosEntrada.offset, parametrosEntrada.periodo, time);
                            
                            if (!parametrosEntrada.obsEstados) {
                                if (parametrosEntrada.malha == Malha.Fechada) {
                                    setPoint = sinalMA;

                                    if (parametrosEntrada.canalPv == 0)
                                        pv = sensor0;
                                    else
                                        pv = sensor1;

                                    erro = setPoint - pv;
                                    saidaControlador = controlador.getSaida(parametrosEntrada.controle, parametrosEntrada.kp, parametrosEntrada.ki, parametrosEntrada.kd, erro, pv);
                                    acaoP = controlador.getAcaoP();
                                    acaoI = controlador.getAcaoI();
                                    acaoD = controlador.getAcaoD();

                                    if (parametrosEntrada.controleCascata) {
                                        erroE = saidaControlador - sensor0; //pv = sensor0?
                                        saidaControladorE = controladorE.getSaida(parametrosEntrada.controleE, parametrosEntrada.kpE, parametrosEntrada.kiE, parametrosEntrada.kdE, erroE, sensor0);
                                        acaoPE = controladorE.getAcaoP();
                                        acaoIE = controladorE.getAcaoI();
                                        acaoDE = controladorE.getAcaoD();
                                    }

                                    sinalEscrito = base.aplicarTravas(saidaControlador, sensor0);
                                }
                                else {
                                    sinalEscrito = base.aplicarTravas(sinalMA, sensor0);
                                }
                            }
                            else {
                                setPoint = sinalMA;
                                y = sensor1;
                                u = base.aplicarTravas(setPoint-y, sensor0);
                                estimador.updateEstimativas(parametrosEntrada.polo1_a, parametrosEntrada.polo1_b, parametrosEntrada.polo2_a, parametrosEntrada.polo2_b, y, u);
                                x1Est = estimador.getX1Est();
                                x2Est = estimador.getX2Est();
                                yEst = estimador.getYEst();
                                sinalEscrito = u;
                            }
                            
                            quanserClient.write(parametrosEntrada.canalEscrita, sinalEscrito);
                            
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    base.addSinais(time, sensor0, sensor1, sinalMA, sinalEscrito);
                                    
                                    if (!parametrosEntrada.obsEstados) {
                                        if (parametrosEntrada.malha == Malha.Fechada) {
                                            comparador.addSinais(time, setPoint, erro);
                                            comparadorE.addSinais(time, saidaControlador, erroE);
                                            variaveisControle.avaliandoSinal(pv, setPoint, time);

                                            if (parametrosEntrada.controleCascata) {
                                                controlador.addSinais(time, acaoP, acaoI, acaoD, saidaControlador);
                                                controladorE.addSinais(time, acaoPE, acaoIE, acaoDE, saidaControladorE);
                                            }
                                        }
                                    }
                                    else
                                        estimador.addSinais(time, x1Est, x2Est, yEst, y, u);
                                }
                            });
                            
                            time = time+0.1;
                        }
                        else
                        {
                            exec.shutdownNow();
                            me().desconectar();
                        }
                        endTempo=System.currentTimeMillis();
                        System.out.println(endTempo - startTempo);
                    }
                    catch(Exception ex) {
                        exec.shutdownNow();
                        me().desconectar();
                        ex.printStackTrace();
                    }
                }
            }, 100, 100, TimeUnit.MILLISECONDS);
        }
    }
    

    public BaseControle getBaseControle() {
        return base;
    }
    
    public Controlador getControlador() {
        return controlador;
    }
    
    public Controlador getControladorE() {
        return controladorE;
    }
    
    public Comparador getComparador() {
        return comparador;
    }
    
    public Comparador getComparadorE() {
        return comparadorE;
    }
    
    public VariaveisControle getVariaveisControle() {
        return variaveisControle;
    }
    
    public ParametrosEntrada getParametrosEntrada() {
        return parametrosEntrada;
    }
    
    public Estimador getEstimador() {
        return estimador;
    }
    
    
    public boolean running() {
        return (!telaCliente.parado());
    }
    
    public void stop() {
        if (this.running())
            this.telaCliente.clique_botao_iniciar();
    }
}
