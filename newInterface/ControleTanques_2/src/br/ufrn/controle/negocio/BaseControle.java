/**
 * \file LogicaControle.java
 * 
 * \brief Este arquivo contém a lógica do programa referente ao controle de uma planta de tanques.
 * 
 * \Author 
 * Adriana Benício Galvão \n
 * Bruno de Almeida Silva \n
 * Daniel Silva de Oliveira \n
 * Petrucio Ricardo Tavares de Medeiros \n
 * Talison Augusto Correia de Melo \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * adriana_benicio06 at yahoo (dot) com (dot) br \n
 * brunoalmeidarn at outlook (dot) com \n
 * tres.daniel at hotmail (dot) com \n
 * petrucior at gmail (dot) com \n
 * talisonaugusto at hotmail (dot) com
 * \version 0.0
 * \date February 2014
 */

package br.ufrn.controle.negocio;

import org.jfree.data.xy.XYSeries;

/*
 * \class BaseControle
 * 
 * \brief Esta classe é a responsável por toda a lógica do projeto
 */

public class BaseControle {
    
    private XYSeries _sinalCanal0 = new XYSeries("Canal 0");
    private XYSeries _sinalCanal1 = new XYSeries("Canal 1");
    private XYSeries _sinalMA = new XYSeries("Sinal Desejado");
    private XYSeries _sinalEscrito = new XYSeries("Sinal Saturado");


    public BaseControle(){ }
    
    
    public double aplicarTravas(double tensao, double canal0) {
        double v = tensao;

        //Trava 1
        if (tensao < -3)
            v = -3;
        else if (tensao > 3)
            v = 3;
        
        //Trava 2
        if (canal0 > 28 && tensao > 1.8)
            v = 1.8;
        
        //Travas 3 e 4
        if ((canal0 > 29 && tensao > 0) || (canal0 < 3 && tensao < 0))
            v = 0;

        return v;
    }

    public void addSinais(double time, double canal0, double canal1, double sinalMA, double sinalEscrito) {
        _sinalCanal0.add(time, canal0);
        _sinalCanal1.add(time, canal1);
        _sinalMA.add(time,sinalMA);
        _sinalEscrito.add(time,sinalEscrito);
    }
    
    public XYSeries getSinalEscrito(String descricao) {
        if (!descricao.equals(""))
            _sinalEscrito.setKey(descricao);
        
        return _sinalEscrito;
    }
    
    public XYSeries getSinalCanal0(String descricao) {
        if (!descricao.equals(""))
            _sinalCanal0.setKey(descricao);
        
        return _sinalCanal0;
    }
    
    public XYSeries getSinalCanal1(String descricao) {
        if (!descricao.equals(""))
            _sinalCanal1.setKey(descricao);
        
        return _sinalCanal1;
    }
    
    public XYSeries getSinalMA(String descricao) {
        if (!descricao.equals(""))
            _sinalMA.setKey(descricao);
        
        return _sinalMA;
    }
}