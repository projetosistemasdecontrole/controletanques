package br.ufrn.controle.negocio;

import Jama.Matrix;
import java.lang.Math;

public class ObservadorEstados {
        private double p1_a, p1_b;	// Primeiro polo complexo
	private double p2_a, p2_b;	// Segundo polo complexo

	private double[][] array_G = {{0.99346112579, 0},{0.00651745267, 0.99346112579}};
	private double[][] array_H = {{0.28691757383}, {0.00009681057}};
	private double[][] array_C = {{0}, {1}};
	private double[][] array_inv_W = {{-152.430977633, 153.434264991}, {1, 0}};
        private double[][] array_q = {{0, 0}, {0, 0}};
        private double[][] array_i = {{1, 0}, {0, 1}};
        private double[][] array_l = {{0}, {0}};
        
        // Valores iniciais dos estimadores
        private double[][] array_x_estimador = {{0}, {0}};
        private double[][] array_y_estimador = {{0}, {0}};
        
        Matrix G = new Matrix(array_G);
        Matrix H = new Matrix(array_H);
        Matrix C = new Matrix(array_C);
        Matrix inv_W = new Matrix(array_inv_W);
        Matrix Q = new Matrix(array_q);
        Matrix I = new Matrix(array_i);
        Matrix L = new Matrix(array_l);
        
        // Estimadores
        Matrix x_estimador = new Matrix(array_x_estimador);
        Matrix y_estimador = new Matrix(array_y_estimador);
        
        // Variáveis auxiliares
	private double p_a_soma, p_b_soma;
	private double p_a_mult, p_b_mult;
        
        // Construtor
	// p1 = a + bj e p2 = a + bj são valores complexos dos polos
	public ObservadorEstados(double _p1_a, double _p1_b, double _p2_a, double _p2_b){
            // Pegando os valores dos polos
            p1_a = _p1_a;
            p1_b = _p1_b;
            p2_a = _p2_a;
            p2_b = _p2_b;

            // Iniciar valores auxiliares
            p_a_soma = 0;
            p_b_soma = 0;
            p_a_mult = 0;
            p_b_mult = 0;
	}
        
        // Soma de números complexos
	public void somaCmpx(){
            p_a_soma = p1_a + p2_a;
            p_b_soma = p1_b + p2_b;
	}

	// Múltiplicação de números complexos
	public void multCmpx(){
            p_a_mult = ((p1_a * p2_a) + (p1_b * p2_b));
            p_b_mult = ((p1_a * p2_b) + (p1_b * p2_a));
	}
        
        // Método que calcula L através dos polos
	public Matrix calc_L(){
            this.somaCmpx();
            this.multCmpx();
            Q = ((G.times(G)).minus(G.times(p_a_soma))).plus(I.times(p_a_mult));
            //Q.print(1, 10);
            L = (Q.times(inv_W)).times(C);
            //L.print(1, 10);
            return L;
 	}
        
        // Método que estima o valor de x e y a partir da saída y e do erro u
        /*public void estimador(double y, double u){
            Matrix g_x = G.times(x_estimador);
            g_x.print(1, 10);
            
        }

        // Método de estimação dos estados
        public Matrix estimacaoEstados(Matrix x){
            return (G.times(x.minus(x_estimador)).minus(L.times(C.times(x.minus(x_estimador)))));
        }
        
        public void print(){
            calc_L();
            double y = 14.8;
            double u = 0.2;
            estimador(y, u);
	}*/
        
        public double[] getL() {
            this.calc_L();
            //L.print(1, 10);
            double[] l = new double[2];
            l[0] = L.get(0,0);
            l[1] = L.get(1,0);
            return l;
        }
        
        // Método para pegar os polos a partir de L
	public double[] getPolos(double l1, double l2){
            Matrix inputL = new Matrix(2, 1);
            inputL.set(0,0,l1);
            inputL.set(1,0,l2);

            Matrix M = (G.minus(inputL.times(C.transpose())));

            double delta = (Math.pow(M.get(0,0), 2) + Math.pow(M.get(1,1), 2)) - (2 * M.get(0,0) * M.get(1,1)) + (4 * M.get(0,1) * M.get(1,0));
            double[] polos = new double[4];
            if ( delta >= 0 ){
                    polos[0] = ((M.get(0,0) + M.get(1,1)) + Math.sqrt(delta))/2; // Real p1
                    polos[1] = 0; // Imag p1
                    polos[2] = ((M.get(0,0) + M.get(1,1)) - Math.sqrt(delta))/2; // Real p2
                    polos[3] = 0; // Imag p2
            }
            else{
                    polos[0] = (M.get(0,0) + M.get(1,1))/2; // Real p1
                    polos[1] = Math.sqrt(Math.pow((M.get(0, 0) - M.get(1, 1))/2, l2) + (M.get(0, 2)*M.get(1, 0))); // Imag p1
                    //polos[1] = (Math.sqrt(Math.abs(delta)))/2; // Imag p1
                    polos[2] = (M.get(0,0) + M.get(1,1))/2; // Real p2
                    polos[1] = -(Math.sqrt(Math.pow((M.get(0, 0) - M.get(1, 1))/2, l2) + (M.get(0, 2)*M.get(1, 0)))); // Imag p2
                    //polos[3] = -(Math.sqrt(Math.abs(delta)))/2; // Imag p2
            }
            return polos;
	}
}