/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.negocio;

import org.jfree.data.xy.XYSeries;

/**
 *
 * @author Adriana
 */
public class Comparador {
    
    private XYSeries _sinalDesejado = new XYSeries("Sinal Desejado");
    private XYSeries _sinalErro = new XYSeries("Sinal de Erro");
    
    public void addSinais(double time, double setpoint, double erro) {
        _sinalDesejado.add(time,setpoint);
        _sinalErro.add(time,erro);
    }
    
    public XYSeries getSinalDesejado(String descricao) {
        if (!descricao.equals(""))
            _sinalDesejado.setKey(descricao);
        
        return _sinalDesejado;
    }
    
    public XYSeries getSinalErro(String descricao) {
        if (!descricao.equals(""))
            _sinalErro.setKey(descricao);
        
        return _sinalErro;
    }
}
