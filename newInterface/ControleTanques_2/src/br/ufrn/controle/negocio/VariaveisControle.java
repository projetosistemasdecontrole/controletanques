/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.negocio;

public class VariaveisControle {
	// Atributos do tempo de subida (tr)
	private double tr_0_100_inicio;
	private double tr_0_100_fim;
	private double tr_5_95_inicio;
	private double tr_5_95_fim;
	private double tr_10_90_inicio;
	private double tr_10_90_fim;

	// Atributos do tempo de overshoot (mp) e tempo de pico (tp)
	private double mp;
	private double tp;
        private double tpInicial;

	// Atributos do tempo de acomodação
	private double ts_2;
	private double ts_5;
	private double ts_7;
	private double ts_10;
        private double ts_2_inicial;
        private double ts_5_inicial;
        private double ts_7_inicial;
        private double ts_10_inicial;
        
        // Atributos do erro de regime
        private int numberPontos;
        private double erroRegime[];

	// Variaveis auxiliares	
	private boolean tr_0_100_f_encontrado;
	private boolean tr_5_95_i_encontrado;
	private boolean tr_5_95_f_encontrado;
	private boolean tr_10_90_i_encontrado;	
	private boolean tr_10_90_f_encontrado;
	private boolean mp_encontrado;

	private boolean pico_inferior_limite_2;
	private boolean pico_superior_limite_2;

	private boolean pico_inferior_limite_5;
	private boolean pico_superior_limite_5;
        
        private boolean pico_inferior_limite_7;
        private boolean pico_superior_limite_7;
        
        private boolean pico_inferior_limite_10;
        private boolean pico_superior_limite_10;

        private double setPoint;
        private double nivelInicial;
        private double guardarSetpoint;
        
        private double ts_anterior;
        private int contador;
        private double mediaErro;
        
	// Construtor
	public VariaveisControle(){
            tpInicial = 0.0;
            ts_2_inicial = 0.0;
            ts_5_inicial = 0.0;
            ts_7_inicial = 0.0;
            ts_10_inicial = 0.0;
            guardarSetpoint = 0.0;
            
            numberPontos = 20;
            erroRegime = new double[numberPontos];
            ts_anterior = 0.0;
            contador = 0;
            mediaErro = 0.0;
            
            zeraValores();
            zeraAuxiliares();
	}

	// Método que zera valores
	public void zeraValores(){
		//tr_0_100_inicio = 0.0;
		//tr_0_100_fim = 0.0;
		//tr_5_95_inicio = 0.0;
		//tr_5_95_fim = 0.0;
		//tr_10_90_inicio = 0.0;
		//tr_10_90_fim = 0.0;
		mp = 0.0;
		//tp = 0.0;
		ts_2 = 0.0;
		ts_5 = 0.0;
		ts_7 = 0.0;
		ts_10 = 0.0;                
	}

	// Método que zera valores auxiliares
	public void zeraAuxiliares(){
		tr_0_100_f_encontrado = false;
		tr_5_95_i_encontrado = false;
		tr_5_95_f_encontrado = false;
		tr_10_90_i_encontrado = false;	
		tr_10_90_f_encontrado = false;
		mp_encontrado = false;

                pico_inferior_limite_2 = false;
                pico_superior_limite_2 = false;
                pico_inferior_limite_5 = false;
                pico_superior_limite_5 = false;
                pico_inferior_limite_7 = false;
                pico_superior_limite_7 = false;
                pico_inferior_limite_10 = false;
                pico_superior_limite_10 = false;
        }

	// Método que calcula tempo de subida, overshoot, tempo de acomodação
	public void avaliandoSinal(double nivel, double setpoint, double tempo){
                if (setPoint != setpoint) // Quando houver alguma alteração de setpoint
                {
                    guardarSetpoint = setPoint;
                    tr_0_100_inicio = tempo;
                    tr_5_95_inicio = tempo;
                    tr_10_90_inicio = tempo;
                    tpInicial = tempo;
                    ts_2_inicial = tempo;
                    ts_5_inicial = tempo;
                    ts_7_inicial = tempo;
                    ts_10_inicial = tempo;
                    this.zeraAuxiliares();
                    this.zeraValores();
                    nivelInicial = nivel;
                    if (nivelInicial >= setpoint) mp = 100;
                }
                setPoint = setpoint;
                
		if (nivelInicial < setpoint)
                {   
                    // Nível antes do degrau menor que o setpoint
                    // Tempo de subida
                    if ((nivel <= setpoint) && (tr_0_100_f_encontrado == false)) tr_0_100_fim = tempo;
                    else tr_0_100_f_encontrado = true;
                    if ((nivel <= (setpoint * 5)/100) && (tr_5_95_i_encontrado == false)) tr_5_95_inicio = tempo;
                    else tr_5_95_i_encontrado = true;
                    if ((nivel <= (setpoint * 95)/100) && (tr_5_95_f_encontrado == false)) tr_5_95_fim = tempo;
                    else tr_5_95_f_encontrado = true;
                    if ((nivel <= (setpoint * 10)/100) && (tr_10_90_i_encontrado == false)) tr_10_90_inicio = tempo;
                    else tr_10_90_i_encontrado = true;
                    if ((nivel <= (setpoint * 90)/100) && (tr_10_90_f_encontrado == false)) tr_10_90_fim = tempo;
                    else tr_10_90_f_encontrado = true;

                    // Overshoot
                    if ((nivel >= setpoint) && (mp_encontrado == false)){
                        if (nivel >= mp){
                            mp = nivel;
                            tp = tempo;
                        }
                    }
                    if ((nivel <= setpoint) && (mp_encontrado == false) && (mp != 0.0) && (tp != 0.0))
                        mp_encontrado = true;
                    
                    
                    // Tempo de acomodação
                    // ts (2%)
                    /*if (nivel < (setpoint - (setpoint * 2)/100)) pico_inferior_limite_2 = true;
                    if (nivel > (setpoint + (setpoint * 2)/100)) pico_superior_limite_2 = true;
                    if (((pico_inferior_limite_2 == true) || (pico_superior_limite_2 == true)) && (ts_2 != 0.0)){
                            pico_inferior_limite_2 = false;
                            pico_inferior_limite_2 = false;
                    }
                    else{
                            ts_2 = tempo;
                            pico_inferior_limite_2 = true;
                            pico_inferior_limite_2 = true;
                    }*/
                    if ((nivel < (setpoint-((setpoint - guardarSetpoint)*2/100)) || (nivel > (setpoint+((setpoint - guardarSetpoint)*2)/100)))) ts_2 = tempo;

                    // ts (5%)
                    /*if (nivel < (setpoint - (setpoint * 5)/100)) pico_inferior_limite_5 = true;
                    if (nivel > (setpoint + (setpoint * 5)/100)) pico_superior_limite_5 = true;
                    if (((pico_inferior_limite_5 == true) || (pico_superior_limite_5 == true)) && (ts_5 != 0.0)){
                            pico_inferior_limite_5 = false;
                            pico_inferior_limite_5 = false;
                    }
                    else{
                            ts_5 = tempo;
                            pico_inferior_limite_5 = true;
                            pico_inferior_limite_5 = true;
                    }*/
                    if ((nivel < (setpoint-((setpoint - guardarSetpoint)*5/100)) || (nivel > (setpoint+((setpoint - guardarSetpoint)*5)/100)))) ts_5 = tempo;


                    // ts (7%)
                    /*if (nivel < (setpoint - (setpoint * 7)/100)) pico_inferior_limite_7 = true;
                    if (nivel > (setpoint + (setpoint * 7)/100)) pico_superior_limite_7 = true;
                    if (((pico_inferior_limite_7 == true) || (pico_superior_limite_7 == true)) && (ts_7 != 0.0)){
                            pico_inferior_limite_7 = false;
                            pico_inferior_limite_7 = false;
                    }
                    else{
                            ts_7 = tempo;
                            pico_inferior_limite_7 = true;
                            pico_inferior_limite_7 = true;
                    }*/
                    if ((nivel < (setpoint-((setpoint - guardarSetpoint)*7/100)) || (nivel > (setpoint+((setpoint - guardarSetpoint)*7)/100)))) ts_7 = tempo;


                    // ts (10%)
                    /*if (nivel < (setpoint - (setpoint * 10)/100)) pico_inferior_limite_10 = true;
                    if (nivel > (setpoint + (setpoint * 10)/100)) pico_superior_limite_10 = true;
                    if (((pico_inferior_limite_10 == true) || (pico_superior_limite_10 == true)) && (ts_10 != 0.0)){
                            pico_inferior_limite_10 = false;
                            pico_inferior_limite_10 = false;
                    }
                    else{
                            ts_10 = tempo;
                            pico_inferior_limite_10 = true;
                            pico_inferior_limite_10 = true;
                    }*/
                    if ((nivel < (setpoint -((setpoint - guardarSetpoint)*10/100)) || (nivel > (setpoint +((setpoint - guardarSetpoint)*10)/100)))) ts_10 = tempo;

                    // ess
                    if ((ts_10 - ts_anterior) < 0.1){
                        if (contador < numberPontos){ // Enquanto tem espaço no vetor
                            erroRegime[contador] = nivel;
                            contador++;
                        }
                        else{
                            double soma = 0.0;
                            for (int i = 0; i < numberPontos; i++){
                                soma = soma + erroRegime[i];
                            } 
                            mediaErro = soma/numberPontos;
                            contador = 0;
                        }
                    }
                        
		}
		else{
                    // Nível antes do degrau é maior que o setpoint
                    // Tempo de subida
                    if ((nivel >= setpoint) && (tr_0_100_f_encontrado == false)) tr_0_100_fim = tempo;
                    else tr_0_100_f_encontrado = true;
                    if ((nivel >= guardarSetpoint - ((guardarSetpoint - setpoint) * 5)/100) && (tr_5_95_i_encontrado == false)) tr_5_95_inicio = tempo;
                    else tr_5_95_i_encontrado = true;
                    if ((nivel >= guardarSetpoint - ((guardarSetpoint - setpoint) * 95)/100) && (tr_5_95_f_encontrado == false)) tr_5_95_fim = tempo;
                    else tr_5_95_f_encontrado = true;
                    if ((nivel >= guardarSetpoint - ((guardarSetpoint - setpoint) * 10)/100) && (tr_10_90_i_encontrado == false)) tr_10_90_inicio = tempo;
                    else tr_10_90_i_encontrado = true;
                    if ((nivel >= guardarSetpoint - ((guardarSetpoint - setpoint) * 90)/100) && (tr_10_90_f_encontrado == false)) tr_10_90_fim = tempo;
                    else tr_10_90_f_encontrado = true;

                    // Overshoot
                    if ((nivel <= setpoint) && (mp_encontrado == false)){
                        if (nivel <= mp){
                                mp = nivel;
                                tp = tempo;
                        }
                        else mp_encontrado = true;
                    }
                    
                    // Tempo de acomodação
                    // ts (2%)
                    /*if (nivel < (setpoint - (setpoint * 2)/100)) pico_inferior_limite_2 = true;
                    if (nivel > (setpoint + (setpoint * 2)/100)) pico_superior_limite_2 = true;
                    if (((pico_inferior_limite_2 == true) || (pico_superior_limite_2 == true)) && (ts_2 != 0.0)){
                            pico_inferior_limite_2 = false;
                            pico_inferior_limite_2 = false;
                    }
                    else{
                            ts_2 = tempo;
                            pico_inferior_limite_2 = true;
                            pico_inferior_limite_2 = true;
                    }*/
                    if ((nivel < (setpoint-((guardarSetpoint - setpoint)*2/100)) || (nivel > (setpoint+((guardarSetpoint  - setpoint)*2)/100)))) ts_2 = tempo;

                    // ts (5%)
                    /*if (nivel < (setpoint - (setpoint * 5)/100)) pico_inferior_limite_5 = true;
                    if (nivel > (setpoint + (setpoint * 5)/100)) pico_superior_limite_5 = true;
                    if (((pico_inferior_limite_5 == true) || (pico_superior_limite_5 == true)) && (ts_5 != 0.0)){
                            pico_inferior_limite_5 = false;
                            pico_inferior_limite_5 = false;
                    }
                    else{
                            ts_5 = tempo;
                            pico_inferior_limite_5 = true;
                            pico_inferior_limite_5 = true;
                    }*/
                    if ((nivel < (setpoint-((guardarSetpoint - setpoint)*5/100)) || (nivel > (setpoint+((guardarSetpoint - setpoint)*5)/100)))) ts_5 = tempo;


                    // ts (7%)
                    /*if (nivel < (setpoint - (setpoint * 7)/100)) pico_inferior_limite_7 = true;
                    if (nivel > (setpoint + (setpoint * 7)/100)) pico_superior_limite_7 = true;
                    if (((pico_inferior_limite_7 == true) || (pico_superior_limite_7 == true)) && (ts_7 != 0.0)){
                            pico_inferior_limite_7 = false;
                            pico_inferior_limite_7 = false;
                    }
                    else{
                            ts_7 = tempo;
                            pico_inferior_limite_7 = true;
                            pico_inferior_limite_7 = true;
                    }*/
                    if ((nivel < (setpoint -((guardarSetpoint - setpoint)*7/100)) || (nivel > (setpoint+((guardarSetpoint - setpoint)*7)/100)))) ts_7 = tempo;


                    // ts (10%)
                    /*if (nivel < (setpoint - (setpoint * 10)/100)) pico_inferior_limite_10 = true;
                    if (nivel > (setpoint + (setpoint * 10)/100)) pico_superior_limite_10 = true;
                    if (((pico_inferior_limite_10 == true) || (pico_superior_limite_10 == true)) && (ts_10 != 0.0)){
                            pico_inferior_limite_10 = false;
                            pico_inferior_limite_10 = false;
                    }
                    else{
                            ts_10 = tempo;
                            pico_inferior_limite_10 = true;
                            pico_inferior_limite_10 = true;
                    }*/
                    if ((nivel < (setpoint -((guardarSetpoint - setpoint)*10/100)) || (nivel > (setpoint +((guardarSetpoint - setpoint)*10)/100)))) ts_10 = tempo;
		
                    // ess
                    if ((ts_10 - ts_anterior) < 0.1){
                        if (contador < numberPontos){ // Enquanto tem espaço no vetor
                            erroRegime[contador] = nivel;
                            contador++;
                        }
                        else{
                            double soma = 0.0;
                            for (int i = 0; i < numberPontos; i++){
                                soma = soma + erroRegime[i];
                            } 
                            mediaErro = soma/numberPontos;
                            contador = 0;
                        }
                    }
                }
                ts_anterior = ts_10;
                //System.out.println("Erro absoluto -> " + String.valueOf(this.getEssAbs()));
                //System.out.println("Erro Percentual -> " + String.valueOf(this.getEssPerc()));
	}

	// Método para pegar tr de 0 - 100%
	public double getTr_0_100(){
		return (tr_0_100_fim - tr_0_100_inicio);
	}
	// Método para pegar tr de 5 - 95%
	public double getTr_5_95(){
		return (tr_5_95_fim - tr_5_95_inicio);
	}
	// Método para pegar tr de 10 - 90%
	public double getTr_10_90(){
		return (tr_10_90_fim - tr_10_90_inicio);
	}
	// Método para pegar mp absoluto
	public double getMpAbs(){
                if (mp < 80.0){
                    if ((mp != 0.0) && (mp < setPoint)) return (setPoint - mp);
                    else{
                            if ((mp != 0.0) && (mp >= setPoint)) return (mp - setPoint);
                            else return 0.0;
                    }
                }
                else return 0.0;
	}
	// Método para pegar mp percentual
	public double getMpPerc(){
            //return ((getMpAbs() * 100)/setPoint);
            return ((getMpAbs() * 100)/Math.abs(setPoint - guardarSetpoint));
	}
	// Método para pegar tp
	public double getTp(){
            //return (tp - tpInicial);
            if ((tp - tpInicial) < 0.0) return 0.0;
            else return (tp - tpInicial);
	}
	// Método para pegar ts de 2%
	public double getTs_2(){
            //return ts_2;
            if ((ts_2 - ts_2_inicial) < 0.0) return 0.0;
            else return (ts_2 - ts_2_inicial);
	}	
	// Método para pegar ts de 5%
	public double getTs_5(){
            //return ts_5;
            if ((ts_5 - ts_5_inicial) < 0.0) return 0.0;
            else return (ts_5 - ts_5_inicial);
	}
	// Método para pegar ts de 7%
	public double getTs_7(){
            //return ts_7;
            if ((ts_7 - ts_7_inicial) < 0.0) return 0.0;
            else return (ts_7 - ts_7_inicial);
	}
	// Método para pegar ts de 10%
	public double getTs_10(){
            //return ts_10;
            if ((ts_10 - ts_10_inicial) < 0.0) return 0.0;
            else return (ts_10 - ts_10_inicial);
	}
        // Método para pegar ess absoluto
        public double getEssAbs(){
            if ((ts_10 - ts_anterior) < 0.1){
                return setPoint - mediaErro;
            }
            else return 0.0;
        }
        // Método para pegar ess percentual
        public double getEssPerc(){
            return Math.abs((getEssAbs() * 100)/setPoint);
        }
}