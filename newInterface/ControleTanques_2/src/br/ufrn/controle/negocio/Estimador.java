/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.negocio;

import org.jfree.data.xy.XYSeries;

/**
 *
 * @author Adriana B.
 */
public class Estimador {
    
    private double[][] G = {{0.9935, 0},{0.0065, 0.9935}};
    private double[] H = {0.2869, 0.0001};
    private double[] L = new double[2];
    
    private double X1Est = 0;
    private double X2Est = 0;
    private double YEst = 0;    
    private double Y = 0;
    private double U = 0;
    
    private XYSeries sX1Est = new XYSeries("X1 Estimado");
    private XYSeries sX2Est = new XYSeries("X2 Estimado");
    private XYSeries sYEst = new XYSeries("Y Estimado");
    private XYSeries sY = new XYSeries("Y");
    private XYSeries sU = new XYSeries("U");
    
    
    public double[] calcL(double p1_a, double p1_b, double p2_a, double p2_b) {
        ObservadorEstados obs = new ObservadorEstados(p1_a, p1_b, p2_a, p2_b);
        return obs.getL();
    }
    
    public void updateEstimativas(double p1_a, double p1_b, double p2_a, double p2_b, double y, double u) {
        L = this.calcL(p1_a,p1_b,p2_a,p2_b);
        X1Est = G[0][0]*X1Est + G[0][1]*X2Est + L[0]*(Y-YEst) + H[0]*U;
        X2Est = G[1][0]*X1Est + G[1][1]*X2Est + L[1]*(Y-YEst) + H[1]*U;
        YEst = X2Est;
        Y = y;
        U = u;
    }
    
    
    public double getX1Est() {
        return X1Est;
    }
    
    public double getX2Est() {
        return X2Est;
    }
    
    public double getYEst() {
        return YEst;
    }
    
    
    public void addSinais(double time, double x1Est, double x2Est, double yEst, double y, double u) {
        sX1Est.add(time,x1Est);
        sX2Est.add(time,x2Est);
        sYEst.add(time,yEst);
        sY.add(time,y);
        sU.add(time,u);
    }
    
    
    public XYSeries getSinalX1Est() {
        return sX1Est;
    }
    
    public XYSeries getSinalX2Est() {
        return sX2Est;
    }
    
    public XYSeries getSinalYEst() {
        return sYEst;
    }
    
    public XYSeries getSinalY() {
        return sY;
    }
    
    public XYSeries getSinalU() {
        return sU;
    }
}
