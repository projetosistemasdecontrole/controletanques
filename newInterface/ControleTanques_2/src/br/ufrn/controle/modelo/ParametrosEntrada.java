/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.modelo;

/**
 *
 * @author Adriana
 */

public class ParametrosEntrada {
    public Malha malha;
    public Funcao funcao;
    
    public double amplitude;
    public double offset;
    public double periodo;
    
    public double periodoMax;
    public double periodoMin;
    public double amplitudeMax;
    public double amplitudeMin;
    
    public int canalPv;
    public int canalEscrita;
    public boolean canal0;
    public boolean canal1;
    
    public Controles controle;
    public double kp;
    public double kd;
    public double ki;
    public Controles controleE;
    public double kpE;
    public double kdE;
    public double kiE;
    
    public boolean filtroDerivativo;
    public boolean filtroCondicional;
    public boolean filtroAntiWindup;
    public boolean filtroIntegrativo;
    
    public boolean controleCascata = false;
    
    public boolean obsEstados = false;
    public double polo1_a = 0;
    public double polo1_b = 0;
    public double polo2_a = 0;
    public double polo2_b = 0;
    
    
    public double getConstFilterDerivative(){
        /*String auxConst = txtFiltroDerivativo.getText();
        double constFilterDerivative = Double.parseDouble(auxConst);
        
        return constFilterDerivative;
        */
        return 0.0;
    }
    
    public double getConstAntiWindup(){
        /*String auxConst = txtAntiWindUp.getText();
        double constAntiWindup = Double.parseDouble(auxConst);
        
        return constAntiWindup;
        */
        return 0.0;
    }
}
