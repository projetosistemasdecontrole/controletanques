/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.modelo;

/**
 *
 * @author Adriana
 */
public enum Malha {
    Aberta(0),
    Fechada(1);
    
    private int id;
    
    Malha(int id) {
       this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
}
