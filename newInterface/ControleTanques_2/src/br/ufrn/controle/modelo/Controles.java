/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.modelo;

/**
 *
 * @author Adriana
 */
public enum Controles {
    P(0,"P"),
    PD(1,"PD"),
    PI(2,"PI"),
    PID(3,"PID"),
    PI_D(4,"PI-D");
    
    private int id;
    private String name;
    
    Controles(int id, String name) {
       this.id = id;
       this.name = name;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
}
