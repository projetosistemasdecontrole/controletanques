package br.ufrn.controle.interfaceCliente;

import br.ufrn.controle.clienteControleTanque.ClienteControleTanque;
import br.ufrn.controle.modelo.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.concurrent.ScheduledExecutorService;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class TelaCliente extends javax.swing.JPanel {

    private ClienteControleTanque clienteControle;
    private Grafico telaGrafico;
    ScheduledExecutorService exec;
    private boolean parado = true;
    
    private Malha malha;
    private Funcao funcao;
    
    private double amplitude;
    private double offset;
    private double periodo;
    
    private double periodoMax;
    private double periodoMin;
    private double amplitudeMax;
    private double amplitudeMin;
    
    private int canalPv;
    private int canalEscrita;
    private int valorFiltro;
    private boolean lerCanal0;
    private boolean lerCanal1;
    
    private Controles controle;
    private double kp;
    private double kd;
    private double ki;
    private double td;
    private double ti;
    
    private boolean filtroDerivativo;
    private boolean filtroCondicional;
    private boolean filtroAntiWindup;
    private boolean semFiltro;


    public static void main(String args[]) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().add(new TelaCliente());
        frame.pack();
        frame.setVisible(true);
    }

    public TelaCliente() {
        initComponents();
        
        addWindowListener(  
            new WindowAdapter()   
            {
                public void windowClosing(WindowEvent we) {
                    if (!parado)
                        clique_botao_iniciar();
                }
            }
        );
        
        btnAtualizar.setVisible(false);
        
        txtSetPoint.setVisible(false);
        lblSetPoint.setVisible(false);
        txtInfo1.setVisible(false);
        txtInfo2.setVisible(false);
        
        radioAntiWindup.setVisible(false);
        chkFiltroDerivativo.setVisible(false);
        radioFiltroCondicional.setVisible(false);
        lblFiltroDerivativo.setVisible(false);
        txtFiltroDerivativo.setVisible(false);
        lblAntiWindUp.setVisible(false);
        txtAntiWindUp.setVisible(false);
        chkSemFiltroIntegrativo.setVisible(false);
        radioFiltroCondicional.setSelected(true);
        
        lblAmplitudeMax.setVisible(false);
        txtAmplitudeMax.setVisible(false);
        lblAmplitudeMin.setVisible(false);
        txtAmplitudeMin.setVisible(false);
        
        lblPeriodoMax.setVisible(false);
        txtPeriodoMax.setVisible(false);
        lblPeriodoMin.setVisible(false);
        txtPeriodoMin.setVisible(false);
        
        jtextkp.setEnabled(false);
        jtextkd.setEnabled(false);
        jtextki.setEnabled(false);
        jtexttd.setEnabled(false);
        jtextti.setEnabled(false);
        
        btnDegrau.setEnabled(false);
        btnSenoidal.setEnabled(false);
        btnQuadratico.setEnabled(false);
        btnDenteDeSerra.setEnabled(false);
        btnAleatorio.setEnabled(false);
        
        btnMalhaFechada.setEnabled(false);
        btnMalhaAberta.setEnabled(false);
        
        txtAmplitude.setEnabled(false);
        txtOffset.setEnabled(false);
        txtSetPoint.setEnabled(false);
        txtAmplitudeMax.setEnabled(false);
        txtAmplitudeMin.setEnabled(false);
        
        txtPeriodo.setEnabled(false);
        txtPeriodoMin.setEnabled(false);
        txtPeriodoMax.setEnabled(false);
        
        comboBoxPV.setEnabled(false);
        jComboBox1.setEnabled(false);
        
        comboBoxPV.setVisible(false);
        lblPV.setVisible(false);
        
        txtIP.setEnabled(false);
        txtPorta.setEnabled(false);
    }
    
    private void atualizarCanaisPV() {
        String s = "";
        
        if (comboBoxPV.getItemCount() > 0)
            s = comboBoxPV.getSelectedItem().toString();

        comboBoxPV.removeAllItems();
        if (leitura00.isSelected())
            comboBoxPV.addItem(leitura00.getText());
        if (leitura01.isSelected())
            comboBoxPV.addItem(leitura01.getText());
        if (leitura02.isSelected())
            comboBoxPV.addItem(leitura02.getText());
        if (leitura03.isSelected())
            comboBoxPV.addItem(leitura03.getText());
        if (leitura04.isSelected())
            comboBoxPV.addItem(leitura04.getText());
        if (leitura05.isSelected())
            comboBoxPV.addItem(leitura05.getText());
        if (leitura06.isSelected())
            comboBoxPV.addItem(leitura06.getText());
        if (leitura07.isSelected())
            comboBoxPV.addItem(leitura07.getText());
        
        if (!s.equals("")) {
            for(int i = 0; i < comboBoxPV.getItemCount(); i++) {
                if (comboBoxPV.getItemAt(i).toString().equals(s))
                    comboBoxPV.setSelectedItem(s);
            }
        }
    }


    public boolean lerCanal0() {
        return lerCanal0;
    }
    
    public boolean lerCanal1() {
        return lerCanal1;
    }
    
    public Funcao getFuncao() {
        return funcao;
    }
    
    public Malha getMalha() {
        return malha;
    }
    
    public double getAmplitude() {
        return amplitude;
    }
    
    public double getOffset() {
        return offset;
    }
    
    public double getPeriodo() {
        return periodo;
    }
    
    public double getPeriodoMinimo() {
        return periodoMin;
    }
    
    public double getPeriodoMaximo() {
        return periodoMax;
    }
    
    public double getAmplitudeMinima() {
        return amplitudeMax;
    }
    
    public double getAmplitudeMaxima() {
        return amplitudeMin;
    }
    
    public int getCanalEscrita() {
        return canalEscrita;
    }
        
    public double getkp() {
        if (!jtextkp.getText().isEmpty()){
            String sKp = jtextkp.getText();
            double kp = Double.parseDouble(sKp);
            return kp;
        }
        else return 0.0;
    }
    
    public double getkd() {
        if (!jtextkd.getText().isEmpty()){
            String sKd = jtextkd.getText();
            double kd = Double.parseDouble(sKd);
            return kd;
        }
        else return 0.0;
    }
    
     public double gettd() {
        if (!jtexttd.getText().isEmpty()){
            String sTd = jtexttd.getText();
            double td = Double.parseDouble(sTd);
            return td;
        }
        else return 0.0;
    }
    
    public double getki() {
        if (!jtextki.getText().isEmpty()){
            String sKi = jtextki.getText();
            double ki = Double.parseDouble(sKi);
            return ki;
        }
        else return 0.0;
    }
    
    public double getti() {
        if (!jtextti.getText().isEmpty()){
            String sTi = jtextti.getText();
            double ti = Double.parseDouble(sTi);
            return ti;
        }
        else return 0.0;
    }
    
    public Controles getControle() {
        return controle;
    }
    
    public int getCanalPV() {
        return canalPv;
    }
    
    public boolean parado() {
        return parado;
    }
    
    public String getIp() {
        return txtIP.getText();
    }
    
    public String getPorta() {
        return txtPorta.getText();
    }
    
    // Filtros
    public boolean verificarFiltroCondicional(){
        return filtroCondicional;
    }

    public boolean verificarAntiWindup(){
        return filtroAntiWindup;
    }
    
    public boolean verificarIntegrativos(){
        return semFiltro;
    }
        
    public boolean filtroDerivativo() {
        return filtroDerivativo;
    }
    
    public double getConstFilterDerivative(){
        String auxConst = txtFiltroDerivativo.getText();
        double constFilterDerivative = Double.parseDouble(auxConst);
        
        return constFilterDerivative;
    }
    
    public double getConstAntiWindup(){
        String auxConst = txtAntiWindUp.getText();
        double constAntiWindup = Double.parseDouble(auxConst);
        
        return constAntiWindup;
    }
    
    public void clique_botao_iniciar() {
        if (parado) {
            parado = false;
            btnEnviar.setText("Parar");
            btnAtualizar.setVisible(true);
            
            clienteControle = new ClienteControleTanque(this);
            clienteControle.iniciar(this.getIp(), this.getPorta());

            if (telaGrafico != null)
                telaGrafico.dispose();
            
            telaGrafico = new Grafico(clienteControle);
            telaGrafico.setVisible(true);
            telaGrafico.criar();
        }
        else
        {
            parado = true;
            btnEnviar.setText("Iniciar");
            btnAtualizar.setVisible(false);
        }
    }
    
    
    public boolean aplicarParametros() {
        try {       
            //Malha
            if (btnMalhaAberta.isSelected())
                malha = Malha.Aberta;
            else if (btnMalhaFechada.isSelected())
                malha = Malha.Fechada;

            //Sinal
            if (btnDegrau.isSelected())
                funcao = Funcao.Degrau;
            else if (btnSenoidal.isSelected())
                funcao = Funcao.Senoide;
            else if (btnQuadratico.isSelected())
                funcao = Funcao.Quadrada;
            else if (btnDenteDeSerra.isSelected())
                funcao = Funcao.DenteDeSerra;
            else if (btnAleatorio.isSelected())
                funcao = Funcao.Random;

            //Parâmetros do sinal
            if (!txtAmplitude.getText().isEmpty() && btnMalhaAberta.isSelected())
                amplitude = Double.parseDouble(txtAmplitude.getText());
            else if (!txtSetPoint.getText().isEmpty() && btnMalhaFechada.isSelected())
                amplitude = Double.parseDouble(txtSetPoint.getText());
            else
                amplitude = 0;

            if (!txtPeriodo.getText().isEmpty())
                periodo = Double.parseDouble(txtPeriodo.getText());
            else
                periodo = 0;

            if (!txtOffset.getText().isEmpty())
                offset = Double.parseDouble(txtOffset.getText());
            else
                offset = 0;

            if (!txtPeriodoMin.getText().isEmpty())
                periodoMin = Double.parseDouble(txtPeriodoMin.getText());
            else
                periodoMin = 0;

            if (!txtPeriodoMax.getText().isEmpty())
                periodoMax = Double.parseDouble(txtPeriodoMax.getText());
            else
                periodoMax = 0;

            if (!txtAmplitudeMin.getText().isEmpty())
                amplitudeMin = Double.parseDouble(txtAmplitudeMin.getText());
            else
                amplitudeMin = 0;

            if (!txtAmplitudeMax.getText().isEmpty())
                amplitudeMax = Double.parseDouble(txtAmplitudeMax.getText());
            else
                amplitudeMax = 0;

            //Canais de leitura
            lerCanal0 = leitura00.isSelected();
            lerCanal1 = leitura01.isSelected();

            //Canal de escrita
            if (escrita00.isSelected())
                canalEscrita = 0;
            else if (escrita01.isSelected())
                canalEscrita = 1;
            else if (escrita02.isSelected())
                canalEscrita = 2;
            else if (escrita03.isSelected())
                canalEscrita = 3;
            else if (escrita04.isSelected())
                canalEscrita = 4;
            else if (escrita05.isSelected())
                canalEscrita = 5;
            else if (escrita06.isSelected())
                canalEscrita = 6;
            else
                canalEscrita = 7;
            
            //PV
            switch(comboBoxPV.getSelectedItem().toString()) {
                case "Canal 01":
                    canalPv = 1;
                    break;
                case "Canal 02":
                    canalPv = 2;
                    break;
                case "Canal 03":
                    canalPv = 3;
                    break;
                case "Canal 04":
                    canalPv = 4;
                    break;
                case "Canal 05":
                    canalPv = 5;
                    break;
                case "Canal 06":
                    canalPv = 6;
                    break;
                case "Canal 07":
                    canalPv = 7;
                    break;
                default:
                    canalPv = 0;
                    break;
            }

            //Parâmetros do controlador
            if (!jtextkp.getText().isEmpty())
                kp = Double.parseDouble(jtextkp.getText());
            else
                kp = 0;

            if (!jtextkd.getText().isEmpty())
                kd = Double.parseDouble(jtextkd.getText());
            else
                kd = 0;

            if (!jtexttd.getText().isEmpty())
                td = Double.parseDouble(jtexttd.getText());
            else
                td = 0;

            if (!jtextki.getText().isEmpty())
                ki = Double.parseDouble(jtextki.getText());
            else
                ki = 0;

            if (!jtextti.getText().isEmpty())
                ti = Double.parseDouble(jtextti.getText());
            else
                ti = 0;

            //Tipo de controlador
            if (jComboBox1.getSelectedItem().toString().equals(Controles.P.getName()))
                controle = Controles.P;
            else if (jComboBox1.getSelectedItem().toString().equals(Controles.PD.getName()))
                controle = Controles.PD;
            else if (jComboBox1.getSelectedItem().toString().equals(Controles.PI.getName()))
                controle = Controles.PI;
            else if (jComboBox1.getSelectedItem().toString().equals(Controles.PID.getName()))
                controle = Controles.PID;
            else if (jComboBox1.getSelectedItem().toString().equals(Controles.PI_D.getName()))
                controle = Controles.PI_D;
            
            return true;
        }
        catch(Exception ex) {
            return false;
        }
    }
    
    public void calcularParametrosControlador() {
        
    }

    private void habilitarEntradasControlador() {
        final DecimalFormat decimal = new DecimalFormat("0.000");
         
        if (jComboBox1.getSelectedItem().toString().equals("P")) {
                jtextkp.setEnabled(true);
                jtextkd.setEnabled(false);
                jtextki.setEnabled(false);
                jtexttd.setEnabled(false);
                jtextti.setEnabled(false);
                jtextkd.setText("");
                jtextki.setText("");
                jtexttd.setText("");
                jtextti.setText("");
                
                chkFiltroDerivativo.setSelected(false);
                chkSemFiltroIntegrativo.setVisible(false);
                radioFiltroCondicional.setVisible(false);
                radioAntiWindup.setVisible(false);
                chkFiltroDerivativo.setVisible(false);
                
        }else if (jComboBox1.getSelectedItem().toString().equals("PD")){
                jtextkp.setEnabled(true);
                jtextkd.setEnabled(true);
                jtextki.setEnabled(false);
                jtexttd.setEnabled(true);
                jtextti.setEnabled(false);
                jtextki.setText("");
                jtextti.setText("");
                
                chkFiltroDerivativo.setSelected(false);
                radioFiltroCondicional.setVisible(false);
                radioAntiWindup.setVisible(false);
                chkFiltroDerivativo.setVisible(true);
                
                jtexttd.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double td = gettd();
                        double kp = getkp();
                        double kd = kp * td;
                        jtextkd.setText(String.valueOf(kd));
                    }
                });
                
                jtextkd.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double kd = getkd();
                        double kp = getkp();
                        double td = kd / kp;
                        jtexttd.setText(String.valueOf(td));
                    }
                });
                
        }else if (jComboBox1.getSelectedItem().toString().equals("PI")){
                jtextkp.setEnabled(true);
                jtextkd.setEnabled(false);
                jtextki.setEnabled(true);
                jtexttd.setEnabled(false);
                jtextti.setEnabled(true);
                jtextkd.setText("");
                jtexttd.setText("");
                
                txtFiltroDerivativo.setVisible(false);
                lblFiltroDerivativo.setVisible(false);
                chkFiltroDerivativo.setSelected(false);
                chkSemFiltroIntegrativo.setVisible(true);
                radioFiltroCondicional.setVisible(true);
                radioAntiWindup.setVisible(true);
                chkFiltroDerivativo.setVisible(false);
                
                jtextti.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double ti = getti();
                        double kp = getkp();
                        double ki = kp * ti;
                        jtextki.setText(String.valueOf(ki));
                    }
                });
                
                jtextki.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double ki = getki();
                        double kp = getkp();
                        double ti = ki / kp;
                        jtextti.setText(String.valueOf(ti));
                    }
                });
                
        }else if (jComboBox1.getSelectedItem().toString().equals("PID")){
                jtextkp.setEnabled(true);
                jtextkd.setEnabled(true);
                jtextki.setEnabled(true);
                jtexttd.setEnabled(true);
                jtextti.setEnabled(true);
                
                chkFiltroDerivativo.setSelected(false);
                chkSemFiltroIntegrativo.setVisible(true);
                radioFiltroCondicional.setVisible(true);
                radioAntiWindup.setVisible(true);
                chkFiltroDerivativo.setVisible(true);
                
                jtexttd.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double td = gettd();
                        double kp = getkp();
                        double kd = kp * td;
                        jtextkd.setText(String.valueOf(kd));
                    }
                });
                
                jtextkd.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double kd = getkd();
                        double kp = getkp();
                        double td = kd / kp;
                        jtexttd.setText(String.valueOf(td));
                    }
                });
                
                jtextti.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double ti = getti();
                        double kp = getkp();
                        double ki = kp * ti;
                        jtextki.setText(String.valueOf(ki));
                    }
                });
                
                jtextki.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double ki = getki();
                        double kp = getkp();
                        double ti = ki / kp;
                        jtextti.setText(String.valueOf(ti));
                    }
                });
                
        }else if (jComboBox1.getSelectedItem().toString().equals("PI-D")){
                jtextkp.setEnabled(true);
                jtextkd.setEnabled(true);
                jtextki.setEnabled(true);
                jtexttd.setEnabled(true);
                jtextti.setEnabled(true);
                
                chkFiltroDerivativo.setSelected(false);
                chkSemFiltroIntegrativo.setVisible(true);
                radioFiltroCondicional.setVisible(true);
                radioAntiWindup.setVisible(true);
                chkFiltroDerivativo.setVisible(false);
                txtFiltroDerivativo.setVisible(false);
                lblFiltroDerivativo.setVisible(false);
                
                 jtexttd.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double td = gettd();
                        double kp = getkp();
                        double kd = kp * td;
                        jtextkd.setText(String.valueOf(kd));
                    }
                });
                
                jtextkd.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double kd = getkd();
                        double kp = getkp();
                        double td = kd / kp;
                        jtexttd.setText(String.valueOf(td));
                    }
                });
                
                jtextti.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double ti = getti();
                        double kp = getkp();
                        double ki = kp * ti;
                        jtextki.setText(String.valueOf(ki));
                    }
                });
                
                jtextki.addFocusListener(new FocusListener() {
                    @Override
                    public void focusGained(FocusEvent fe) {
                    }

                    @Override
                    public void focusLost(FocusEvent fe) {
                        double ki = getki();
                        double kp = getkp();
                        double ti = ki / kp;
                        jtextti.setText(String.valueOf(ti));
                    }
                });
        }
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoPortasEscrita = new javax.swing.ButtonGroup();
        grupoSinais = new javax.swing.ButtonGroup();
        grupoMalhas = new javax.swing.ButtonGroup();
        jTextField3 = new javax.swing.JTextField();
        grupoControlador = new javax.swing.ButtonGroup();
        panelPortas = new javax.swing.JPanel();
        leitura00 = new javax.swing.JCheckBox();
        leitura02 = new javax.swing.JCheckBox();
        leitura03 = new javax.swing.JCheckBox();
        leitura01 = new javax.swing.JCheckBox();
        leitura05 = new javax.swing.JCheckBox();
        leitura04 = new javax.swing.JCheckBox();
        leitura06 = new javax.swing.JCheckBox();
        escrita06 = new javax.swing.JRadioButton();
        escrita05 = new javax.swing.JRadioButton();
        escrita04 = new javax.swing.JRadioButton();
        escrita03 = new javax.swing.JRadioButton();
        escrita02 = new javax.swing.JRadioButton();
        escrita01 = new javax.swing.JRadioButton();
        escrita00 = new javax.swing.JRadioButton();
        labelLeitura = new javax.swing.JLabel();
        labelEscrita = new javax.swing.JLabel();
        escrita07 = new javax.swing.JRadioButton();
        leitura07 = new javax.swing.JCheckBox();
        labelTitulo = new javax.swing.JLabel();
        btnEnviar = new javax.swing.JButton();
        panelSinal = new javax.swing.JPanel();
        btnDegrau = new javax.swing.JToggleButton();
        btnSenoidal = new javax.swing.JToggleButton();
        btnQuadratico = new javax.swing.JToggleButton();
        btnDenteDeSerra = new javax.swing.JToggleButton();
        btnAleatorio = new javax.swing.JToggleButton();
        panelDados = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblAmplitude = new javax.swing.JLabel();
        txtAmplitude = new javax.swing.JTextField();
        lblOffset = new javax.swing.JLabel();
        txtOffset = new javax.swing.JTextField();
        txtSetPoint = new javax.swing.JTextField();
        lblSetPoint = new javax.swing.JLabel();
        lblAmplitudeMax = new javax.swing.JLabel();
        txtAmplitudeMax = new javax.swing.JTextField();
        txtInfo1 = new javax.swing.JLabel();
        lblAmplitudeMin = new javax.swing.JLabel();
        txtAmplitudeMin = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        lblPeriodo = new javax.swing.JLabel();
        txtPeriodo = new javax.swing.JTextField();
        lblPeriodoMin = new javax.swing.JLabel();
        txtPeriodoMin = new javax.swing.JTextField();
        txtInfo2 = new javax.swing.JLabel();
        lblPeriodoMax = new javax.swing.JLabel();
        txtPeriodoMax = new javax.swing.JTextField();
        panelPV = new javax.swing.JPanel();
        lblPV = new javax.swing.JLabel();
        comboBoxPV = new javax.swing.JComboBox();
        btnAtualizar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblIP = new javax.swing.JLabel();
        txtIP = new javax.swing.JTextField();
        lblPorta = new javax.swing.JLabel();
        txtPorta = new javax.swing.JTextField();
        btnSimulador = new javax.swing.JButton();
        btnPlanta = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jtextkp = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jtextkd = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jtexttd = new javax.swing.JTextField();
        jtextti = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jtextki = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        chkFiltroDerivativo = new javax.swing.JCheckBox();
        lblAntiWindUp = new javax.swing.JLabel();
        lblFiltroDerivativo = new javax.swing.JLabel();
        txtAntiWindUp = new javax.swing.JTextField();
        txtFiltroDerivativo = new javax.swing.JTextField();
        radioFiltroCondicional = new javax.swing.JRadioButton();
        radioAntiWindup = new javax.swing.JRadioButton();
        chkSemFiltroIntegrativo = new javax.swing.JCheckBox();
        panelMalha = new javax.swing.JPanel();
        btnMalhaAberta = new javax.swing.JToggleButton();
        btnMalhaFechada = new javax.swing.JToggleButton();

        jTextField3.setText("jTextField3");

        panelPortas.setBorder(javax.swing.BorderFactory.createTitledBorder("Portas"));
        panelPortas.setToolTipText("");

        leitura00.setText("Canal 00");
        leitura00.setToolTipText("");
        leitura00.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura00ActionPerformed(evt);
            }
        });

        leitura02.setText("Canal 02");
        leitura02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura02ActionPerformed(evt);
            }
        });

        leitura03.setText("Canal 03");
        leitura03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura03ActionPerformed(evt);
            }
        });

        leitura01.setText("Canal 01");
        leitura01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura01ActionPerformed(evt);
            }
        });

        leitura05.setText("Canal 05");
        leitura05.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura05ActionPerformed(evt);
            }
        });

        leitura04.setText("Canal 04");
        leitura04.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura04ActionPerformed(evt);
            }
        });

        leitura06.setText("Canal 06");
        leitura06.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura06ActionPerformed(evt);
            }
        });

        grupoPortasEscrita.add(escrita06);
        escrita06.setText("Canal 06");
        escrita06.setEnabled(false);

        grupoPortasEscrita.add(escrita05);
        escrita05.setText("Canal 05");
        escrita05.setToolTipText("");
        escrita05.setEnabled(false);

        grupoPortasEscrita.add(escrita04);
        escrita04.setText("Canal 04");
        escrita04.setEnabled(false);

        grupoPortasEscrita.add(escrita03);
        escrita03.setText("Canal 03");
        escrita03.setEnabled(false);

        grupoPortasEscrita.add(escrita02);
        escrita02.setText("Canal 02");
        escrita02.setEnabled(false);

        grupoPortasEscrita.add(escrita01);
        escrita01.setText("Canal 01");
        escrita01.setEnabled(false);

        grupoPortasEscrita.add(escrita00);
        escrita00.setSelected(true);
        escrita00.setText("Canal 00");
        escrita00.setEnabled(false);

        labelLeitura.setText("Portas de Leitura");

        labelEscrita.setText("Portas para Escrita");

        grupoPortasEscrita.add(escrita07);
        escrita07.setText("Canal 07");
        escrita07.setEnabled(false);
        escrita07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                escrita07ActionPerformed(evt);
            }
        });

        leitura07.setText("Canal 07");
        leitura07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leitura07ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPortasLayout = new javax.swing.GroupLayout(panelPortas);
        panelPortas.setLayout(panelPortasLayout);
        panelPortasLayout.setHorizontalGroup(
            panelPortasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPortasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPortasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelLeitura)
                    .addGroup(panelPortasLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(panelPortasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPortasLayout.createSequentialGroup()
                                .addComponent(escrita00)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(escrita01)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(escrita02)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(escrita03)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(escrita04)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(escrita05)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(escrita06)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(escrita07))
                            .addGroup(panelPortasLayout.createSequentialGroup()
                                .addComponent(leitura00)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(leitura01)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(leitura02)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(leitura03)
                                .addGap(4, 4, 4)
                                .addComponent(leitura04)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(leitura05)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(leitura06)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(leitura07))))
                    .addComponent(labelEscrita))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelPortasLayout.setVerticalGroup(
            panelPortasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPortasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelLeitura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPortasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPortasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(leitura01)
                        .addComponent(leitura02)
                        .addComponent(leitura03)
                        .addComponent(leitura04)
                        .addComponent(leitura05)
                        .addComponent(leitura06)
                        .addComponent(leitura07))
                    .addComponent(leitura00))
                .addGap(4, 4, 4)
                .addComponent(labelEscrita)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPortasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(escrita00)
                    .addComponent(escrita01)
                    .addComponent(escrita02)
                    .addComponent(escrita03)
                    .addComponent(escrita04)
                    .addComponent(escrita05)
                    .addComponent(escrita06)
                    .addComponent(escrita07))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        labelTitulo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        labelTitulo.setText("Controle de Tanques");

        btnEnviar.setText("Enviar");
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        panelSinal.setBorder(javax.swing.BorderFactory.createTitledBorder("Sinal"));

        grupoSinais.add(btnDegrau);
        btnDegrau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufrn/controle/interfaceCliente/icones/sinalDegrau.png"))); // NOI18N
        btnDegrau.setText("Degrau");
        btnDegrau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDegrauActionPerformed(evt);
            }
        });

        grupoSinais.add(btnSenoidal);
        btnSenoidal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufrn/controle/interfaceCliente/icones/sinalSenoidal.png"))); // NOI18N
        btnSenoidal.setText("Senoidal");
        btnSenoidal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSenoidalActionPerformed(evt);
            }
        });

        grupoSinais.add(btnQuadratico);
        btnQuadratico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufrn/controle/interfaceCliente/icones/sinalQuadrado.png"))); // NOI18N
        btnQuadratico.setText("Quadrático");
        btnQuadratico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuadraticoActionPerformed(evt);
            }
        });

        grupoSinais.add(btnDenteDeSerra);
        btnDenteDeSerra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufrn/controle/interfaceCliente/icones/sinalSerra.png"))); // NOI18N
        btnDenteDeSerra.setText("Dente de Serra");
        btnDenteDeSerra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDenteDeSerraActionPerformed(evt);
            }
        });

        grupoSinais.add(btnAleatorio);
        btnAleatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/ufrn/controle/interfaceCliente/icones/sinalRand.png"))); // NOI18N
        btnAleatorio.setText("Aleatório");
        btnAleatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAleatorioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelSinalLayout = new javax.swing.GroupLayout(panelSinal);
        panelSinal.setLayout(panelSinalLayout);
        panelSinalLayout.setHorizontalGroup(
            panelSinalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSinalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnDegrau)
                .addGap(10, 10, 10)
                .addComponent(btnSenoidal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnQuadratico)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDenteDeSerra)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAleatorio)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelSinalLayout.setVerticalGroup(
            panelSinalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelSinalLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(panelSinalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDegrau)
                    .addComponent(btnSenoidal)
                    .addComponent(btnQuadratico)
                    .addComponent(btnDenteDeSerra)
                    .addComponent(btnAleatorio)))
        );

        panelDados.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados"));

        lblAmplitude.setLabelFor(txtAmplitude);
        lblAmplitude.setText("Amplitude");

        lblOffset.setLabelFor(txtOffset);
        lblOffset.setText("Offset");

        lblSetPoint.setText("SetPoint");

        lblAmplitudeMax.setText("Amplitude Máx.");

        txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
        txtInfo1.setToolTipText("");
        txtInfo1.setEnabled(false);

        lblAmplitudeMin.setText("Amplitude Mín.");

        lblPeriodo.setLabelFor(txtPeriodo);
        lblPeriodo.setText("Período");

        lblPeriodoMin.setText("Período Mín.");

        txtInfo2.setText("Por favor, utilizar valores em segundos");
        txtInfo2.setToolTipText("");
        txtInfo2.setEnabled(false);

        lblPeriodoMax.setText("Período Máx.");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(lblPeriodo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPeriodoMax)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPeriodoMax, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblPeriodoMin)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPeriodoMin, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(txtInfo2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPeriodoMin)
                            .addComponent(txtPeriodoMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPeriodoMax)
                            .addComponent(txtPeriodoMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPeriodo)
                            .addComponent(txtPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txtInfo2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAmplitude)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAmplitude, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblOffset)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtOffset, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAmplitudeMax)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAmplitudeMax, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAmplitudeMin)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAmplitudeMin, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSetPoint)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSetPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtInfo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblAmplitudeMax)
                        .addComponent(txtAmplitudeMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblAmplitude)
                        .addComponent(txtAmplitude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblOffset)
                        .addComponent(txtOffset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblAmplitudeMin)
                            .addComponent(txtAmplitudeMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtInfo1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSetPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSetPoint))))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        panelPV.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblPV.setText("PV");

        comboBoxPV.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Canal 00", "Canal 01", "Canal 02", "Canal 03", "Canal 04", "Canal 05", "Canal 06", "Canal 07" }));
        comboBoxPV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxPVActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelPVLayout = new javax.swing.GroupLayout(panelPV);
        panelPV.setLayout(panelPVLayout);
        panelPVLayout.setHorizontalGroup(
            panelPVLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPVLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPV)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboBoxPV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelPVLayout.setVerticalGroup(
            panelPVLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPVLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblPV)
                .addComponent(comboBoxPV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout panelDadosLayout = new javax.swing.GroupLayout(panelDados);
        panelDados.setLayout(panelDadosLayout);
        panelDadosLayout.setHorizontalGroup(
            panelDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(107, 107, 107)
                .addComponent(panelPV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelDadosLayout.setVerticalGroup(
            panelDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDadosLayout.createSequentialGroup()
                .addGroup(panelDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelPV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnAtualizar.setText("Atualizar");
        btnAtualizar.setToolTipText("");
        btnAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblIP.setText("IP");

        txtIP.setText("127.0.0.1");

        lblPorta.setText("Porta");

        txtPorta.setText("20081");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblIP)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtIP, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(lblPorta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIP)
                    .addComponent(lblPorta)
                    .addComponent(txtIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSimulador.setText("Simulador");
        btnSimulador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimuladorActionPerformed(evt);
            }
        });

        btnPlanta.setText("Planta");
        btnPlanta.setToolTipText("");
        btnPlanta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlantaActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Controlador"));
        jPanel3.setName(""); // NOI18N

        jLabel2.setText("Kp:");

        jLabel3.setText("Kd:");

        jLabel4.setText("Td:");

        jLabel6.setText("Ti:");
        jLabel6.setPreferredSize(new java.awt.Dimension(16, 14));

        jLabel5.setText("Ki:");
        jLabel5.setPreferredSize(new java.awt.Dimension(16, 14));

        jLabel1.setText("Tipo de Controlador");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "P", "PD", "PI", "PID", "PI-D" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        chkFiltroDerivativo.setText("Filtro Derivativo");
        chkFiltroDerivativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFiltroDerivativoActionPerformed(evt);
            }
        });

        lblAntiWindUp.setText("Coeficiente de Tempo do Filtro Anti-Windup");

        lblFiltroDerivativo.setText("Coeficiente de Tempo do Filtro Derivativo");

        grupoControlador.add(radioFiltroCondicional);
        radioFiltroCondicional.setText("Filtro Condicional");
        radioFiltroCondicional.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioFiltroCondicionalActionPerformed(evt);
            }
        });

        grupoControlador.add(radioAntiWindup);
        radioAntiWindup.setText("Anti-Windup");
        radioAntiWindup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAntiWindupActionPerformed(evt);
            }
        });

        chkSemFiltroIntegrativo.setText("Sem Filtro Integrativo");
        chkSemFiltroIntegrativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkSemFiltroIntegrativoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkSemFiltroIntegrativo)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(chkFiltroDerivativo)
                            .addComponent(radioFiltroCondicional)
                            .addComponent(radioAntiWindup))
                        .addGap(32, 32, 32)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(jtextkp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(114, 114, 114)
                                        .addComponent(lblAntiWindUp))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(jtextki, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(jtextkd, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel4)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jtextti, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(jtexttd, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(lblFiltroDerivativo))))))
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtAntiWindUp, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFiltroDerivativo, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jtextkp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAntiWindUp)
                    .addComponent(txtAntiWindUp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblFiltroDerivativo)
                            .addComponent(txtFiltroDerivativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(radioFiltroCondicional)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioAntiWindup)
                                .addGap(3, 3, 3)
                                .addComponent(chkFiltroDerivativo))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jtextki, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jtextti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jtextkd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jtexttd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, Short.MAX_VALUE)
                .addComponent(chkSemFiltroIntegrativo))
        );

        panelMalha.setBorder(javax.swing.BorderFactory.createTitledBorder("Malha"));

        grupoMalhas.add(btnMalhaAberta);
        btnMalhaAberta.setSelected(true);
        btnMalhaAberta.setText("Malha Aberta");
        btnMalhaAberta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMalhaAbertaActionPerformed(evt);
            }
        });

        grupoMalhas.add(btnMalhaFechada);
        btnMalhaFechada.setText("Malha Fechada");
        btnMalhaFechada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMalhaFechadaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMalhaLayout = new javax.swing.GroupLayout(panelMalha);
        panelMalha.setLayout(panelMalhaLayout);
        panelMalhaLayout.setHorizontalGroup(
            panelMalhaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMalhaLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(btnMalhaAberta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMalhaFechada)
                .addContainerGap(58, Short.MAX_VALUE))
        );
        panelMalhaLayout.setVerticalGroup(
            panelMalhaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnMalhaFechada, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
            .addComponent(btnMalhaAberta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAtualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEnviar)
                .addGap(8, 8, 8))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelMalha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelSinal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(labelTitulo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnSimulador)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPlanta)
                                .addGap(111, 111, 111))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addComponent(panelPortas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelDados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSimulador)
                            .addComponent(btnPlanta))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(labelTitulo)))
                .addGap(1, 1, 1)
                .addComponent(panelPortas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelSinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelMalha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelDados, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAtualizar)
                    .addComponent(btnEnviar))
                .addContainerGap())
        );

        panelPortas.getAccessibleContext().setAccessibleName("Portas para Leitura");
        jPanel3.getAccessibleContext().setAccessibleDescription("");
    }// </editor-fold>//GEN-END:initComponents

    private void btnDegrauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDegrauActionPerformed
        txtAmplitude.setEnabled(true);
        txtOffset.setEnabled(false);
        txtSetPoint.setEnabled(false);
        txtAmplitudeMax.setEnabled(false);
        txtAmplitudeMin.setEnabled(false);
        
        txtOffset.setEnabled(true);
        txtSetPoint.setEnabled(true);
        
        
        btnMalhaAberta.setEnabled(true);
        btnMalhaFechada.setEnabled(true);
        
        //Botão Degrau
        lblAmplitude.setVisible(true);
        txtAmplitude.setVisible(true);
        lblOffset.setVisible(false);
        txtOffset.setVisible(false);
        lblPeriodo.setVisible(false);        
        txtPeriodo.setVisible(false);
        
        lblAmplitudeMax.setVisible(false);
        txtAmplitudeMax.setVisible(false);
        lblAmplitudeMin.setVisible(false);
        txtAmplitudeMin.setVisible(false);
        
        lblPeriodoMax.setVisible(false);
        txtPeriodoMax.setVisible(false);
        lblPeriodoMin.setVisible(false);
        txtPeriodoMin.setVisible(false);
        
        if (btnMalhaFechada.isSelected()) {
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(false);
            txtAmplitude.setVisible(false);
            lblAmplitude.setVisible(false);
            txtInfo1.setText("Por favor, utilizar valores entre 0 e 30cm");
        } else if (btnMalhaAberta.isSelected()) {
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(false);
            txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
        }
    }//GEN-LAST:event_btnDegrauActionPerformed

    private void btnMalhaAbertaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMalhaAbertaActionPerformed
       comboBoxPV.setEnabled(false);
       jComboBox1.setEnabled(false);
        
       txtIP.setEnabled(true);
       txtPorta.setEnabled(true);
        
        //Botão Malha Aberta
        jtextkp.setEnabled(false);
        jtextkd.setEnabled(false);
        jtextki.setEnabled(false);
        jtexttd.setEnabled(false);
        jtextti.setEnabled(false);
        jComboBox1.setEnabled(false);
        comboBoxPV.setEnabled(false);
        
        
        lblAmplitude.setVisible(true);
        txtAmplitude.setVisible(true);
        
        if (btnDegrau.isSelected()) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(false);
            txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
        } else if (btnSenoidal.isSelected()) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(false);
            txtInfo1.setText("Por favor, utilizar valores entre 0 e 3V");
        } else if (btnQuadratico.isSelected()) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        } else if (btnDenteDeSerra.isSelected()) {
            txtAmplitude.setVisible(true);
            lblAmplitude.setVisible(true);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        } else if (btnAleatorio.isSelected()) {
            txtAmplitude.setVisible(false);
            lblAmplitude.setVisible(false);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("O valor do período será gerado aleatoriamente");
            txtInfo2.setText("O valor da amplitude será gerada aleatoriamente");
        }
        txtSetPoint.setVisible(false);
        lblSetPoint.setVisible(false);


    }//GEN-LAST:event_btnMalhaAbertaActionPerformed

    private void btnMalhaFechadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMalhaFechadaActionPerformed
           habilitarEntradasControlador();
        
           txtIP.setEnabled(true);
           txtPorta.setEnabled(true);
       
        
            comboBoxPV.setEnabled(true);
            jComboBox1.setEnabled(true);
        
            //Botão Malha Fechada
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtAmplitude.setVisible(false);
            lblAmplitude.setVisible(false);
            txtSetPoint.setVisible(true);
            lblSetPoint.setVisible(true);
        
        if (btnSenoidal.isSelected() || btnQuadratico.isSelected() || btnDenteDeSerra.isSelected()) {
            txtInfo1.setText("Por favor, utilizar valores entre 0 e 30cm");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        } else if (btnAleatorio.isSelected()) {          
            txtInfo1.setText("O valor do período será gerado aleatoriamente");
            txtInfo2.setText("O valor da amplitude será gerada aleatoriamente");
        } else if(btnDegrau.isSelected()){
            txtInfo1.setText("Por favor, utilizar valores entre 0 e 30cm");
            txtInfo2.setVisible(false);
        }

        
        txtSetPoint.setVisible(true);
        lblSetPoint.setVisible(true);
    }//GEN-LAST:event_btnMalhaFechadaActionPerformed

    private void btnSenoidalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSenoidalActionPerformed

        txtPeriodo.setEnabled(true);
        txtPeriodoMin.setEnabled(false);
        txtPeriodoMax.setEnabled(false);
        
        
        txtAmplitude.setEnabled(true);
        txtOffset.setEnabled(true);
        txtSetPoint.setEnabled(true);
        
        btnMalhaFechada.setEnabled(true);
        btnMalhaAberta.setEnabled(true);   

        // Botão Senoidal
        lblAmplitudeMax.setVisible(false);
        txtAmplitudeMax.setVisible(false);
        lblAmplitudeMin.setVisible(false);
        txtAmplitudeMin.setVisible(false);
        
        lblPeriodoMax.setVisible(false);
        txtPeriodoMax.setVisible(false);
        lblPeriodoMin.setVisible(false);
        txtPeriodoMin.setVisible(false);
        
        lblAmplitude.setVisible(true);
        txtAmplitude.setVisible(true);
        lblOffset.setVisible(true);
        txtOffset.setVisible(true);
        lblPeriodo.setVisible(true);        
        txtPeriodo.setVisible(true);

        if (btnMalhaFechada.isSelected()) {
            txtAmplitude.setVisible(false);
            lblAmplitude.setVisible(false);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("Por favor, utilizar valores entre 0 e 30cm");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        } else if (btnMalhaAberta.isSelected()) {
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        }

        txtInfo1.setText("Por favor, utilizar valores entre 0 e 3V");
        txtInfo2.setText("Por favor, utilizar valores em segundos");

    }//GEN-LAST:event_btnSenoidalActionPerformed

    private void btnQuadraticoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuadraticoActionPerformed
        txtPeriodo.setEnabled(true);
        txtPeriodoMin.setEnabled(false);
        txtPeriodoMax.setEnabled(false);
        
        txtAmplitude.setEnabled(true);
        txtOffset.setEnabled(true);
        txtSetPoint.setEnabled(true);
        txtAmplitudeMax.setEnabled(false);
        txtAmplitudeMin.setEnabled(false);
       
        btnMalhaFechada.setEnabled(true);
        btnMalhaAberta.setEnabled(true);   

        // Botão Quadrático
        
        lblAmplitudeMax.setVisible(false);
        txtAmplitudeMax.setVisible(false);
        lblAmplitudeMin.setVisible(false);
        txtAmplitudeMin.setVisible(false);
        
        lblPeriodoMax.setVisible(false);
        txtPeriodoMax.setVisible(false);
        lblPeriodoMin.setVisible(false);
        txtPeriodoMin.setVisible(false);
        
        lblAmplitude.setVisible(true);
        txtAmplitude.setVisible(true);
        lblOffset.setVisible(true);
        txtOffset.setVisible(true);
        lblPeriodo.setVisible(true);        
        txtPeriodo.setVisible(true);

        if (btnMalhaFechada.isSelected()) {
            txtAmplitude.setVisible(false);
            lblAmplitude.setVisible(false);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("Por favor, utilizar valores entre 0 e 30cm");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        } else if (btnMalhaAberta.isSelected()) {
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        }

    }//GEN-LAST:event_btnQuadraticoActionPerformed

    private void btnAleatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAleatorioActionPerformed
        txtPeriodo.setEnabled(false);
        txtPeriodoMin.setEnabled(true);
        txtPeriodoMax.setEnabled(true);
        
        
        txtAmplitude.setEnabled(true);
        txtOffset.setEnabled(true);
        txtSetPoint.setEnabled(true);
        txtAmplitudeMax.setEnabled(true);
        txtAmplitudeMin.setEnabled(true);
       
        btnMalhaFechada.setEnabled(true);
        btnMalhaAberta.setEnabled(true);   

        
        // Botão Aleatório
        
        lblOffset.setVisible(false);
        txtOffset.setVisible(false);
        lblPeriodo.setVisible(false);        
        txtPeriodo.setVisible(false);
        
        lblAmplitudeMax.setVisible(true);
        txtAmplitudeMax.setVisible(true);
        lblAmplitudeMin.setVisible(true);
        txtAmplitudeMin.setVisible(true);
        
        lblPeriodoMax.setVisible(true);
        txtPeriodoMax.setVisible(true);
        lblPeriodoMin.setVisible(true);
        txtPeriodoMin.setVisible(true);
        
        lblAmplitude.setVisible(false);
        txtAmplitude.setVisible(false);

         if (btnMalhaAberta.isSelected()) {
            lblAmplitude.setVisible(false);
            txtAmplitude.setVisible(false);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo2.setText("O valor do período será gerado aleatoriamente");
            txtInfo1.setText("O valor da amplitude será gerada aleatoriamente");
         }
         else if (btnMalhaFechada.isSelected()) {
            lblAmplitude.setVisible(false);
            txtAmplitude.setVisible(false);
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo2.setText("O valor do período será gerado aleatoriamente");
            txtInfo1.setText("O valor da amplitude será gerada aleatoriamente");
        }
    }//GEN-LAST:event_btnAleatorioActionPerformed

    private void btnDenteDeSerraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDenteDeSerraActionPerformed
        txtPeriodo.setEnabled(true);
        txtPeriodoMin.setEnabled(false);
        txtPeriodoMax.setEnabled(false);
        
        
        txtAmplitude.setEnabled(true);
        txtOffset.setEnabled(true);
        txtSetPoint.setEnabled(true);
        txtAmplitudeMax.setEnabled(false);
        txtAmplitudeMin.setEnabled(false);
       
        
        btnMalhaFechada.setEnabled(true);
        btnMalhaAberta.setEnabled(true);   

        
        // Botão Dente-de-Serra
        lblAmplitudeMax.setVisible(false);
        txtAmplitudeMax.setVisible(false);
        lblAmplitudeMin.setVisible(false);
        txtAmplitudeMin.setVisible(false);
        
        lblPeriodoMax.setVisible(false);
        txtPeriodoMax.setVisible(false);
        lblPeriodoMin.setVisible(false);
        txtPeriodoMin.setVisible(false);
        
        lblAmplitude.setVisible(true);
        txtAmplitude.setVisible(true);
        lblOffset.setVisible(true);
        txtOffset.setVisible(true);
        lblPeriodo.setVisible(true);        
        txtPeriodo.setVisible(true);

        if (btnMalhaFechada.isSelected()) {
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtAmplitude.setVisible(false);
            lblAmplitude.setVisible(false);
            txtInfo1.setText("Por favor, utilizar valores entre 0 e 30cm");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        } else if (btnMalhaAberta.isSelected()) {
            txtInfo1.setVisible(true);
            txtInfo2.setVisible(true);
            txtInfo1.setText("Por favor, utilizar valores entre -3 e 3V");
            txtInfo2.setText("Por favor, utilizar valores em segundos");
        }
    }//GEN-LAST:event_btnDenteDeSerraActionPerformed

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        if(aplicarParametros())
            clique_botao_iniciar();
        else
            JOptionPane.showMessageDialog(this, "Parâmetros incorretos");
    }//GEN-LAST:event_btnEnviarActionPerformed

    private void comboBoxPVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxPVActionPerformed
        // TODO add your handling code here:
        if (leitura00.isSelected()) {
        }
    }//GEN-LAST:event_comboBoxPVActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        habilitarEntradasControlador();
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void btnSimuladorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimuladorActionPerformed
        // TODO add your handling code here:
        txtIP.setText("127.0.0.1");
    }//GEN-LAST:event_btnSimuladorActionPerformed

    private void btnPlantaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlantaActionPerformed
        // TODO add your handling code here:
        txtIP.setText("10.13.99.69");
        //txtIP.setText("10.13.99.70");
    }//GEN-LAST:event_btnPlantaActionPerformed

    private void leitura07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura07ActionPerformed
        // TODO add your handling code here:

        atualizarCanaisPV();

        if (leitura07.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);
        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura07ActionPerformed

    private void escrita07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_escrita07ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_escrita07ActionPerformed

    private void leitura06ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura06ActionPerformed
        // TODO add your handling code here:

        atualizarCanaisPV();

        if (leitura06.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);
        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura06ActionPerformed

    private void leitura04ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura04ActionPerformed
        // TODO add your handling code here:

        atualizarCanaisPV();

        if (leitura04.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);
        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura04ActionPerformed

    private void leitura05ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura05ActionPerformed
        // TODO add your handling code here:

        atualizarCanaisPV();

        if (leitura05.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);
        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura05ActionPerformed

    private void leitura01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura01ActionPerformed
        // TODO add your handling code here

        atualizarCanaisPV();

        if (leitura01.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);
        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura01ActionPerformed

    private void leitura03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura03ActionPerformed
        // TODO add your handling code here:

        atualizarCanaisPV();

        if (leitura03.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);
        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura03ActionPerformed

    private void leitura02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura02ActionPerformed
        // TODO add your handling code here:

        atualizarCanaisPV();

        if (leitura02.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);

        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura02ActionPerformed

    private void leitura00ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leitura00ActionPerformed
        // TODO add your handling code here:
        atualizarCanaisPV();

        if (leitura00.isSelected()) {
            escrita00.setEnabled(true);
            escrita01.setEnabled(true);
            escrita02.setEnabled(true);
            escrita03.setEnabled(true);
            escrita04.setEnabled(true);
            escrita05.setEnabled(true);
            escrita06.setEnabled(true);
            escrita07.setEnabled(true);

            btnDegrau.setEnabled(true);
            btnSenoidal.setEnabled(true);
            btnQuadratico.setEnabled(true);
            btnDenteDeSerra.setEnabled(true);
            btnAleatorio.setEnabled(true);

        } else if (!leitura00.isSelected() && !leitura01.isSelected() && !leitura02.isSelected() && !leitura03.isSelected() && !leitura04.isSelected() && !leitura05.isSelected() && !leitura06.isSelected() && !leitura07.isSelected()) {
            escrita00.setEnabled(false);
            escrita01.setEnabled(false);
            escrita02.setEnabled(false);
            escrita03.setEnabled(false);
            escrita04.setEnabled(false);
            escrita05.setEnabled(false);
            escrita06.setEnabled(false);
            escrita07.setEnabled(false);

            btnDegrau.setEnabled(false);
            btnSenoidal.setEnabled(false);
            btnQuadratico.setEnabled(false);
            btnDenteDeSerra.setEnabled(false);
            btnAleatorio.setEnabled(false);
        }
    }//GEN-LAST:event_leitura00ActionPerformed

    private void btnAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarActionPerformed
        if(!aplicarParametros())
            JOptionPane.showMessageDialog(this, "Erro ao atualizar parâmetros");
    }//GEN-LAST:event_btnAtualizarActionPerformed

    private void chkFiltroDerivativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFiltroDerivativoActionPerformed
        // TODO add your handling code here:
        if(chkFiltroDerivativo.isSelected()){
                lblFiltroDerivativo.setVisible(true);
                txtFiltroDerivativo.setVisible(true);
                filtroDerivativo = true;
        } else if(!chkFiltroDerivativo.isSelected()){
                lblFiltroDerivativo.setVisible(false);
                txtFiltroDerivativo.setVisible(false);
                filtroDerivativo = false;
        }
    }//GEN-LAST:event_chkFiltroDerivativoActionPerformed

    private void radioAntiWindupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAntiWindupActionPerformed
        // TODO add your handling code here:
        if(radioAntiWindup.isSelected()){
                lblAntiWindUp.setVisible(true);
                txtAntiWindUp.setVisible(true);
                filtroAntiWindup = true;
        } else if(!radioAntiWindup.isSelected()){
                lblAntiWindUp.setVisible(false);
                txtAntiWindUp.setVisible(false);
                filtroAntiWindup = false;
        }
    }//GEN-LAST:event_radioAntiWindupActionPerformed

    private void radioFiltroCondicionalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioFiltroCondicionalActionPerformed
        // TODO add your handling code here:
        if (radioFiltroCondicional.isSelected()){
                lblAntiWindUp.setVisible(false);
                txtAntiWindUp.setVisible(false);
                filtroCondicional = true;
        } else filtroCondicional = false;
        
    }//GEN-LAST:event_radioFiltroCondicionalActionPerformed

    private void chkSemFiltroIntegrativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkSemFiltroIntegrativoActionPerformed
        // TODO add your handling code here:
        if(chkSemFiltroIntegrativo.isSelected()){
            radioAntiWindup.setEnabled(false);
            radioFiltroCondicional.setEnabled(false);
            semFiltro = true;
        } else if(!chkSemFiltroIntegrativo.isSelected()){
            radioAntiWindup.setEnabled(true);
            radioFiltroCondicional.setEnabled(true);
            semFiltro = false;
        }
        
        if(!chkSemFiltroIntegrativo.isSelected() && radioAntiWindup.isSelected()){
            lblAntiWindUp.setVisible(true);
            txtAntiWindUp.setVisible(true);
            semFiltro = false;
        } else if(chkSemFiltroIntegrativo.isSelected() && radioAntiWindup.isSelected()){
            lblAntiWindUp.setVisible(false);
            txtAntiWindUp.setVisible(false);
            semFiltro = true;
        }
    }//GEN-LAST:event_chkSemFiltroIntegrativoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnAleatorio;
    private javax.swing.JButton btnAtualizar;
    private javax.swing.JToggleButton btnDegrau;
    private javax.swing.JToggleButton btnDenteDeSerra;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JToggleButton btnMalhaAberta;
    private javax.swing.JToggleButton btnMalhaFechada;
    private javax.swing.JButton btnPlanta;
    private javax.swing.JToggleButton btnQuadratico;
    private javax.swing.JToggleButton btnSenoidal;
    private javax.swing.JButton btnSimulador;
    private javax.swing.JCheckBox chkFiltroDerivativo;
    private javax.swing.JCheckBox chkSemFiltroIntegrativo;
    private javax.swing.JComboBox comboBoxPV;
    private javax.swing.JRadioButton escrita00;
    private javax.swing.JRadioButton escrita01;
    private javax.swing.JRadioButton escrita02;
    private javax.swing.JRadioButton escrita03;
    private javax.swing.JRadioButton escrita04;
    private javax.swing.JRadioButton escrita05;
    private javax.swing.JRadioButton escrita06;
    private javax.swing.JRadioButton escrita07;
    private javax.swing.ButtonGroup grupoControlador;
    private javax.swing.ButtonGroup grupoMalhas;
    private javax.swing.ButtonGroup grupoPortasEscrita;
    private javax.swing.ButtonGroup grupoSinais;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jtextkd;
    private javax.swing.JTextField jtextki;
    private javax.swing.JTextField jtextkp;
    private javax.swing.JTextField jtexttd;
    private javax.swing.JTextField jtextti;
    private javax.swing.JLabel labelEscrita;
    private javax.swing.JLabel labelLeitura;
    private javax.swing.JLabel labelTitulo;
    private javax.swing.JLabel lblAmplitude;
    private javax.swing.JLabel lblAmplitudeMax;
    private javax.swing.JLabel lblAmplitudeMin;
    private javax.swing.JLabel lblAntiWindUp;
    private javax.swing.JLabel lblFiltroDerivativo;
    private javax.swing.JLabel lblIP;
    private javax.swing.JLabel lblOffset;
    private javax.swing.JLabel lblPV;
    private javax.swing.JLabel lblPeriodo;
    private javax.swing.JLabel lblPeriodoMax;
    private javax.swing.JLabel lblPeriodoMin;
    private javax.swing.JLabel lblPorta;
    private javax.swing.JLabel lblSetPoint;
    private javax.swing.JCheckBox leitura00;
    private javax.swing.JCheckBox leitura01;
    private javax.swing.JCheckBox leitura02;
    private javax.swing.JCheckBox leitura03;
    private javax.swing.JCheckBox leitura04;
    private javax.swing.JCheckBox leitura05;
    private javax.swing.JCheckBox leitura06;
    private javax.swing.JCheckBox leitura07;
    private javax.swing.JPanel panelDados;
    private javax.swing.JPanel panelMalha;
    private javax.swing.JPanel panelPV;
    private javax.swing.JPanel panelPortas;
    private javax.swing.JPanel panelSinal;
    private javax.swing.JRadioButton radioAntiWindup;
    private javax.swing.JRadioButton radioFiltroCondicional;
    private javax.swing.JTextField txtAmplitude;
    private javax.swing.JTextField txtAmplitudeMax;
    private javax.swing.JTextField txtAmplitudeMin;
    private javax.swing.JTextField txtAntiWindUp;
    private javax.swing.JTextField txtFiltroDerivativo;
    private javax.swing.JTextField txtIP;
    private javax.swing.JLabel txtInfo1;
    private javax.swing.JLabel txtInfo2;
    private javax.swing.JTextField txtOffset;
    private javax.swing.JTextField txtPeriodo;
    private javax.swing.JTextField txtPeriodoMax;
    private javax.swing.JTextField txtPeriodoMin;
    private javax.swing.JTextField txtPorta;
    private javax.swing.JTextField txtSetPoint;
    // End of variables declaration//GEN-END:variables

    private void addWindowListener(WindowAdapter windowAdapter) {
    }
}
