/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.interfaceCliente;

import br.ufrn.controle.clienteControleTanque.ClienteControleTanque;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import javax.swing.Timer;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Adriana
 */

public class Grafico extends javax.swing.JFrame {
    
    private ClienteControleTanque clienteControle;
    private Timer timer;
    
    public Grafico(ClienteControleTanque cliente) {
        initComponents();
        clienteControle = cliente;
        
        addWindowListener(  
            new WindowAdapter()   
            {
                public void windowClosing(WindowEvent we) {
                    clienteControle.stop();
                }
            }
        );
    }
    
    public void criar(){
        XYSeriesCollection datasetTensao = new XYSeriesCollection();
        XYSeriesCollection datasetNivel = new XYSeriesCollection();
        XYSeriesCollection datasetControlador = new XYSeriesCollection();
            
        try {
            if (clienteControle.malhaFechada()) {
                datasetControlador.addSeries(clienteControle.getSinalCalculadoP());
                datasetControlador.addSeries(clienteControle.getSinalCalculadoI());
                datasetControlador.addSeries(clienteControle.getSinalCalculadoD());
                datasetControlador.addSeries(clienteControle.getSinalCalculado());
                
                datasetNivel.addSeries(clienteControle.getSinalDesejado());
                datasetNivel.addSeries(clienteControle.getSinalErro());
                datasetNivel.addSeries(clienteControle.getSinalEscrito());
            }
            else {
                datasetTensao.addSeries(clienteControle.getSinalDesejado());
                datasetTensao.addSeries(clienteControle.getSinalEscrito());
            }

            datasetNivel.addSeries(clienteControle.getSinalCanal0());
            if (clienteControle.exibirCanal1())
                datasetNivel.addSeries(clienteControle.getSinalCanal1());
            
            //Gráfico 1: Nível
            JFreeChart graf1 = ChartFactory.createXYLineChart(
                     "Gráfico de Nível",
                     "Tempo (s)",
                     "Nível (cm)",
                     datasetNivel,
                     PlotOrientation.VERTICAL,
                     true,
                     true,
                     false
             );

            ChartPanel chartPanel1 = new ChartPanel(graf1, true);
            chartPanel1.setSize(jPanel1.getWidth(),jPanel1.getHeight());
            chartPanel1.setVisible(true); 
            jPanel1.removeAll();
            jPanel1.add(chartPanel1); 
            jPanel1.revalidate();
            jPanel1.repaint();
            
            //Gráfico 2: Tensão (malha aberta) ou Controladores (malha fechada)
            JFreeChart graf2;
            if (clienteControle.malhaAberta()) {
                graf2 = ChartFactory.createXYLineChart(
                            "Gráfico de Tensão",
                            "Tempo (s)",
                            "Tensão (V)",
                            datasetTensao,
                            PlotOrientation.VERTICAL,
                            true,
                            true,
                            false
                        );
            }
            else {
                graf2 = ChartFactory.createXYLineChart(
                            "Gráfico dos Controladores",
                            "Tempo (s)",
                            "Tensão (v)",
                            datasetControlador,
                            PlotOrientation.VERTICAL,
                            true,
                            true,
                            false
                        );
            }
            
            ChartPanel chartPanel2 = new ChartPanel(graf2, true);
            chartPanel2.setSize(jPanel2.getWidth(),jPanel2.getHeight());
            chartPanel2.setVisible(true);
            jPanel2.removeAll();
            jPanel2.add(chartPanel2); 
            jPanel2.revalidate();
            jPanel2.repaint();
            
            showVariaveisRegime();
        }  
        catch (Exception e) {  
            e.printStackTrace();  
        }
    }
    
    
    public void showVariaveisRegime() {
        final DecimalFormat decimal = new DecimalFormat("0.00");
        
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lblTr100.setText(String.valueOf(decimal.format(clienteControle.getTr_0_100())));
                lblTr95.setText(String.valueOf(decimal.format(clienteControle.getTr_5_95())));
                lblTr90.setText(String.valueOf(decimal.format(clienteControle.getTr_10_90())));
                
                lblTs2.setText(String.valueOf(decimal.format(clienteControle.getTs_2())));
                lblTs5.setText(String.valueOf(decimal.format(clienteControle.getTs_5())));
                lblTs7.setText(String.valueOf(decimal.format(clienteControle.getTs_7())));
                lblTs10.setText(String.valueOf(decimal.format(clienteControle.getTs_10())));
                
                lblMpAbs.setText(String.valueOf(decimal.format(clienteControle.getMpAbs())));
                lblMpPer.setText(String.valueOf(decimal.format(clienteControle.getMpPerc())));
                lblTp.setText(String.valueOf(decimal.format(clienteControle.getTp())));
                
                lblEssAbs.setText(String.valueOf(decimal.format(clienteControle.getEssAbs())));
                lblEssPerc.setText(String.valueOf(decimal.format(clienteControle.getEssPerc())));
            }
        };
        
        this.timer = new Timer(100, action);
        this.timer.start();
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabelTs2 = new javax.swing.JLabel();
        jLabelTs5 = new javax.swing.JLabel();
        jLabelTs7 = new javax.swing.JLabel();
        jLabelTs10 = new javax.swing.JLabel();
        lblTs2 = new javax.swing.JLabel();
        lblTs5 = new javax.swing.JLabel();
        lblTs7 = new javax.swing.JLabel();
        lblTs10 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabelMp = new javax.swing.JLabel();
        jLabelTp = new javax.swing.JLabel();
        lblMpAbs = new javax.swing.JLabel();
        lblTp = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lblTr100 = new javax.swing.JLabel();
        lblTr95 = new javax.swing.JLabel();
        lblTr90 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        lblMpPer = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblEssAbs = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lblEssPerc = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 434, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 368, Short.MAX_VALUE)
        );

        jLabelTs2.setText("Ts(2%)");

        jLabelTs5.setText("Ts(5%)");

        jLabelTs7.setText("Ts(7%)");

        jLabelTs10.setText("Ts(10%)");

        lblTs2.setText("--");

        lblTs5.setText("--");

        lblTs7.setText("--");

        lblTs10.setText("--");

        jLabel1.setText("s");

        jLabel2.setText("s");

        jLabel3.setText("s");

        jLabel4.setText("s");

        jLabelMp.setText("Mp");

        jLabelTp.setText("Tp");

        lblMpAbs.setText("--");

        lblTp.setText("--");

        jLabel5.setText("cm");

        jLabel6.setText("s");

        jLabel8.setText("Tr (0 - 100%)");

        jLabel9.setText("Tr (5 - 95%)");

        jLabel10.setText("Tr (10 - 90%)");

        lblTr100.setText("--");

        lblTr95.setText("--");

        lblTr90.setText("--");

        jLabel11.setText("s");

        jLabel12.setText("s");

        jLabel13.setText("s");

        jButton2.setText("Parar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel14.setText("Mp");

        lblMpPer.setText("--");

        jLabel16.setText("%");

        jLabel7.setText("Ess");

        lblEssAbs.setText("--");

        jLabel15.setText("cm");

        jLabel17.setText("Ess");

        lblEssPerc.setText("--");

        jLabel19.setText("%");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(2, 2, 2))
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTr100)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTr95)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel12))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(lblTr90)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13)))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMp)
                            .addComponent(jLabelTp, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTp)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblMpAbs)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel5))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(18, 18, 18)
                        .addComponent(lblMpPer)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel16)))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelTs10)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(4, 4, 4)
                                    .addComponent(jLabelTs2))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelTs5)))
                            .addComponent(jLabelTs7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(lblTs10))
                                    .addComponent(lblTs7))
                                .addGap(19, 19, 19)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel3)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(lblTs5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblTs2))
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lblEssAbs)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel15))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel17)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lblEssPerc)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel19)))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 308, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelMp)
                                    .addComponent(lblMpAbs)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel7)
                                    .addComponent(lblEssAbs)
                                    .addComponent(jLabel15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel14)
                                    .addComponent(lblMpPer)
                                    .addComponent(jLabel16)
                                    .addComponent(jLabel17)
                                    .addComponent(lblEssPerc)
                                    .addComponent(jLabel19))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblTp)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabelTp)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(lblTr100)
                                    .addComponent(jLabel11))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(lblTr95)
                                    .addComponent(jLabel12))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel10)
                                    .addComponent(lblTr90)
                                    .addComponent(jLabel13)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelTs2)
                                    .addComponent(lblTs2)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelTs5)
                                    .addComponent(jLabel2)
                                    .addComponent(lblTs5, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelTs7)
                                    .addComponent(lblTs7)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelTs10)
                                    .addComponent(lblTs10)
                                    .addComponent(jLabel4))))
                        .addGap(0, 18, Short.MAX_VALUE))))
        );

        jLabel8.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        clienteControle.stop();
        jButton2.disable();
    }//GEN-LAST:event_jButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelMp;
    private javax.swing.JLabel jLabelTp;
    private javax.swing.JLabel jLabelTs10;
    private javax.swing.JLabel jLabelTs2;
    private javax.swing.JLabel jLabelTs5;
    private javax.swing.JLabel jLabelTs7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblEssAbs;
    private javax.swing.JLabel lblEssPerc;
    private javax.swing.JLabel lblMpAbs;
    private javax.swing.JLabel lblMpPer;
    private javax.swing.JLabel lblTp;
    private javax.swing.JLabel lblTr100;
    private javax.swing.JLabel lblTr90;
    private javax.swing.JLabel lblTr95;
    private javax.swing.JLabel lblTs10;
    private javax.swing.JLabel lblTs2;
    private javax.swing.JLabel lblTs5;
    private javax.swing.JLabel lblTs7;
    // End of variables declaration//GEN-END:variables
}
