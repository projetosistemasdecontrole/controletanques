/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.ufrn.controle.modelo;

/**
 *
 * @author Adriana
 */

public enum Funcao {
    
    Degrau(0),
    Quadrada(1),
    Senoide(2),
    DenteDeSerra(3),
    Random(4);
    
    private int id;
    
    Funcao(int id) {
       this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
}
