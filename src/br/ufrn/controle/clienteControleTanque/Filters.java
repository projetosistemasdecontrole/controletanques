package br.ufrn.controle.clienteControleTanque;

public class Filters {
        //Atributos Privados
        private double tiAnterior;
        private double tawAnterior;
        private double tdAnterior;
        
        // Construtor
        public Filters(){
            tiAnterior = 0.0;
            tawAnterior = 0.0;
            tdAnterior = 0.0;
        }
        
        public void atualizarTieTawAnterior(double tiAtual, double tawAtual){
            tiAnterior = tiAtual;
            tawAnterior = tawAtual;
        }
        
        public void atualizarTdAnterior(double tdAtual){
            tdAnterior = tdAtual;
        }
    
        // Filtro Integrativo Condicional
        // referencia: https://repositorio.ufsc.br/bitstream/handle/123456789/102669/223414.pdf?sequence=1 página: 12
	public double filterIntegrativeCondicional(double erro, double setpoint, double ki){
            // Foi escolhido este fator com intuito de fazer o erro ficar mais próximo do setpoint
            if (Math.abs(erro) < (setpoint/2 + setpoint/4)){
                return ki;
            }
            else return 0.0;
	}

	// Filtro Integrativo Anti-windup
	// Tt é o ganho do integrador e de acordo com esta 
	// referencia: http://www.scs-europe.net/services/ecms2006/ecms2006%20pdf/107-ind.pdf
	// pode ser calculado através de Tt = sqrt(Ti * Td) = sqrt(kp/ki * kd/kp) = sqrt(kd/ki)
	// Valor de amostragem = 0.1
	public double filterIntegrativeAntiWindup(double tiAtual, double tawAtual,
						  double erroAntesTravas, double erroDepoisTravas,
						  double amostragem){
            double erroSaturado = erroDepoisTravas - erroAntesTravas;
            double numerador = amostragem * (tawAtual + (tawAtual * erroSaturado));
            double denominador = (tiAtual * tawAtual) - (tiAnterior * tawAnterior);
            this.atualizarTieTawAnterior(tiAtual, tawAtual);
            return (numerador / denominador);
	}

	// Filtro Derivativo
	// td = kd/kp e é retirado da interface (pode ser aproximado)
	// Valor de amostragem = 0.1
	public double filterDerivative(double tdAtual, double erroAtual,
                                       double constFilterDerivative, double amostragem){
            double numerador = tdAtual - tdAnterior;
            double denominador = amostragem + (constFilterDerivative * tdAtual) - (constFilterDerivative * tdAnterior);
            this.atualizarTdAnterior(tdAtual);
            return (numerador / denominador);
	}
    
}
