/**
 * \file ClienteControleTanque.java
 * 
 * \brief Este arquivo contém a chamada da aplicação referente ao controle de uma planta de tanques.
 * 
 * \author 
 * Adriana Benício Galvão \n
 * Bruno de Almeida Silva \n
 * Daniel Silva de Oliveira \n
 * Petrucio Ricardo Tavares de Medeiros \n
 * Talison Augusto Correia de Melo \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * adriana_benicio06 at yahoo (dot) com (dot) br \n
 * brunoalmeidarn at outlook (dot) com \n
 * tres.daniel at hotmail (dot) com \n
 * petrucior at gmail (dot) com \n
 * talisonaugusto at hotmail (dot) com
 * \version 0.0
 * \date February 2014
 */

package br.ufrn.controle.clienteControleTanque;

import br.ufrn.controle.interfaceCliente.TelaCliente;
import br.ufrn.controle.modelo.Controles;
import br.ufrn.controle.modelo.Funcao;
import br.ufrn.controle.modelo.Malha;
import br.ufrn.dca.controle.QuanserClient;
import br.ufrn.dca.controle.QuanserClientException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.jfree.data.xy.XYSeries;


public class ClienteControleTanque {

    final private TelaCliente telaCliente;
    final private LogicaControle logicaControle;
    final private Filters filtros;
    final private VariaveisControle variaveisControle;
    private QuanserClient quanserClient;
    private double time = 0;
    private boolean conectado = false;
    
    //Valor dos canais/sensores
    private double sensor0 = 0;
    private double sensor1 = 0;
    
    //Constante de multiplicação para cm
    private double v_to_cm = 6.25;
    
    //Variáveis do sinal
    private double setPoint = 0;
    private double erroRastreamento = 0;
    private double erroAposControlador = 0;
    private double sinalEscrito = 0;
    private double pv = 0;    
    
    //Variável auxiliar do filtro anti-windup
    private double sinalAntesTravas;
    private double sinalDepoisTravas;
    private boolean ehPrimeiraVez;
    
    public ClienteControleTanque(final TelaCliente telaCliente) {
        this.telaCliente = telaCliente;
        this.logicaControle = new LogicaControle();
        this.filtros = new Filters();
        this.variaveisControle = new VariaveisControle();
        sinalAntesTravas = 0.0;
        sinalDepoisTravas = 0.0;
        ehPrimeiraVez = true;
    }
    
    final public void conectar() {
        try {
            quanserClient = new QuanserClient(telaCliente.getIp(), Integer.parseInt(telaCliente.getPorta()));
            conectado = true;
        }
        catch (QuanserClientException ex) {
            conectado = false;
            System.out.print("Erro ao tentar conexão");
        }
    }
    
    final public void desconectar() {
        if (conectado) {
            System.out.print("Desconectando...");
            try {
                quanserClient.write(0, 0);
                System.out.print("Desconectado com sucesso.");
            }
            catch(Exception ex) {
                System.out.print("Erro ao enviar tensão 0.");
                System.out.print(ex.getMessage());
            }
        }
        else
            System.out.print("Cliente desconectado");
        
        conectado = false;
    }
    
    private ClienteControleTanque me() {
        return this;
    }
    
    public boolean conectado() {
        return this.conectado;
    }
    
    
    public void iniciar(String ip, String porta) 
    {
        if (!conectado)
            conectar();
        
        if (conectado) {           
            final ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
            exec.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    //long startTempo, endTempo;
                    try {
                        //startTempo = System.currentTimeMillis(); 
                        if (!telaCliente.parado()) {
                            
                            sensor0 = v_to_cm * quanserClient.read(0);
                            if (telaCliente.lerCanal1())
                                sensor1 = v_to_cm * quanserClient.read(1);

                            pv = logicaControle.setSensores(time, sensor0, sensor1, telaCliente.getCanalPV());

                            if (telaCliente.getFuncao() == Funcao.Random)
                                setPoint = logicaControle.getRetornoFuncaoRandom(telaCliente.getAmplitudeMinima(), telaCliente.getAmplitudeMaxima(), telaCliente.getPeriodoMinimo(), telaCliente.getPeriodoMaximo(), time);
                            else
                                setPoint = logicaControle.getRetornoFuncao(telaCliente.getFuncao(), telaCliente.getAmplitude(), telaCliente.getOffset(), telaCliente.getPeriodo(), time);
                            
                            variaveisControle.avaliandoSinal(pv, setPoint, time);
                            
                            
                            if (telaCliente.getMalha() == Malha.Fechada) {
                                //erroRastreamento = logicaControle.getErro(time, setPoint, telaCliente.getControle(), telaCliente.getkp(), telaCliente.getki(), telaCliente.getkd());
                                if (verificaFiltroCondicional()){   // Filtro condicional selecionado
                                    erroRastreamento = logicaControle.getErro(time, setPoint);
                                    erroAposControlador = logicaControle.aplicarControlador(time, erroRastreamento, telaCliente.getControle(), telaCliente.getkp(), telaCliente.getki(), telaCliente.getkd());
                                }
                                else{
                                    // De acordo com http://www.ece.ufrgs.br/~fetter/eng04037/pid.pdf Ti = Kp/Ki
                                    if (verificaAntiWindup()){ // Filtro Anti-windup selecionado
                                         // Falta implementar!
                                        if (ehPrimeiraVez == true){
                                            sinalAntesTravas = erroRastreamento;
                                            sinalDepoisTravas = logicaControle.aplicarTravas(time, erroRastreamento);
                                            ehPrimeiraVez = false;
                                        }
                                        System.out.println("erro anterior:" + String.valueOf(sinalAntesTravas));
                                        System.out.println("erro depois:" + String.valueOf(sinalDepoisTravas));
                                        
                                        erroRastreamento = logicaControle.getErro(time, setPoint);
                                        double contribuicaoIntegrativa = filtros.filterIntegrativeAntiWindup((telaCliente.getkp()/telaCliente.getki()), getConstAntiWindup(), sinalAntesTravas, sinalDepoisTravas, 0.1);
                                        erroAposControlador = logicaControle.aplicarControlador(time, erroRastreamento, telaCliente.getControle(), telaCliente.getkp(), contribuicaoIntegrativa, telaCliente.getkd());
                                    }
                                    else{
                                        // De acordo com http://www.ece.ufrgs.br/~fetter/eng04037/pid.pdf Td = Kd/Kp
                                        if (verificarDerivativo()){ // Filtro derivado selecionado
                                            erroRastreamento = logicaControle.getErro(time, setPoint);
                                            erroAposControlador = logicaControle.aplicarControlador(time, erroRastreamento, telaCliente.getControle(), telaCliente.getkp(), telaCliente.getki(), filtros.filterDerivative((telaCliente.getkd()/telaCliente.getkp()), erroRastreamento, getConstFilterDerivative(), 0.1));
                                        }
                                        else{   // Sem filtro
                                            erroRastreamento = logicaControle.getErro(time, setPoint);
                                            erroAposControlador = logicaControle.aplicarControlador(time, erroRastreamento, telaCliente.getControle(), telaCliente.getkp(), telaCliente.getki(), telaCliente.getkd());
                                        }
                                    }
                                }
                                
                                sinalEscrito = logicaControle.aplicarTravas(time, erroAposControlador);
                            }
                            else
                                sinalEscrito = logicaControle.aplicarTravas(time, setPoint);
                            
                            sinalAntesTravas = erroRastreamento;
                            sinalDepoisTravas = sinalEscrito;
                            
                            quanserClient.write(telaCliente.getCanalEscrita(), sinalEscrito);
                            time = time+0.1;
                        }
                        else
                        {
                            exec.shutdownNow();
                            me().desconectar();
                        }
                        //endTempo=System.currentTimeMillis();
                        //System.out.println(endTempo - startTempo);
                    }
                    catch(Exception ex) {
                        exec.shutdownNow();
                        me().desconectar();
                        ex.printStackTrace();
                    }
                }
            }, 100, 100, TimeUnit.MILLISECONDS);
        }
    }
    

    /*------- Variáveis de LogicaControle ---------------*/
    
    public XYSeries getSinalDesejado() {
        return logicaControle.getSinalDesejado();
    }
    
    public XYSeries getSinalErro() {
        return logicaControle.getSinalErro();
    }
    
    public XYSeries getSinalCalculado() {
        return logicaControle.getSinalCalculado();
    }
    
    public XYSeries getSinalCalculadoP() {
        return logicaControle.getSinalCalculadoP();
    }
    
    public XYSeries getSinalCalculadoI() {
        return logicaControle.getSinalCalculadoI();
    }
    
    public XYSeries getSinalCalculadoD() {
        return logicaControle.getSinalCalculadoD();
    }
    
    public XYSeries getSinalEscrito() {
        return logicaControle.getSinalEscrito();
    }
    
    public XYSeries getSinalCanal0() {
        return logicaControle.getSinalCanal0();
    }
    
    public XYSeries getSinalCanal1() {
        return logicaControle.getSinalCanal1();
    }
    
    
    /*---------- Valores das Variaveis de Regime ---------------*/
    
    public double getTr_0_100(){
        return variaveisControle.getTr_0_100();
    }
    
    public double getTr_5_95(){
        return variaveisControle.getTr_5_95();
    }
    
    public double getTr_10_90(){
        return variaveisControle.getTr_10_90();
    }
    
    public double getMpAbs(){
        return variaveisControle.getMpAbs();
    }
    
    public double getMpPerc(){
        return variaveisControle.getMpPerc();
    }
    
    public double getEssAbs(){
        return variaveisControle.getEssAbs();
    }
    
    public double getEssPerc(){
        return variaveisControle.getEssPerc();
    }
    
    public double getTp(){
        return variaveisControle.getTp();
    }
    
    public double getTs_2(){
        return variaveisControle.getTs_2();
    }	
    
    public double getTs_5(){
        return variaveisControle.getTs_5();
    }
    
    public double getTs_7(){
        return variaveisControle.getTs_7();
    }
   
    public double getTs_10(){
        return variaveisControle.getTs_10();
    }
    
    
    /*---------- Variáveis da Tela do Cliente ---------------*/
    
    public boolean exibirCanal0() {
        return telaCliente.lerCanal0();
    }
    
    public boolean exibirCanal1() {
        return telaCliente.lerCanal1();
    }
    
    public boolean P() {
        if (telaCliente.getControle() == Controles.P)
            return true;
        else
            return false;
    }
    
    public boolean PI() {
        if (telaCliente.getControle() == Controles.PI)
            return true;
        else
            return false;
    }
    
    public boolean PD() {
        if (telaCliente.getControle() == Controles.PD)
            return true;
        else
            return false;
    }
    
    public boolean PID() {
        if (telaCliente.getControle() == Controles.PID)
            return true;
        else
            return false;
    }
    
    public boolean PI_D() {
        if (telaCliente.getControle() == Controles.PI_D)
            return true;
        else
            return false;
    }
    
    public boolean malhaAberta() {
        return (telaCliente.getMalha() == Malha.Aberta);
    }
    
    public boolean malhaFechada() {
        return (telaCliente.getMalha() == Malha.Fechada);
    }
    
    public boolean verificaFiltroCondicional(){
        return telaCliente.verificarFiltroCondicional();
    }

    public boolean verificaAntiWindup(){
        return telaCliente.verificarAntiWindup();
    }
    
    public boolean verificarIntegrativos(){
        return telaCliente.verificarIntegrativos();
    }
        
    public boolean verificarDerivativo() {
        return telaCliente.filtroDerivativo();
    }
    
    public double getConstFilterDerivative(){
        return telaCliente.getConstFilterDerivative();
    }
    
    public double getConstAntiWindup(){
        return telaCliente.getConstAntiWindup();
    }
    
    public boolean running() {
        return (!telaCliente.parado());
    }
    
    public void stop() {
        if (this.running())
            this.telaCliente.clique_botao_iniciar();
    }
}
