/**
 * \file LogicaControle.java
 * 
 * \brief Este arquivo contém a lógica do programa referente ao controle de uma planta de tanques.
 * 
 * \Author 
 * Adriana Benício Galvão \n
 * Bruno de Almeida Silva \n
 * Daniel Silva de Oliveira \n
 * Petrucio Ricardo Tavares de Medeiros \n
 * Talison Augusto Correia de Melo \n
 * Universidade Federal do Rio Grande do Norte \n
 * Departamento de Computacao e Automacao Industrial \n
 * adriana_benicio06 at yahoo (dot) com (dot) br \n
 * brunoalmeidarn at outlook (dot) com \n
 * tres.daniel at hotmail (dot) com \n
 * petrucior at gmail (dot) com \n
 * talisonaugusto at hotmail (dot) com
 * \version 0.0
 * \date February 2014
 */

package br.ufrn.controle.clienteControleTanque;

import br.ufrn.controle.modelo.Controles;
import br.ufrn.controle.modelo.Funcao;
import java.util.Random;
import org.jfree.data.xy.XYSeries;

/*
 * \class LogicaControle
 * 
 * \brief Esta classe é a responsável por toda a lógica do projeto
 */

public class LogicaControle {
   
    // Valores auxiliares para configuração randômica
    private double periodoRandom = -1; //segundos
    private double amplitudeRandom;
    
    //variaveis auxiliares para calculo das tecnicas de controle (P, PD, PI, PID, PI-D)
    private double h = 0.1;
    private double erro_integral_anterior;
    private double erro_anterior;
    private double nivel_anterior;
    
    //Valor da PV (variável controlada quando for malha fechada)
    private double _pv;
    
    //Nível do tanque superior
    private double _nivel;
    
    //Arrays para os gráficos
    private XYSeries _sinalDesejado = new XYSeries("Sinal Desejado"); //getValorFuncao
    private XYSeries _sinalErro = new XYSeries("Sinal de Erro"); //getErro
    private XYSeries _sinalEscrito = new XYSeries("Sinal Escrito"); //aplicarTravas
    private XYSeries _sinalControlador = new XYSeries("Saída Controlador"); //aplicarControlador
    private XYSeries _sinalCanal0 = new XYSeries("Canal 0");
    private XYSeries _sinalCanal1 = new XYSeries("Canal 1");
    private XYSeries _sinalControladorP = new XYSeries("Saída P");
    private XYSeries _sinalControladorI = new XYSeries("Saída I");
    private XYSeries _sinalControladorD = new XYSeries("Saída D");


    public LogicaControle(){
        erro_integral_anterior = 0.0;
        erro_anterior = 0.0;
        nivel_anterior = 0.0;
    }

    
    //Funções
    public double getDegrau(double amplitude, double offset){
         return (amplitude + offset);
     }

    public double getSenoide(double amplitude, double periodo, double offset, double tempo){
         return ((amplitude * Math.sin((2.0*Math.PI*tempo)/periodo)) + offset);
     }

    public double getQuadrada(double amplitude, double periodo, double offset, double tempo){ 
         if (tempo % periodo <= (periodo/2.0)) {
             return (offset - amplitude);
         }
         else{
             return (offset + amplitude);
         }
     }
     
    public double getDenteSerra(double amplitude, double periodo, double offset, double tempo){
         return (((amplitude / (periodo)) * ((tempo/4) % ((periodo+0.125)*2))) - (amplitude)+ offset);
     }
     
    
    //Chamada das funções
    public double getRetornoFuncao(Funcao funcao, double amplitude, double offset, double periodo, double tempo) {
        double y = 0.0;
        
        if (funcao == Funcao.Degrau)
            y = getDegrau(amplitude, offset);
        else if (funcao == Funcao.Senoide)
            y = getSenoide(amplitude, periodo, offset, tempo);
        else if (funcao == Funcao.DenteDeSerra)
            y = getDenteSerra(amplitude, periodo, offset, tempo);
        else if (funcao == Funcao.Quadrada)
            y = getQuadrada(amplitude, periodo, offset, tempo);
        
        _sinalDesejado.add(tempo, y);
        return y;
    }

    public double getRetornoFuncaoRandom(double amp_min, double amp_max, double per_min, double per_max, double tempo) {   
        if (tempo >= periodoRandom) {
           periodoRandom = (per_min + (int) (Math.random() * ((per_max - per_min) + 1)));
           amplitudeRandom = amp_min + (amp_max - amp_min) * new Random().nextDouble();
        }
        
        _sinalDesejado.add(tempo, amplitudeRandom);
        return amplitudeRandom;
    }
    
    
    //Sensores
    public double setSensores(double time, double canal0, double canal1, int numCanalPV) {
        this._nivel = canal0;

        if (numCanalPV == 0)
            this._pv = canal0;
        else if (numCanalPV == 1)
            this._pv = canal1;

        _sinalCanal0.add(time, canal0);
        _sinalCanal1.add(time, canal1);
        
        return _pv;
    }
    
    
    //Erro
    public double getErro(double time, double setPoint) {
        double erro = setPoint - _pv;
        _sinalErro.add(time, erro);
        
        return erro;
    }
    
    public double aplicarControlador(double time, double erroRastreamento, Controles controle, double kp, double ki, double kd) {
        double controlador = 0.0;
        
        if (controle == Controles.P)
            controlador = P(time,erroRastreamento,kp);
        else if (controle == Controles.PI)
            controlador = PI(time,erroRastreamento,kp,ki);
        else if (controle == Controles.PD)
            controlador = PD(time,erroRastreamento,kp,kd);
        else if (controle == Controles.PID)
            controlador = PID(time,erroRastreamento,kp,ki,kd);
        else if (controle == Controles.PI_D)
            controlador = PI_D(time,erroRastreamento,kp,ki,kd);
        
        _sinalControlador.add(time, controlador);
        return controlador;
    }

    
    //Arrays dos sinais para os gráficos
    public XYSeries getSinalDesejado() {
        return _sinalDesejado;
    }
    
    public XYSeries getSinalErro() {
        return _sinalErro;
    }
    
    public XYSeries getSinalEscrito() {
        return _sinalEscrito;
    }
    
    public XYSeries getSinalCalculado() {
        return _sinalControlador;
    }
    
    public XYSeries getSinalCalculadoP() {
        return _sinalControladorP;
    }
    
    public XYSeries getSinalCalculadoI() {
        return _sinalControladorI;
    }
    
    public XYSeries getSinalCalculadoD() {
        return _sinalControladorD;
    }
    
    public XYSeries getSinalCanal0() {
        return _sinalCanal0;
    }
    
    public XYSeries getSinalCanal1() {
        return _sinalCanal1;
    }
    
     
    //Travas
    public double aplicarTravas(double time, double tensao) {
        double v = tensao;

        //Trava 1
        if (tensao < -3)
            v = -3;
        else if (tensao > 3)
            v = 3;
        
        //Trava 2
        if (_nivel > 28 && tensao > 1.8)
            v = 1.8;
        
        //Travas 3 e 4
        if ((_nivel > 29 && tensao > 0) || (_nivel < 3 && tensao < 0))
            v = 0;
        
        _sinalEscrito.add(time, v);
        return v;
    }
    
    
    //Controle
    public double P(double time, double erro, double kp){
        double P = erro * kp;
        _sinalControladorP.add(time,P);
        return P;
    }
    
    public double I(double time, double erro, double ki) {
        double I = erro_integral_anterior + ki*h*erro;
        _sinalControladorI.add(time,I);
        return I;
    }
    
    public double D(double time, double erro, double kd) {
        double D = kd * (erro-erro_anterior)/h;
        //System.out.println(String.valueOf(erro));
        _sinalControladorD.add(time,D);
        return D;
    }
    
    public double _D(double time, double erro, double kd) {
        double _D = kd * (_pv-nivel_anterior)/h;
        _sinalControladorD.add(time,_D);
        return _D;
    }
    
    public double PD(double time, double erro, double kp, double kd){
        double aux = (P(time,erro,kp) + D(time,erro,kd));
        erro_anterior = erro;
        return aux;
    }
    
    public double PI(double time, double erro, double kp, double ki) {
        double I = I(time, erro,ki);
        double aux = (P(time,erro,kp) + I);
        //erro_integral_anterior = I;
        //return (P(time,erro,kp) + I);
        erro_integral_anterior = I;
        
        System.out.println(String.valueOf(I));
        return aux;
    }
    
    public double PID(double time, double erro, double kp, double ki, double kd) {
        double I = I(time,erro,ki);
        double aux = (P(time,erro,kp) + I + D(time,erro,kd));
        erro_anterior = erro;
        erro_integral_anterior = I;
        return aux;
        
        //erro_anterior = erro;
        //erro_integral_anterior = I;
        //return (P(time,erro,kp) + I + D(time,erro,kd));
    }
    
    public double PI_D(double time, double erro, double kp, double ki, double kd) {
        double I = I(time,erro,ki);
        double aux = (P(time,erro,kp) + I + _D(time,erro,kd));
        erro_integral_anterior = I;
        nivel_anterior = _pv;
        return aux;
        //erro_integral_anterior = I;
        //nivel_anterior = _pv;
        //return (P(time,erro,kp) + I + _D(time,erro,kd));
    }
    
}